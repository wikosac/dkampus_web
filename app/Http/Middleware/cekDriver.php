<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class cekDriver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->get('user_id') == false && session()->get('level') != 'driver'){
            return redirect('/driver');
        }else if(session()->get('level') != 'driver'){
            return redirect('/driver');
        }else if(session()->get('user_id') == false){
            return redirect('/driver');
        }
        return $next($request);
    }
}
