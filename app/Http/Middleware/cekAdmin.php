<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class cekAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->get('user_id') == false && session()->get('level') != 'admin_super'){
            return redirect('/login');
        }else if(session()->get('level') != 'admin_super'){
            return redirect('/login');
        }else if(session()->get('user_id') == false){
            return redirect('/login');
        }
        return $next($request);
    }
}
