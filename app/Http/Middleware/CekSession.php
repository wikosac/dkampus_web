<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CekSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->get('user_id') == false){
            return redirect('/login')->with('pesan','Silahkan login ke akun anda terlebih dahulu');
        }
        return $next($request);
    }
}
