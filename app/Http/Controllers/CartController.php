<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\{Produk, Umkm, Cart, Pengguna, Transaksi, Orderan, Ongkir, User};



class CartController extends Controller

{
    
    public function index()

    {
        $user = User::find(session('data')->id);
        $carts = Cart::where('user_id','=',session()->get('user_id'))
        ->get();
        
        $ongkirs = Ongkir::all();

        $subtotal = Cart::where('user_id','=',session()->get('user_id'))->sum('subtotal');
		//$catatan = Cart::where('user_id','=',session()->get('user_id'))->get('catatan');
	
        $jumlah_barang = Cart::where('user_id', session('data')['id'])->sum('qty');

        return view('pengguna.cart', compact('carts','subtotal','ongkirs',/*'catatan',*/'jumlah_barang','user'));
    }

    public function store(Request $req){
        //check if this shop has not set a location

        if(!$req->lat && !$req->lng){
            return redirect('detail/' . $req->produk_id)->with('danger','Toko ini belum menetapkan lokasi. Silahkan pilih toko lain');
        }

        //checked if umkm or users want to buy something
        // Users and umkm latitude and longitude check

        if(session('data')->lat == null && session('data')->lng == null && $req->level == 'user'){
            return redirect('edit-profile')->with('danger','Harap periksa pengaturan lokasi browser anda');

        }elseif(!session('data')->lat && !session('data')->lng && $req->level == 'umkm' ){
        return redirect('umkm/setting')->with('danger','Harap Periksa pengaturan lokasi anda');
        }

        // Users address check
        if(session('data')->provinsi == null && session('data')->kabupaten == null && session('data')->kecamatan == null && $req->level == 'user'){
            return redirect('edit-profile')->with('danger','Harap Mengisi Provinsi, kabupaten dan kecamatan');
        }

        if(session('data')->no_telephone == null){
            return redirect('edit-profile')->with('danger','Harap Mengisi Nomor Telepon Anda');
        }

        // if(session('data')->alamat == null){
        //     return redirect('edit-profile')->with('danger','Harap Mengisi Alamat Pengiriman Anda');
        // }

        if(session('data')->password1 == null){
            return redirect('edit-profile')->with('danger','Harap Menambahkan Password Anda');
        }

        // umkm address check
        if(session('data')->provinsi == null && session('data')->kabupaten == null && session('data')->kecamatan == null && $req->level == 'umkm'){
            return redirect('edit-profile')->with('danger','Harap Mengisi Provinsi, kabupaten dan kecamatan');
        }
		
		 // Save user address
        if($req->alamat != null){
            User::select('alamat')
            ->where('id', session('data')->id)
            ->update(['alamat' => $req->alamat]);
        } else {
            return back()->with('danger','Masukkan alamat!');
        }
        
        // Save current order
        $qty    = $req->qty;

        $harga  = $req->harga;

        $id     = $req->produk_id;
        $subtotal = $qty * $harga;



        $cart = new Cart;

        $cart->produk_id = $id;

        $cart->qty = $qty;

        $cart->subtotal = $subtotal;
		
		//$cart->catatan = $req->catatan;

        $cart->user_id = session()->get('user_id');
        $cart->save();

        // Check if order have same umkm id or not
        $carts = Cart::where('user_id','=',session()->get('user_id'))
        ->get();

        // retrieve data from carts collection
        foreach ($carts as $cart) { 
           // check umkm id
           if($cart->produk->umkm_id != $req->umkm_id){
            $req->session()->put('failed','Pesananmu berbeda toko, apakah anda ingin mengganti pesanan ?');
            return back();
        }
            return redirect('cart');
        }
    }



    public function delete($id){
        
        $cart = Cart::find($id);
        $cart->delete();

        return redirect('cart');

    }

    public function deleteCollection(Request $request){
        $id = $request->id;
        //get data with collection id
        $cart = Cart::whereIn('id',$id)->delete();
            //delete failed session if failed session exist
            if(session()->has('failed')){
                
                session()->forget('failed');
            }
        if($cart){
            return response()->json(['code'=>1,'msg'=>'Pesanan Berhasil di Hapus!']);
        }else{
            return response()->json(['code'=>0,'msg'=>'Error Menghapus Pesanan!']);
        }
    }

    public function changedOrder(Request $request,$id)
    {

        return response()->json(['code'=>1,'result'=>$request->qty]);
    }

    public function order(Request $req){
        
        if ($req->total !== null) {
            $transaksi = new Transaksi;
            $transaksi->user_id = session()->get('user_id');
            $transaksi->ongkir_id = 1;
            $transaksi->total = $req->total;
            $transaksi->ongkirmap = $req->ongkirmap;
            $transaksi->subtotal = $req->subtotal;
            $transaksi->catatan = $req->catatan;
            $masukkan = $transaksi->save();

            Cart::where('user_id','=',session()->get('user_id'))->chunk(3, function($cart) use($transaksi){
                foreach($cart as $a){
                    $orderan = new Orderan;
                    $orderan->transaksi_id = $transaksi->id;
                    $orderan->produk_id = $a->produk->id;
                    $orderan->qty = $a->qty;
                    $orderan->save();
                }
            });

            if($masukkan){
                User::whereLevel('driver')->chunk(2, function($drivers){

                    foreach($drivers as $driver){

                    $phone = $driver->no_telephone;
                    $data = [
                        'api_key' => '1rho8Ayo6wiNqe9brwwHMxctbjpvmQ',
                        'sender' => '6285156181331',
                        'number' => $phone,
                        'message' => "Halo D'squad ".$driver->fullname.' ada orderan baru masuk nih, Yuk buruan ambil Klik link dibawah ini : https://dkampus.dcreativ.id/driver'
                    ];
                   
                    $curl = curl_init();
                                                        
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://apiwa.dcreativ.my.id/send-message",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                    ));
                    
                    $response = curl_exec($curl);
                    
                    curl_close($curl);
                    
                    }
                });
                return redirect('pesanan');
            }
        }
        return redirect()->back()->withErrors(['msg' => 'Terjadi Kesalahan!']);
    }
}

