<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Umkm, User, Produk, TokoTransaksi, Provinces, Kategori};
use Illuminate\Support\Facades\{ DB, File };
use PhpParser\Node\Expr\FuncCall;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use Carbon\Carbon;
use ImageKit\ImageKit; 
use Illuminate\Support\Arr;


class UmkmController extends Controller
{

    public function __construct()
    {
        $this->umkm   = new umkm();
        $this->produk = new produk();
        $this->users  = new User();
    }

    public function index()
    {

        $umkm = Umkm::where('user_id','=',session()->get('user_id'))->firstOrFail();
        
        session(['umkm'=>$umkm]);
        
        if(!session('umkm')->lat && !session('umkm')->lng){
            return redirect('umkm/setting')->with('danger','Anda belum memberikan lokasi toko anda');
        }
        
        $total_pendapatan = TokoTransaksi::where('umkm_id','=',session()->get('umkm_id'))->sum('total');

        $total_produk = Produk::where('umkm_id','=',session()->get('umkm_id'))->count();
        
        $feetoko = TokoTransaksi::where('umkm_id','=',session()->get('umkm_id'))->whereMonth('created_at',Carbon::now()->month)->sum('qty');
        
        return view('umkm.dashboard', compact('total_pendapatan','total_produk','feetoko'));
    }
    //halaman Daftar UMKS
    public function add()
    {
        $provinces = Provinces::orderBy('name','ASC')->get();
        return view('umkm.registerumkm', compact('provinces'));
    }

    //controller tambah data/logika pendaftaran UMKMS
    public function insert(Request $r)
    {
        // User Update Level ;
        $user = User::find(session()->get('user_id'));
        $user->level = 'umkm';
        $user->status = 'belum';
        $user->save();
        
        $umkm = new Umkm;
        $umkm->nama_toko = $r->nama_toko;
        $umkm->provinsi = $r->provinsi;
        $umkm->kabupaten = $r->kota;
        $umkm->kecamatan = $r->kecamatan;
        $umkm->kelurahan = $r->kelurahan;
        $umkm->alamat = $r->alamat;
        $umkm->no_telephone = $r->nowa;
        $umkm->keterangan = $r->keterangan;
        $umkm->status = 'belum';
        $umkm->gambar_ktp = 'default.png';
        $umkm->nik = $r->nik;
        $umkm->user_id = session()->get('user_id');
        $umkm->save();

        return redirect()->to('login')->with('pesan','Berhasil mendaftar UMKM');
    }

    // Buat Akun
    public function addRegister()
    {
        return view('umkm.register');
    }

    public function store(Request $r)
    {
        if ($r->password1 == $r->password2) {
            $user = new User;
            $user->nama_depan           = $r->nama_depan;
            $user->nama_belakang        = $r->nama_belakang;
            $user->username             = $r->username;
            $user->email                = $r->email;
            $user->password1            = $r->password1;
            $user->level                = 'umkm';
            $user->save();

            session(['user_id' => $user->id]);
            return redirect()->to('registerumkm')->with('pesan' . 'data berhasil di tambah');
        } else {
            return redirect('register')->with('error', 'Konfirmasi Password tidak sama');
        }
    }


    //Auth
    public function auth(Request $data)
    {
        $users = User::where('username', '=', $data->username)->where('password1', '=', $data->password)->where('status', '=', 'diterima')->count();
        if ($users == 1) {
            $user = User::where('username', '=', $data->username)->where('password1', '=', $data->password)->firstOrFail();
            if($user->level == 'umkm'){
                session(['user_id' => $user->id]);
                session(['level' => 'umkm']);
                session(['data' => $user]);
                session(['umkm_id' => $user->umkm->id]);
                return redirect('umkm/dashboard');
            }else if($user->level == 'admin_super'){
                session(['user_id' => $user->id]);
                session(['level' => 'admin_super']);
                session(['data' => $user]);
                return redirect('admin/dashboard'); 
            }else if($user->level == 'user'){
                session(['user_id' => $user->id]);
                session(['level' => 'user']);
                session(['data' => $user]);
                return redirect('/'); 
            }
        }else{
            return redirect('login')->with('pesan','Harap Periksa Kembali Username / password anda');
        }
    }

    public function keluar()
    {
        session()->forget('user_id');
        session()->forget('data');
        session()->forget('umkm');
        session()->forget('umkm_id');
        session()->forget('level');
        session()->forget('lat');
        session()->forget('lng');
        session()->forget('provinsi');
        session()->forget('kabupaten');
        session()->forget('kecamatan');
        session()->forget('kelurahan');
        session()->forget('alamat');
        session()->forget('success');
        session()->forget('failed');
        return redirect('login');
    }

    

    // CRUD PRODUK
    
    public function manage_produk()
    {
        $manage_produk = Produk::where('umkm_id', '=', session()->get('umkm_id'))->orderBy('id','desc')->simplePaginate(5);
        // $manage_produk->prepend($imageKit->);
        return view('umkm.manage_produk', compact('manage_produk'));
    }
    public function add_produk()
    {
        $kategoris = Kategori::all();
        $user = User::find(session()->get('user_id'));
        return view('umkm.add_produk', compact('user','kategoris'));
    }

    public function insert_produk(Request $r)
    {
        if(!session('data')->level == 'umkm'){
            return keluar();
        }        
          //imagekit key
          $public_key = env('public_key');
          $private_key = env('private_key');
          $url_end_point = env('url_end_point');
  
          //image kit initialization
          $imageKit = new ImageKit(
              $public_key,
              $private_key,
              $url_end_point
          );

        Request()->validate(
            [
                'gambar_produk'        => 'required|mimes:jpg,jpeg,png|max:4024',
                'nama_produk'          => 'required|string',
                'rak_produk'           => 'required|string',
                'keterangan'           => 'required|string|max:1000|min:10',
                
                ]
            );
            
        $user = User::find(session()->get('user_id'));
        /* upload gambar */
        $file       = $r->gambar_produk;
        $fileName   = uniqid().'.'.$file->extension();
        $getImage = file_get_contents($file);
        $encodeImage = base64_encode($getImage);
            //upload to imagekit.io
        $uploadFile = $imageKit->uploadFile([
            "file" => $encodeImage,
            "fileName" => $fileName
        ]);

        $dataProduk = [
            'gambar_produk'        => $fileName, 
            'url_image'            => $uploadFile->result->url,
            'nama_produk'          => $r->nama_produk,
            'rak_produk'           => $r->rak_produk,
            'harga'                => $r->harga + 1000,
            'keterangan'           => $r->keterangan,
            'promo'                => $r->promo,
            'terjual'              => 0,
            'visibilitas'          => $r->visibilitas,
            'umkm_id'              => $user->umkm->id,
        ];

        if(!session('umkm')->lat && !session('umkm')->lng){

            $setData = data_set($dataProduk,'visibilitas','sembunyikan');
            $this->produk->addData($setData);

            if($setData){
                return redirect('umkm/setting')->with('danger','Produk disembunyikan, Harap Memasukan lokasi tokomu');
            }else{
                return redirect('umkm/setting')->with('danger','Terjadi kesalahan , produk tidak tersimpan, Harap Memasukan lokasi tokomu');
            }
        }
        
        $this->produk->addData($dataProduk);
        return redirect()->to('/umkm/manage_produk')->with('success','Produk berhasil di tambahkan');
    }

    public function edit_produk($id){
        $kategoris = Kategori::all();
        $produk = Produk::find($id);
        return view('umkm.edit_produk', compact('produk','kategoris'));
    }

    public function update_produk(Request $r)
    {
       
        if(!session('umkm')->lat && !session('umkm')->lng){
            return redirect('umkm/setting')->with('danger','Harap Tentukan Lokasi Toko anda terlebih dahulu');
        }

         //imagekit key
         $public_key = env('public_key');
         $private_key = env('private_key');
         $url_end_point = env('url_end_point');
 
         //image kit initialization
         $imageKit = new ImageKit(
             $public_key,
             $private_key,
             $url_end_point
         );

        Request()->validate(
            [
                'nama_produk'          => 'required',
                'rak_produk'           => 'required',
                'keterangan'           => 'required',

            ]
        );

        /* upload gambar */
        $user = User::find(session()->get('user_id'));
        $file       = $r->gambar_produk;

        if($file){

            $fileName   = uniqid().'.'.$file->extension();
              /* upload gambar */
                $user = User::find(session()->get('user_id'));
                $file       = $r->gambar_produk;
                $fileName   = uniqid().'.'.$file->extension();

                $getImage = file_get_contents($file);
                $encodeImage = base64_encode($getImage);

                //upload to imagekit.io
                $uploadFile = $imageKit->uploadFile([
                    "file" => $encodeImage,
                    "fileName" => $fileName
                ]);
            
            $produk = Produk::find($r->produk_id);
            $produk->gambar_produk = $fileName;
            $produk->url_image = $uploadFile->result->url;
            $produk->nama_produk = $r->nama_produk;
            $produk->rak_produk = $r->rak_produk;
            $produk->harga = $r->harga;
            $produk->keterangan = $r->keterangan;
            $produk->visibilitas = $r->visibilitas;
            $produk->promo = $r->promo;
            $isSave = $produk->save();

            return redirect()->to('/umkm/manage_produk')->with('success','Produk berhasil di perbarui');
        }else{
            $produk = Produk::find($r->produk_id);
            $produk->nama_produk = $r->nama_produk;
            $produk->rak_produk = $r->rak_produk;
            $produk->harga = $r->harga;
            $produk->keterangan = $r->keterangan;
            $produk->visibilitas = $r->visibilitas;
            $produk->promo = $r->promo;
            $isSave = $produk->save();
         
            return redirect()->to('/umkm/manage_produk')->with('success','Produk berhasil di perbarui');
        }
    }

    public function delete_produk($id){
        $produk = Produk::find($id);
        $hapus = $produk->delete();
        if($hapus){
            return redirect()->back()->with('success','Produk berhasil dihapus');
        }else{
            return redirect()->back()->with('error','Produk tidak bisa dihapus karena sudah ada ada ditransaksi');
        }
    }

    // TRANSAKSI

    public function transaksi(){
        $umkm = Umkm::where('user_id','=',session()->get('user_id'))->firstOrFail();
        $total = TokoTransaksi::where('umkm_id','=',session()->get('umkm_id'))->sum('total');
        return view('umkm.transaksi', compact('umkm','total'));
    }
    public function setting(){
        $umkm = Umkm::where('user_id','=',session()->get('user_id'))->firstOrFail();
        return view('umkm.setting', compact('umkm'));
    }
    public function updateSetting(Request $req){
        $umkm = Umkm::find($req->umkmId);
        $umkm->lat = $req->lat;
        $umkm->lng = $req->lng;
        $umkm->save();
        if($umkm->save()){
            session()->forget('umkm');

            $umkm = Umkm::where('user_id','=',session()->get('user_id'))->firstOrFail();

            session(['umkm'=>$umkm]);
        }
        return redirect()->route('umkm.setting');
    }
    public function updateLocation(Request $req){
        $umkm = Umkm::find($req->umkmId);
        $umkm->lat = $req->lat;
        $umkm->lng = $req->lng;
        $umkm->save();

        if($umkm->save()){
            session()->forget('umkm');
            
            $umkm = Umkm::where('user_id','=',session()->get('user_id'))->firstOrFail();

            session(['umkm'=>$umkm]);
        }
        return response()->json($umkm);
    }
	
	// Visibilitas

    public function visibilitas_off($id)
    {
        $produk =  Produk::find($id);
       $produk->visibilitas = "sembunyikan";
       $produk->save();
       if($produk->save() == true){
        return back()->with('success','Produk Disembunyikan');
       }
        return back()->with('error','Terjadi Kesalahan! Silahkan Coba Lagi');
    }
    public function visibilitas_on($id)
    {
       $produk =  Produk::find($id);
       $produk->visibilitas = "terlihat";
       $produk->save();
       if($produk->save() == true){
        return back()->with('success','Produk Dapat dilihat');
       }
        return back()->with('error','Terjadi Kesalahan! Silahkan Coba Lagi');
    }

}
