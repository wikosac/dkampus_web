<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Arr;
use App\Models\User;

class GoogleController extends Controller
{
    public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }

    public function callback(SocialGoogleAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->user());
        auth()->login($user);
        return redirect()->to('/edit-profile');
    }
    
    public function handleGoogleCallback(){
        $user = Socialite::driver('google')->stateless()->user();
        $givenAndFamily = Arr::only($user->user,['given_name','family_name']);
        $googleAccount = User::where('google_id',$user->getId())->first();
        if($googleAccount){

             //set session when success
             session(['user_id' => $googleAccount->id]);
             session(['level' => 'user']);
             session(['data' => $googleAccount]);

             if ($googleAccount->alamat !== null) {
                return redirect('/');
            }
            
             return redirect('edit-profile');
        }else{
            if ($googleAccount == null) {
                $userUpdate = new User;
                $userUpdate->google_id = $user->getId();
                $userUpdate->nama_depan = data_get($givenAndFamily,'given_name');
                $userUpdate->nama_belakang =  data_get($givenAndFamily,'family_name');
                $userUpdate->fullname = $user->getName();
                $userUpdate->username = $user->getName();
                $userUpdate->email = $user->getEmail();
                $userUpdate->level = 'user';
                $userUpdate->save();
            }
                
                if($userUpdate){
                    //set session when success
                    session(['user_id' => $userUpdate->id]);
                    session(['level' => 'user']);
                    session(['data' => $userUpdate]);
                    
                    return redirect('edit-profile')->with('google_login','Sebelum Melanjutkan Silahkan lengkapi data diri anda');
                    
                }else{
                    return redirect('login')->with('pesan','Pendaftaran gagal, terjadi kesalahan saat pendaftaran!');
                }
        }
    }
}
