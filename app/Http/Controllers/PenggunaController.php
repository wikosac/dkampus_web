<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use \Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\{Kategori, Kawasan, Produk, MitraProduk, Umkm, Cart, User, Transaksi, Orderan, TokoTransaksi, Mitra, Provinces, Regencies, Districts, Villages};


class PenggunaController extends Controller
{
     public function __construct()
    {

        $this->cart   = new cart();
    }
    /*
        Home page
    */
    public function index()
    {
        $lariss = TokoTransaksi::with('produk')
        ->selectRaw('sum(qty) as totaljual,umkm_id,produk_id')
        ->whereMonth('created_at',Carbon::now()->month)
        ->groupBy('produk_id')
        ->orderBy("created_at","DESC")
        ->get();

        $promos = Produk::with('umkm')
        ->where('visibilitas', 'terlihat')
        ->where('promo', 'iya')
        ->get();

        $kategoris = Kategori::select('icon','nama')->get();
        $kawasans = Kawasan::select('nama','icon')->get();
        $produks = Produk::with('umkm')->where('visibilitas', 'terlihat')
        ->orderBy('id', 'desc')->simplePaginate(12);

        return view('pengguna.home', compact('produks','lariss','promos','kategoris','kawasans'));
    }
    /*
        Banner update pages
    */
    public function bannerStore(Request $request, $id){
        $banner = Umkm::find($id);
        $validate = Validator::make($request->all(), [
            'banner' => 'required|file|image'
        ])->validate();

        if($request->file('banner') && $validate){
            if($banner->banner){
                 //delete old file if exist
                unlink("images/banner/" . $banner->banner);
            }
            //insert new image
            $file = $request->file('banner');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('images/banner'), $filename);
            $banner->banner = $filename;
        }

        $banner->save();

        if($banner){
            return back()->with('success','berhasil mengubah banner');
        }else{
            return back()->with('failed','Terjadi kesalahan! pengubahan banner gagal');
        }
    }
     /*
        Detail Product
    */
    public function detail($id)
    {
        if(!session()->has('data')){
            return redirect('/login')->with('pesan','Harap Login terlebih dahulu');
        }
        $carts = Cart::where('user_id','=',session()->get('user_id'))->get();
        $produk = Produk::without('umkm')->find($id);
        $umkm = Umkm::with('produks')->find($produk->umkm_id);
        $user = User::find(session('data')->id);

        return view('pengguna.detail', compact('produk', 'umkm','user','carts'));
    }
    /* 
        cart insert
    */
    public function keranjang($produk, $user, $qty, $subtotal)
    {
        $data = [
            'produk_id' => $produk,
            'user_id' =>  $user,
        ];
        $this->cart->insert($data);
        return redirect()->to('cart');
    }
     /* 
        add register
    */
    public function addRegister()
    {
        $provinces = Provinces::orderBy('name','ASC')->get();
        return view('pengguna.register', compact('provinces'));
    }
     /* 
        store register data
    */
    public function store(Request $r)
    {
        if ($r->password1 == $r->password2) {
            $user = new Pengguna();
            $user->nama_depan           = $r->nama_depan;
            $user->nama_belakang        = $r->nama_belakang;
            $user->email                = $r->email;
            $user->password1            = $r->password1;
            $user->level                = 'pengguna';
            $user->save();
            return redirect()->back()->with('pesan' . 'data berhasil di tambah');
        } else {
            return redirect('register-v2')->with('error', 'Konfirmasi Password tidak sama');
        }
    }
    /* 
        store detail
    */
    public function toko($id){
        $toko = Umkm::with(['produks' => function($query){
            $query->where('visibilitas', 'terlihat');
            $query->orderBy('id','desc');
        }])->where('id','=',base64_decode($id))->first();
        if($toko){
            return view('pengguna.toko', compact('toko'));
        }else{
            echo 'Toko yang anda cari tidak ada';
        }
    }
    /* 
        register account
    */
    public function register(Request $request){
        
        $validated = $request->validate([
                'username'=>'required|unique:users',
                'nama_depan'=>'required|string',
                'nama_belakang'=>'required|string',
                'username'=>'required|string|unique:users|max:20',
                'email'=>'required|string|unique:users',
                'no_telephone'=>'required|numeric|min:16|unique:users',
                'alamat'=>'required|string',
                'password1'=>'required|string',
            ]);
    
        if($request->password1 == $request->password2){

            $user = new User;

            $user->nama_depan       = $request->nama_depan;
            $user->nama_belakang    = $request->nama_belakang;
            $user->username         = $request->username;
            $user->email            = $request->email;
            $user->no_telephone     = $request->no_telephone;
            $user->provinsi         = $request->provinsi;
            $user->kabupaten        = $request->kota;
            $user->kecamatan        = $request->kecamatan;
            $user->kelurahan        = $request->kelurahan;
            $user->alamat           = $request->alamat;
            $user->password1        = $request->password1;
            $user->level            = 'user';
            $user->status            = 'diterima';
            $user->save();

            $message = "Halo! berikut detail akun anda,\n\n Username :".$request->username."\n Password :".$request->password1."\n\n Mohon untuk tidak memberikan detail akun anda kepada orang lain";

            $data = [
                'api_key' => '1rho8Ayo6wiNqe9brwwHMxctbjpvmQ',
                'sender' => '6285156181331',
                'number' => $request->telepon,
                'message' => $message
            ];

            $curl = curl_init();                             
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://apiwa.dcreativ.my.id/send-message",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => json_encode($data),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
              ),
            ));
    
            $response = curl_exec($curl);
            curl_close($curl);

            if($response){
                return redirect('/login');
            }
            return redirect('/login');
        }else{

            return redirect()->back()->with('pesan', 'Konfirmasi Password tidak sama');

        }
    }
    public function login_check(Request $req){
        $user = User::where('username', '=', $req->username)->where('password1', '=', $req->password)->firstOrFail();

        $users = User::where('username', '=', $req->username)->where('password1', '=', $req->password)->count();

        if ($users == 1) {

            session(['user_id' => $user->id]);
            session(['data' => $user]);
            session(['level' => 'user']);
            return redirect('/');
        }else{
            return redirect('/');
        }

    }
    /**
     * Logout Function
     */
    public function keluar()
    {
        session()->forget('user_id');
        session()->forget('data');
        session()->forget('umkm');
        session()->forget('lat');
        session()->forget('lng');
        session()->forget('umkm_id');
        session()->forget('level');
        session()->forget('success');
        session()->forget('failed');
        return redirect()->to('/');
    }
    /**
     * Order data
     */
    public function datapesanan(){
        $transaksis = Transaksi::where('user_id','=',session()->get('user_id'))->where('status','!=','selesai')->orderBy('id','desc')->get();
        return view('datapesanan', compact('transaksis'));
    }
    /**
     * Suggest create
     */
    public function saranPesanan(Request $request)
    {
        $send = Saran::create(
            [
                'user_id'=>decrypt($request->user_id),
                'saran'=>$request->saran
            ]
        );

        if($send == true){
            return back()->with('success','Terimakasih atas saranmu!');
        }
        return back()->with('failed','Terjadi Kesalahan! Proses Kirim gagal');
    }
    /** 
     * Transaksi Select
     * 
    */
    public function pesanan(){
        $transaksis = Transaksi::where('user_id','=',session()->get('user_id'))->where('status','=','selesai')->get();
        return view('pesanan', compact('transaksis'));
    }
    public function delete_pesanan($id){
        $transaksis = Transaksi::find($id);
        $transaksis->delete();
        return redirect('pesanan');
    }
    public function search(Request $req){
        $kawasans = kawasan::all();
        if($req->lok == true  && $req->sort != 'laris'){
            $produks = Produk::with('umkm')->where('nama_produk','like','%'.$req->q.'%')->whereHas('umkm', function($q) use($req){
                $q->where('kecamatan',$req->lok);
            })->orderBy('created_at',$req->sort)->get();
            $tokos = Umkm::whereHas('produks', function($q) use($req){
                $q->orderBy('created_at','desc');
                $q->where('nama_produk','LIKE','%'.$req->q.'%');
            })->get();
            return view('pengguna.result', compact('produks','tokos','kawasans'));
        }
        else if($req->lok == true && $req->sort == 'laris'){
            $produks = Produk::with('umkm')->where('nama_produk','like','%'.$req->q.'%')->whereHas('umkm', function($q) use($req){
                $q->where('kecamatan',$req->lok);
            })->orderBy('terjual','desc')->get();
            $tokos = Umkm::whereHas('produks', function($q) use($req){
                $q->orderBy('created_at','desc');
                $q->where('nama_produk','LIKE','%'.$req->q.'%');
            })->get();
            return view('pengguna.result', compact('produks','tokos','kawasans'));
        }
        else if($req->sort == 'laris'){
            $produks = Produk::with('umkm')->where('nama_produk','like','%'.$req->q.'%')->orderBy('terjual','desc')->get();
            $tokos = Umkm::whereHas('produks', function($q) use($req){
                $q->orderBy('created_at','desc');
                $q->where('nama_produk','LIKE','%'.$req->q.'%');
            })->get();
            return view('pengguna.result', compact('produks','tokos','kawasans'));
        }
        else if($req->sort){
            $produks = Produk::with('umkm')->where('nama_produk','like','%'.$req->q.'%')->orderBy('created_at',$req->sort)->get();
            $tokos = Umkm::whereHas('produks', function($q) use($req){
                $q->orderBy('created_at','desc');
                $q->where('nama_produk','LIKE','%'.$req->q.'%');
            })->get();
            return view('pengguna.result', compact('produks','tokos','kawasans'));
        }else{
            $produks = Produk::where('nama_produk','like','%'.$req->q.'%')->get();
            $tokos = Umkm::whereHas('produks', function($q) use($req){
                $q->orderBy('created_at','desc');
                $q->where('nama_produk','LIKE','%'.$req->q.'%');
            })->get();
            return view('pengguna.result', compact('produks','tokos','kawasans'));
        }
    }
    public function mitra(){
        $mitras = Mitra::all(['slug','foto']);
        $produks = MitraProduk::with('mitra')->orderBy('id', 'desc')->simplePaginate(12);
        return view('pengguna.mitra', compact('mitras','produks'));
    }
    public function mitra_detail(Mitra $slug){
        $mitra = $slug->with('produks')->firstOrFail();
        return view('pengguna.mitra-detail', compact('mitra'));
    }
    public function mitra_produk_detail($slug){
        $produk = MitraProduk::whereSlug($slug)->with('mitra')->firstOrFail();
        return view('pengguna.mitra-produk-detail', compact('produk'));
    }

    public function data_wilayah($jenis,Request $req){
        switch($jenis){
            case 'kota' :
                $id_provinsi = $req->id_provinces;
                if($id_provinsi == ''){
                    exit;
                }else{
                    $regencies = Regencies::where('province_id', $id_provinsi)->orderBy('name','asc')->get();
                    foreach($regencies as $kabupaten){
                        echo "<option value='".$kabupaten->name."' data-id='".$kabupaten->id."'>".$kabupaten->name."</option>";
                    }
                }
                break;
            case 'kecamatan' :
                $id_kecamatan = $req->id_regencies;
                if($id_kecamatan == ''){
                    exit;
                }else{
                    $districts = Districts::where('regency_id', $id_kecamatan)->orderBy('name','asc')->get();
                    foreach($districts as $kecamatan){
                        echo "<option value='".$kecamatan->name."' data-id='".$kecamatan->id."'>".$kecamatan->name."</option>";
                    }
                }
                break;
            case 'kelurahan' :
                $id_kelurahan = $req->id_district;
                if($id_kelurahan == ''){
                    exit;
                }else{
                    $villages = Villages::where('district_id', $id_kelurahan)->orderBy('name','asc')->get();
                    foreach($villages as $kelurahan){
                        echo "<option value='".$kelurahan->name."' data-id='".$kelurahan->id."'>".$kelurahan->name."</option>";
                    }
                }
                break;
        }
    }

    public function edit_profile(){
        $user = User::find(session('data')->id);
        $provinces = Provinces::orderBy('name','ASC')->get();
        return view('pengguna.edit-profile', compact('provinces','user'));
    }
    public function edit_profile_proses(Request $req){
        $provinsis = Provinces::where('id', $req->provinsi)->get();
        $kotas = Regencies::where('id', $req->kota)->get();
        $kecamatans = Districts::where('id', $req->kecamatan)->get();
        $desas = Villages::where('id', $req->kelurahan)->get();

        if($req->provinsi){
            $user = User::find(session('data')->id);
            $user->nama_depan       = $req->nama_depan;
            $user->nama_belakang    = $req->nama_belakang;
            $user->username         = $req->username;
            $user->password1         = $req->password1;
            $user->email            = $req->email;
            $user->no_telephone     = $req->telepon;
            foreach ($provinsis as $provinsi) {
                $user->provinsi         = $provinsi->name;
            }
            foreach ($kotas as $kota) {
                $user->kabupaten         = $kota->name;
            }
            foreach ($kecamatans as $kecamatan) {
                $user->kecamatan         = $kecamatan->name;
            }
            foreach ($desas as $desa) {
                $user->kelurahan         = $desa->name;
            }
            // $user->kabupaten        = $kota->name;
            // $user->kecamatan        = $kecamatan->name;
            // $user->kelurahan        = $desa->name;
            $user->alamat           = $req->alamat;
            $update = $user->save();
            session(['data' => $user]);

            if($update){
                return redirect()->back()->with('success', 'Data diri anda berhasil diperbarui');
            }else{
                return redirect()->back()->with('danger', 'Data diri anda gagal diperbarui');
            }
        }else if($req->password1){
            $user = User::find(session('data')->id);
            $user->nama_depan       = $req->nama_depan;
            $user->nama_belakang    = $req->nama_belakang;
            $user->username         = $req->username;
            $user->password1         = $req->password1;
            $user->email            = $req->email;
            $user->no_telephone     = $req->telepon;
            $user->alamat           = $req->alamat;
            $update = $user->save();
            session(['data' => $user]);

            if($update){
                return redirect()->back()->with('success', 'Data diri anda berhasil diperbarui');
            }else{
                return redirect()->back()->with('danger', 'Data diri anda gagal diperbarui');
            }
        }else{
            $user = User::find(session('data')->id);
            $user->nama_depan       = $req->nama_depan;
            $user->nama_belakang    = $req->nama_belakang;
            $user->username         = $req->username;
            $user->email            = $req->email;
            $user->no_telephone     = $req->telepon;
            $user->alamat           = $req->alamat;
            $update = $user->save();
            session(['data' => $user]);

            if($update){
                return redirect()->back()->with('success', 'Data diri anda berhasil diperbarui');
            }else{
                return redirect()->back()->with('danger', 'Data diri anda gagal diperbarui');
            }
        }
    }
    public function kawasan($daerah){
        $tokos = Umkm::where('kelurahan', $daerah)->get();
        return view('pengguna.kawasan', compact('tokos'));
    }
    public function kategori($kategori){
        $produks = Produk::where('rak_produk',$kategori)->where('visibilitas','terlihat')->simplePaginate(12);
        return view('pengguna.kategori', compact('produks'));
    }
    public function update_location(Request $req){
        $user = User::find($req->userId);
        if($user){
            $user->lat = $req->lat;
            $user->lng = $req->lng;
            $user->save();
            session(['data' => $user]);
            return "Success";
        }else{
            return "Error";
        }
    }

    public function getKota(Request $req)
    {
        $id_provinsi = $req->id_provinsi;
        $kotas = Regencies::where('province_id', $id_provinsi)->get();
        $option = "<option >Pilih Kota/Kabupaten</option>";

        foreach ($kotas as $kota) {
            $option.= "<option value='$kota->id'>$kota->name</option>";
        }
        echo $option;
    }
    public function getKecamatan(Request $req)
    {
        $id_kota = $req->id_kota;
        $kecamatans = Districts::where('regency_id', $id_kota)->get();
        $option = "<option >Pilih Kecamatan</option>";

        foreach ($kecamatans as $kecamatan) {
            $option.= "<option value='$kecamatan->id'>$kecamatan->name</option>";
        }
        echo $option;
    }
    public function getKelurahan(Request $req)
    {
        $id_kecamatan = $req->id_kecamatan;
        $kelurahans = Villages::where('district_id', $id_kecamatan)->get();
        $option = "<option >Pilih Kelurahan</option>";

        foreach ($kelurahans as $kelurahan) {
            $option.= "<option value='$kelurahan->id'>$kelurahan->name</option>";
        }
        echo $option;
    }
}
