<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Produk, Umkm, Cart, User, Transaksi, Orderan, Ongkir, TokoTransaksi, DriverTransaksi};

class DriverController extends Controller
{
    /*
        login page
    */
    public function index(){
        if(session()->has('data')){

            return redirect('driver/dashboard');
        }
            return view('driver.index');
    }
    /*
        driver dashboard
    */
    
    public function dashboard(){
        return view('driver.dashboard');
    }

    /*
        authentication check
    */
    public function auth(Request $request){
        $users = User::select('username','level','password1','status')->where('username', '=', $request->username)->where('password1', '=', $request->password)->where('level', '=', 'driver')->where('status', '=', 'diterima')->count();

        if ($users == 1) {

            $user = User::where('username', '=', $request->username)
            ->where('password1', '=', $request->password)
            ->where('level', '=', 'driver')
            ->firstOrFail();

            session(['user_id' => $user->id]);
            session(['data' => $user]);
            session(['level' => 'driver']);

            return redirect('/driver/dashboard');
        }

        return redirect('/driver')->with('pesan', 'Akun tidak ditemukan atau belum diterima');
    }

    /*
        create account page
    */
    public function create_account(){

        return view('driver.create_account');
    }

     /*
        store account to database
    */
    public function store_account(Request $request){
        $request->validate([
            'fullname' => 'required|unique:users|max:255',
            'username' => 'required|unique:users|max:255',
            'email' => 'required|string',
            'password' => 'required|string',
            'whatsapp' => 'required|numeric',
            'nik' => 'required|numeric',
            'ktp'=>'file|image|mimes:jpg,png',
            'sim'=>'file|image|mimes:jpg,png'
        ]);

        
        $user = new User;

        if($request->file('ktp')){
            //insert image ktp
            $file = $request->file('ktp');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('foto_ktp'), $filename);
            $user->ktp = $filename;
        }

        if($request->file('sim')){
            //insert image sim
            $file = $request->file('sim');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('foto_sim'), $filename);
            $user->sim = $filename;
        }

        $user->fullname = $request->fullname;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->status = 'belum';
        $user->password1 = $request->password;
        $user->no_telephone = $request->whatsapp;
        $user->nik = $request->nik;
        $user->level = 'driver';
        $save = $user->save();

        if($save){
            return redirect('/driver/create/success');
        }

        return back()->with('pesan','Gagal mendaftar akun');

    }

     /*
        if driver success register show this page
    */
    public function success_create(){
        return view('driver.success_register');
    }

    /*
        data users order
    */
    public function dataorder(){
        $transactions = Transaksi::where('status','=','belum')->orderBy('id','desc')->get();
        return view('driver.dataorder', compact('transactions'));
    }

    /*
        detail order
    */
    public function detail($id){
        $transaksi = Transaksi::findOrFail($id);
        return view('driver.detail', compact('transaksi'));
    }

    /*
        take button order
    */
    public function ambil($id){

        $transaksi = Transaksi::find($id);
        $transaksi->driver_id = session()->get('user_id');
        $transaksi->status = 'proses';
        $transaksi->save();

        $data = [
            'api_key' => '1rho8Ayo6wiNqe9brwwHMxctbjpvmQ',
            'sender' => '6285156181331',
            'number' => $transaksi->user->no_telephone,
            'message' => 'Halo '.$transaksi->user->nama_depan. "pesanan mu sedang diproses oleh D'squad : " . session('data')['fullname']
        ];
   
        $curl = curl_init();
                                                    
                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://apiwa.dcreativ.my.id/send-message",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS => json_encode($data),
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                  ),
                ));
                
                $response = curl_exec($curl);
                
                curl_close($curl);

        return redirect()->back();
    }

    /*
        Done button
    */
    public function selesai($id){
        $transaksi = Transaksi::find($id);
        $transaksi->status = 'selesai';
        $transaksi->save();

        $data = [
            'api_key' => '1rho8Ayo6wiNqe9brwwHMxctbjpvmQ',
            'sender' => '6285156181331',
            'number' => $transaksi->user->no_telephone,
            'message' => 'Halo '.$transaksi->user->nama_depan. " pesanan mu Telah diselesaikan oleh D'squad " . session('data')['fullname'] . "Silahkan isi saran yaa, biar kami menjadi lebih baik lagi"
        ];
        $curl = curl_init();
                                            
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://apiwa.dcreativ.my.id/send-message",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
       
        $user = User::find(session()->get('user_id'));
        $user->total_order = $user->total_order + 1;
        $user->fee_ojek = $user->fee_ojek + 1;
        $user->save();

        foreach($transaksi->orderans as $orderan){
            $umkm_id     = $orderan->produk->umkm->id;
            $produk_id  = $orderan->produk->id;
            $qty        = $orderan->qty;
            $total      = $orderan->qty * $orderan->produk->harga;

            $tokotransaksi              = new TokoTransaksi;
            $tokotransaksi->umkm_id     = $umkm_id;
            $tokotransaksi->produk_id   = $produk_id;
            $tokotransaksi->qty         = $qty;
            $tokotransaksi->total       = $total;
            $tokotransaksi->save();

            $produk = Produk::find($orderan->produk->id);
            $produk->terjual = $produk->terjual + $qty;
            $produk->save();
        }

        return redirect('driver/orderan');
    }

     /*
        delete orders
    */
    public function delete($id){
        $transaksi = Transaksi::find($id);

        $data = [
            'api_key' => '1rho8Ayo6wiNqe9brwwHMxctbjpvmQ',
            'sender' => '6285156181331',
            'number' => $transaksi->user->no_telephone,
            'message' => 'Halo '.$transaksi->user->nama_depan. " pesanan mu telah dibatalkan oleh D'squad : " . session('data')['fullname'] . " Karena Telah Melebihi waktu pembayaran"
        ];
        $curl = curl_init();
                                            
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://apiwa.dcreativ.my.id/send-message",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $transaksi->delete();

        return redirect('driver/orderan');
    }

    /*
        history orders
    */
    public function orderan() {
        $transactions = Transaksi::where('driver_id','=', session()->get('user_id'))->orderBy('id','desc')->get();
        return view('driver.orderan', compact('transactions'));
    }

    /*
        profile page
    */
    public function profile(){
        $driver = User::select('fee_ojek','total_order','username')->find(session()->get('user_id'));

        return view('driver.profile', compact('driver'));
    }

    /*
        not found page
    */
    public function notfound()
    {
        return view('driver.404');
    }

    /*
        logout
    */
    public function logout()
    {
        session()->forget('data');
        session()->forget('user_id');
        session()->forget('level');
        return redirect('driver');
    }
}
