<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\{ Umkm, User, Produk, Ongkir, Mitra, MitraProduk, Kategori, Kawasan,TokoTransaksi };
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index(){
        $total_umkm = Umkm::count();
        $total_produk = Produk::count();
        $total_driver = User::where('level','driver')->count();
        return view('admin.dashboard', compact('total_umkm','total_produk','total_driver'));
    }

    // UMKM
    public function umkm(){
        $umkms = Umkm::where('status','diterima')->get(['nama_toko','kecamatan','no_telephone','id','created_at']);
        return view('admin.umkm.index', compact('umkms'));
    }
    public function pending_umkm(){
        $umkms = Umkm::where('status','belum')->get(['nama_toko','kecamatan','no_telephone','id','created_at']);
        return view('admin.umkm.pending', compact('umkms'));
    }
    public function detail_umkm($id){
        $umkm = Umkm::find($id);
        return view('admin.umkm.detail', compact('umkm'));
    }
    public function approve_umkm($id){
        $umkm = Umkm::find($id);
        $user = User::find($umkm->user_id);
        $umkm->status = 'diterima';
        $user->status = 'diterima';
        $user->save();
        $umkm->save();
        return redirect('admin/umkm')->with('success', 'UMKM Berhasil diterima');
    }
    public function produk_umkm($id){
        $umkm = Umkm::find($id);
        return view('admin.umkm.produk', compact('umkm'));
    }
    public function delete_umkm($id){
        $umkm = Umkm::find($id);
        $delete = $umkm->delete();
        if($delete){
            return redirect()->back()->with('success', 'UMKM Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'UMKM Gagal dihapus');
        }
    }
    public function delete_produk($id){
        $produk = Produk::find($id);
        $delete = $produk->delete();
        if($delete){
            return redirect()->back()->with('success', 'Produk Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'Produk Gagal dihapus');
        }
    }

    // DRIVER
    public function driver(){
        $drivers = User::where('level','driver')->where('status','diterima')->get(['fullname','no_telephone','id','created_at']);
        return view('admin.driver.index', compact('drivers'));
    }
    public function detail_driver($id){
        $driver = User::find($id);
        return view('admin.driver.detail', compact('driver'));
    }
    public function pending_driver(){
        $drivers = User::where('level','driver')->where('status','belum')->get(['fullname','no_telephone','id','created_at']);
        return view('admin.driver.pending', compact('drivers'));
    }
    public function approve_driver($id){
        $driver = User::find($id);
        $driver->status = 'diterima';
        $driver->save();
        return redirect('admin/driver')->with('success', 'Driver Berhasil diterima');
    }
    public function delete_driver($id){
        $driver = User::find($id);
        $delete = $driver->delete();
        if($delete){
            return redirect()->back()->with('success', 'Driver Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'Driver Gagal dihapus');
        }
    }

    // ONGKIR
    public function ongkir(){
        $ongkirs = Ongkir::all();
        return view('admin.ongkir.index', compact('ongkirs'));
    }
    public function input_ongkir(){
        return view('admin.ongkir.input');
    }
    public function store_ongkir(Request $req){
        $ongkir = new Ongkir;
        $ongkir->jarak = $req->jarak;
        $ongkir->harga = $req->harga;
        $ongkir->save();
        return redirect('admin/ongkir')->with('success', 'Ongkir berhasil ditambahkan');
    }
    public function delete_ongkir($id){
        $ongkir = Ongkir::find($id);
        $delete = $ongkir->delete();
        if($delete){
            return redirect()->back()->with('success', 'Ongkir Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'Ongkir Gagal dihapus');
        }
    }

    // TRANSAKSI
    public function transaksi(){
        $transaksis = User::where('level','driver')->get(['fullname','total_order','fee_ojek','id','no_telephone']);
        return view('admin.transaksi.index', compact('transaksis'));
    }

    public function tokoTransaksi()
    {
        $transaksiToko = tokoTransaksi::select('toko_transaksis.umkm_id','produk_id','qty','total','umkms.nama_toko','nama_produk','toko_transaksis.created_at')
        ->join('umkms','umkms.id','=','toko_transaksis.umkm_id')
        ->join('produks','produks.id','=','toko_transaksis.produk_id')
        ->orderBy('created_at','DESC')
        ->get();
        
        return view('admin.transaksi.toko',compact('transaksiToko'));
    }

    public function approve_transaksi($id){
        $transaksi = User::find($id);
        $transaksi->total_order = 0;
        $transaksi->fee_ojek = 0;
        $transaksi->save();
        return redirect()->back()->with('success', 'Transaksi berhasil direset');
    }
    
    // MITRA
    public function mitra(){
        $mitras = Mitra::all();
        return view('admin.mitra.index', compact('mitras'));
    }
    public function mitra_edit($id){
        $mitra = Mitra::find($id);
        return view('admin.mitra.edit', compact('mitra'));
    }
    public function mitra_update(Request $req){
        $mitra = Mitra::find($req->id);

        $mitra->nama = $req->nama;
        $mitra->alamat = $req->alamat;
        $mitra->tokopedia = $req->tokopedia;
        $mitra->bukalapak = $req->bukalapak;
        $mitra->shopee = $req->shopee;
        $mitra->ig = $req->ig;
        $mitra->keterangan = $req->keterangan;
        $mitra->save();

        return redirect()->to('admin/mitra')->with('success', 'Data mitra berhasil diperbarui');


    }
    public function produk_mitra($id){
        $mitra = Mitra::find($id);
        return view('admin.mitra.produk', compact('mitra'));
    }
    public function delete_produk_mitra($id){
        $produk = MitraProduk::find($id);
        $delete = $produk->delete();
        if($delete){
            return redirect()->back()->with('success', 'Produk Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'Produk Gagal dihapus');
        }
    }

    public function mitra_produk_add(){
        $mitras = Mitra::all();
        return view('admin.mitra.create', compact('mitras'));
    }
    public function edit_produk_mitra($id){
        $produk = MitraProduk::find($id);
        $mitras = Mitra::all();
        return view('admin.mitra.produk-edit', compact('produk','mitras'));
    }
    public function mitra_produk_store(Request $req){
        $file       = $req->gambar_produk;
        $fileName   = uniqid().'.'.$file->extension();
        $file->move('foto_produk/', $fileName);
        
        $produkMitra = new MitraProduk;
        $produkMitra->mitra_id = $req->mitra;
        $produkMitra->nama = $req->nama;
        $produkMitra->link_tokped = $req->link_tokped;
        $produkMitra->link_shopee = $req->link_shopee;
        $produkMitra->link_bukalapak = $req->link_bukalapak;
        $produkMitra->slug = Str::slug($req->nama, '-');
        $produkMitra->harga = $req->harga;
        $produkMitra->deskripsi = $req->deskripsi;
        $produkMitra->foto = $fileName;
        $produkMitra->save();

        return redirect('admin/mitra');
        
    }
    public function update_produk_mitra(Request $req){
        
        $produkMitra = MitraProduk::find($req->id);
        $produkMitra->mitra_id = $req->mitra;
        $produkMitra->nama = $req->nama;
        $produkMitra->link_tokped = $req->link_tokped;
        $produkMitra->link_shopee = $req->link_shopee;
        $produkMitra->link_bukalapak = $req->link_bukalapak;
        $produkMitra->slug = Str::slug($req->nama, '-');
        $produkMitra->harga = $req->harga;
        $produkMitra->deskripsi = $req->deskripsi;
        $produkMitra->save();

        return redirect('admin/mitra');
        
    }


    // Kategori
    public function kategori(){
        $kategoris = Kategori::all();
        return view('admin.kategori.index', compact('kategoris'));
    }
    public function add_kategori(){
        return view('admin.kategori.create');
    }
    public function store_kategori(Request $req){

        if ($req->icon) {
            $fileIcon = $req->icon;
            $namaIcon = uniqid().'.'.$fileIcon->extension();
        }else{
            $fileIcon = null;
            $namaIcon = "default.svg";
        }

        $kategori = new Kategori;
        $kategori->icon = $namaIcon;
        $kategori->nama = $req->nama;
        $kategori->save();

        if($kategori->save() && $req->icon){

            $fileIcon->move('images/kategori/', $namaIcon);
            return redirect('/admin/kategori')->with('success','Berhasil Upload Icon dan Menambahkan Kategori');

        }elseif($kategori->save()){ 

            return redirect('/admin/kategori')->with('success','Berhasil Menambahkan Kategori');

        }else{
            return redirect('admin/kategori')->with('error', 'Icon tidak tersedia!');
        }
    }
    public function edit_kategori($id){
        $kategori = Kategori::find($id);
        return view('admin.kategori.edit', compact('kategori'));
    }
    public function update_kategori(Request $req){

        if ($req->icon) {
            $fileIcon = $req->icon;
            $namaIcon = uniqid().'.'. $fileIcon->extension();
        }else{
            $fileIcon = null;
            $namaIcon = "default.svg";
        }

        $kategori = Kategori::find($req->id);
        $kategori->icon = $namaIcon;
        $kategori->nama = $req->nama;
        $kategori->save();

       if($kategori->save() && $req->icon){

            $fileIcon->move('images/kategori/', $namaIcon);
            return redirect('/admin/kategori')->with('success','Berhasil Upload Icon dan Menambahkan Kategori');

        }elseif($kategori->save()){ 

            return redirect('/admin/kategori')->with('success','Berhasil Menambahkan Kategori');

        }else{
            return redirect('admin/kategori')->with('error', 'Icon tidak tersedia!');
        }
    }
    public function delete_kategori($id){
        $kategori = Kategori::find($id);
        $delete = $kategori->delete();
        if($delete){
            return redirect()->back()->with('success', 'kategori Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'kategori Gagal dihapus');
        }
    }

    // Kawasan
    public function kawasan(){
        $kawasans = Kawasan::all();
        return view('admin.kawasan.index', compact('kawasans'));
    }
    public function add_kawasan(){
        return view('admin.kawasan.create');
    }
    public function store_kawasan(Request $req){
        
        if ($req->icon) {
            $fileIcon = $req->icon;
            $namaIcon = uniqid().'.'.$fileIcon->extension();
        }else{
            $fileIcon = null;
            $namaIcon = "default.svg";
        }

        $kawasan = new Kawasan;
        $kawasan->icon = $namaIcon;
        $kawasan->nama = $req->nama;
        $kawasan->save();

        if($kawasan->save() && $req->icon){

            $fileIcon->move('images/icons', $namaIcon);
            return redirect('/admin/kawasan')->with('success','Berhasil Upload Icon dan Menambahkan Kawasan');

        }elseif($kawasan->save()){ 

            return redirect('/admin/kawasan')->with('success','Berhasil Menambahkan Kategori');

        }else{
            return redirect('admin/kawasan')->with('error', 'Icon tidak tersedia!');
        }

        return redirect('admin/kawasan')->with('success', 'kawasan berhasil ditambahkan');
    }
    public function edit_kawasan($id){
        $kawasan = kawasan::find($id);
        return view('admin.kawasan.edit', compact('kawasan'));
    }
    public function update_kawasan(Request $req){

        if ($req->icon) {
            $fileIcon = $req->icon;
            $namaIcon = uniqid().'.'.$fileIcon->extension();
        }else{
            $fileIcon = null;
            $namaIcon = "default.svg";
        }

        $kawasan = kawasan::find($req->id);
        $kawasan->icon = $namaIcon;
        $kawasan->nama = $req->nama;
        $kawasan->save();

        if($kawasan->save() && $req->icon){

            $fileIcon->move('images/icons/', $namaIcon);
            return redirect('/admin/kawasan')->with('success','Berhasil Upload Icon dan Menambahkan Kawasan');

        }elseif($kawasan->save()){ 

            return redirect('/admin/kawasan')->with('success','Berhasil Menambahkan Kategori');

        }else{
            return redirect('admin/kawasan')->with('error', 'Icon tidak tersedia!');
        }

        return redirect('admin/kawasan')->with('success', 'kawasan berhasil diubah');
    }
    public function delete_kawasan($id){
        $kawasan = kawasan::find($id);
        $delete = $kawasan->delete();
        if($delete){
            return redirect()->back()->with('success', 'kawasan Berhasil dihapus');
        }else{
            return redirect()->back()->with('error', 'kawasan Gagal dihapus');
        }
    }

    public function logout(){

        session()->forget('data');
        session()->forget('umkm');
        session()->forget('user_id');
        session()->forget('umkm_id');
        session()->forget('level');
        session()->forget('success');
        session()->forget('failed');

        return redirect('/');
    }
}
