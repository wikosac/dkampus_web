<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TokoTransaksi extends Model
{
    use HasFactory;

    public function umkm(){
        return $this->belongsTo(Umkm::class);
    }
    public function produk(){
        return $this->belongsTo(Produk::class);
    }
}
