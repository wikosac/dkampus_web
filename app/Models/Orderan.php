<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderan extends Model
{
    use HasFactory;

    public function produk(){
        return $this->belongsTo(Produk::class);
    }
    public function transaksi(){
        return $this->belongsTo(Transaksi::class);
    }
}
