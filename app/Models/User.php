<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use HasFactory;

    protected $table = 'users';

    public function umkm()
    {
        return $this->hasOne(Umkm::class);
    }
    public function carts()
    {
        return $this->hasMany(Cart::class);
    }
    public function transaksis(){
        return $this->hasMany(Transaksi::class);
    }
}
