<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Produk extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($produk) {
            $relationMethods = ['cart', 'orderans'];

            foreach ($relationMethods as $relationMethod) {
                if ($produk->$relationMethod()->count() > 0) {
                    return false;
                }else{
                    return true;
                }
            }
        });
    }

    public function addData($data)
    {
        DB::table('produks')->insert($data);
    }

    public function umkm()
    {
        return $this->belongsTo(Umkm::class);
    }
    public function cart()
    {
        return $this->hasOne(Cart::class);
    }
    public function orderans()
    {
        return $this->hasOne(Orderan::class);
    }
    public function tokotransaksis(){
        return $this->hasMany(TokoTransaksi::class);
    }
}
