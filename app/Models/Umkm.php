<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Umkm extends Model
{
    use HasFactory;

    public function addData($data)
    {
        DB::table('umkms')->insert($data);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function produks()
    {
        return $this->hasMany(Produk::class);
    }
    public function deleteData($id)
    {
        DB::table('produks')
            ->where('id', $id)
            ->delete();
    }
    public function tokotransaksis(){
        return $this->hasMany(TokoTransaksi::class);
    }
}
