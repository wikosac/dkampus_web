$(document).ready(function () {
    //untuk memanggil plugin select2
    $("#provinsi").select2({
        placeholder: "Pilih Provinsi",
        language: "id",
    });
    $("#kota").select2({
        placeholder: "Pilih Kota/Kabupaten",
        language: "id",
    });
    $("#kecamatan").select2({
        placeholder: "Pilih Kecamatan",
        language: "id",
    });
    $("#kelurahan").select2({
        placeholder: "Pilih Kelurahan",
        language: "id",
    });

    //saat pilihan provinsi di pilih maka mengambil data di data-wilayah menggunakan ajax
    $("#provinsi").change(function () {
        var id_provinces = $("option:selected", this).attr("data-id");
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "/data-wilayah/kota",
            data: {
                id_provinces: id_provinces,
            },
            success: function (msg) {
                console.log(msg);
                $("select#kota").html(msg);
                getAjaxKota();
            },
        });
    });

    $("#kota").change(getAjaxKota);

    function getAjaxKota() {
        var id_regencies = $("option:selected", this).attr("data-id");
        console.log(id_regencies);
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah/kecamatan",
            data: { id_regencies: id_regencies },
            success: function (msg) {
                $("select#kecamatan").html(msg);
                getAjaxKecamatan();
            },
        });
    }

    $("#kecamatan").change(getAjaxKecamatan);
    function getAjaxKecamatan() {
        var id_district = $("option:selected", this).attr("data-id");
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah/kelurahan",
            data: { id_district: id_district },
            success: function (msg) {
                $("select#kelurahan").html(msg);
            },
        });
    }
});
