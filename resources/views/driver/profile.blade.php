@extends('layout.driver', ['title' => 'Driver - Dcreativ'])

@section('content')

<div class="container mb-5 mt-4">
        <h4 class="font-700">Profile Saya</h4>
        <div class="d-flex mt-0 align-items-center mt-3">
            <div class="">
            </div>
            <div class="ms-2">
            <span style="font-size:14px;font-weight:600" class="text-dark text-capitalize">{{ session('data')['username'] }}</span>
            
        </div>
            <div class="ms-auto">
                <a class="btn btn-danger btn-sm" href="{{ url('/driver/logout') }}"><i class="fa fa-sign-out-alt" style="font-size:20px !important"></i></a>
            </div>
            
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-2">
                <p style="font-size: 14px; font-weight:600">Informasi Lainnya</p>
            </div>
        <div class="col-sm-2">
            <div class="text-dark mt-2" style="font-size:12px;">
                <a href="https://cdn.discordapp.com/attachments/909937842573176849/1039937337313988678/Qris.jpg" class="text-decoration-none" style="font-size: 14px" target="_blank">QR Code</a>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="text-dark mt-4" style="font-size:12px;">
                <a href="#"  data-bs-toggle="modal" data-bs-target="#prosedurModal" class="text-decoration-none" style="font-size: 14px">Prosedur Peminjaman Uang</a>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="text-dark mt-4" style="font-size:12px;">
                <a href="#"class="text-decoration-none" style="font-size: 14px" data-bs-toggle="modal" data-bs-target="#alurModal">Alur Pemesanan</a>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="text-dark mt-4" style="font-size:12px;">
                <a href="" data-bs-toggle="modal" data-bs-target="#penjelasanModal" class="text-decoration-none" style="font-size: 14px">Penjelasan Fitur</a>
            </div>
        </div>
       </div>
       <hr>
       <div class="row">
        <div class="col-sm-2">
            <p style="font-size: 14px; font-weight:600">FAQ</p>
        </div>
        <div class="col-sm-2" style="font-size:13px;">
            <a href="https://api.whatsapp.com/send?phone=+6285163193031&text=Permisi, Saya ingin bertanya" target="_blank" rel="noopener noreferrer" >Hubungi Customer Service</a>
        </div>
       </div>
</div>

<!-- Modal Peminjaman-->
<div class="modal fade" id="prosedurModal" tabindex="-1" aria-labelledby="prosedurModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="prosedurModalLabel">Prosedur Peminjaman Uang</h1>
        </div>
        <div class="modal-body">
          <ol>
            <li>Melakukan Pendaftaran</li>
          </ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"data-bs-dismiss="modal" style="background-color:#E18028">Paham</button>
        </div>
      </div>
    </div>
  </div>
  
<!-- Modal Penjelasan-->
<div class="modal fade" id="penjelasanModal" tabindex="-1" aria-labelledby="prosedurModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <span>Penjelasan Fitur</span>
        </div>
        <div class="modal-body" style="font-size: 12px;">
          <p class="fw-bold">Fitur-fitur</p>
          <ol>
            <li>Fitur Pesanan</li>
            <p class="mt-2">Berisi berbagai macam pesanan dari pelanggan yang dapat Anda ambil ketika pelanggan melakukan pesanan</p>

            <li>Fitur Riwayat Pesanan</li>
            <p class="mt-2">Berisi riwayat dari pesanan yang telah diselesaikan oleh Anda</p>

            <li>Fitur Profile</li>
            <p class="mt-2">Berisi Nama Anda, infomasi yang dibutuhkan selama Anda melakukan kegiatan operasional, serta berisi pertanyan-pertanyaan umum yang sering di tanyakan kepada customer service</p>
          </ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"data-bs-dismiss="modal" style="background-color:#E18028">Paham</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal Alur-->
<div class="modal fade" id="alurModal" tabindex="-1" aria-labelledby="prosedurModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <span>Alur Pemesanan</span>
        </div>
        <div class="modal-body modal-dialog modal-dialog-scrollable" style="font-size: 12px;">
          <ol>
            <li class="fw-bold">Pelanggan</li>
            <p class="mt-2">Pelanggan Melakukan pemesanan dengan cara menekan tombol "beli" maka otomatis pesanan akan muncul pada halaman "pesanan" D'squad</p>
            <li class="fw-bold">D'Squad</li>
            <p class="mt-2">
            <ul>
                <li>
                    Setelah pelanggan melakukan pemesanan, maka Anda dapat mengambil pesanan dengan cara : 
                    <ol>
                        <li>Klik tombol "detail" maka informasi pesanan akan muncul </li>

                        <li>Kemudian klik tombol "ambil"</li>
                    </ol>
                </li>
                <br>
                <li>
                    Apabila Anda ingin membatalkan pesanan, maka dapat menekan tombol "Batal" pada halaman detail.
                </li>
                <br>
                <li>
                    Apabila Anda ingin melakukan komunikasi dengan toko, maka Anda dapat menekan tombol "Chat Toko".
                </li>
                <br>
                <li>
                    Kemudian apabila Anda ingin melakukan komunikasi dengan pelanggan, maka Anda dapat menekan tombol "Chat Pembeli".
                </li>
                <br>
                <li>
                    Apabila Anda telah selesai melakukan transaksi dengan pelanggan, maka Anda dapat menekan tombol "selesai" dan otomatis pesanan akan muncul pada halaman riwayat pesanan.
                </li>
            </ul>
            </p>
          </ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"data-bs-dismiss="modal" style="background-color:#E18028">Paham</button>
        </div>
      </div>
    </div>
  </div>

@endsection