@extends('layout.driver', ['title' => 'Driver - Dcreativ'])

@section('content')

<div class="container my-5">

    <div class="">
        <div class="d-flex justify-content-between"> 
            <h3 class="font-700">Orderan</h3>
            <div>{{ session('data')['username'] }} ({{ session()->get('level') }})</div>
            </div>
        <div id="orderan">

        </div>
    </div>

</div>


@endsection

@push('script')

<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:"{{ url('/driver/dataorder') }}",
            success:function(data){
                $('#orderan').html(data);
            }
        })

        setInterval(() => {
            $.ajax({
            type:'GET',
            url:"{{ url('/driver/dataorder') }}",
            success:function(data){
                $('#orderan').html(data);
            }
        })
        }, 5000);
    })
</script>
@endpush