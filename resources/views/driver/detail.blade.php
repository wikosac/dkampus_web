@extends('layout.driver', ['title' => 'Driver - DSquad'])



@section('content')



<div class="container mt-4" style="margin-bottom:6em">



    <div class="">

        <a href="#" onclick="window.history.back()" style="color:#E96E00" class="mb-3">

            <i class="fa fa-chevron-left"></i> Back

        </a>

        <h3 class="font-700 mt-3"> Detail Orderan</h3>

        <div class="mb-2" style="font-size:15px">

            <div class="card border-0 shadow-sm">

                <div class="card-body">

                    <div class="font-600">Pemesan</div>

                    <table class="table table-borderless" style="font-size:13px">

                        <tr>

                            <td class="font-600">Nama</td>

                            <td>{{ $transaksi->user->nama_depan }} {{ $transaksi->user->nama_belakang }}</td>

                        </tr>

                        <tr>

                            <td class="font-600">Alamat</td>

                            <td>

                                @if($transaksi->user->alamat != null)

                                    {{ $transaksi->user->alamat }}

                                @else

                                    Alamat tidak ada silahkan chat pembeli

                                @endif

                            </td>

                        </tr>

                        <tr>

                            <td class="font-600">Email</td>

                            <td>

                                @if($transaksi->user->email != null)

                                    {{ $transaksi->user->email }}

                                @else

                                    Alamat tidak ada silahkan chat pembeli

                                @endif

                            </td>

                        </tr>

                    </table>

                </div>

            </div>

        </div>

        <div class="mb-2" style="font-size:15px">

            <div class="card border-0 shadow-sm">

                <div class="card-body">

                    <div class="font-600">Pesanan</div>

                    <div class="mt-3">

                        @foreach($transaksi->orderans as $orderan)

                            <div class="d-flex align-items-center mb-4">

                                <div class="" style="width:80px">

                                    <img src="{{ url($orderan->produk->url_image) }}" data-src="{{ url($orderan->produk->url_image) }}" class="lazy rounded w-100" alt="Image">

                                </div>

                                <div class="ms-3">     

                                    <div class="font-600 mb-0 pb-0" style="font-size:15px">{{ $orderan->produk->nama_produk }}</div>

                                    <small>{{ $orderan->produk->umkm->nama_toko }}</small>
                                    <small>({{ $orderan->produk->umkm->no_telephone }})</small>

                                    <div class="mt-2" style="font-size:small">

                                        Harga : Rp {{ number_format($orderan->produk->harga,2,',','.') }}<br/>

                                        Jumlah : {{ $orderan->qty }}<br/>

                                        Alamat : {{ $orderan->produk->umkm->alamat }}<br/>

                                        Catatan : {{ $transaksi->catatan }}<br/>

                                        <br/>

                                        <a href="https://wa.me/62{{ $orderan->produk->umkm->no_telephone }}" class="btn w-100 btn-sm" style="background:#E96E00;color:white;border:1px solid #E96E00;font-size:15px">Chat Toko</a>

                                    </div>

                                </div>

                            </div>

                        @endforeach

                    </div>

                </div>

            </div>

        </div>

        <div class="mb-2" style="font-size:15px">

            <div class="card border-0 shadow-sm">

                <div class="card-body">

                    <div class="d-flex justify-content-between mb-2">

                        <div class="font-600" style="color:#9B9B9B;font-size:small">Subtotal</div>

                        <div class="font-700">Rp {{ number_format($transaksi->subtotal,2,',','.') }}</div>

                    </div>

                    {{-- <div class="d-flex justify-content-between mb-2">

                        <div class="font-600" style="color:#9B9B9B;font-size:small">Biaya Pengiriman</div>
                        @if($transaksi->ongkirmap != '')
                        <div class="font-700">Rp {{ number_format($transaksi->ongkirmap,2,',','.') }}</div>
                        @else
                        <div class="font-700">Rp {{ number_format($transaksi->ongkir->harga,2,',','.') }}</div>
                        @endif
                    </div> --}}

                    <div class="d-flex justify-content-between mb-2">

                        <div class="font-600" style="color:#9B9B9B;font-size:small">Biaya Penanganan</div>

                        <div class="font-700">Rp.{{number_format(1000)}}</div>

                    </div>

                    <div class="d-flex justify-content-between mb-2">

                        <div class="font-600" style="color:#E96E00;font-size:15px">Total</div>

                        <div class="font-700">Rp {{ number_format($transaksi->total,2,',','.') }}</div>

                    </div>

                </div>

            </div>

        </div>

        <div class="mb-2 mt-3">

        @if($transaksi->driver_id)

            @if($transaksi->driver_id == session()->get('user_id') && $transaksi->status == 'proses')

            @if($transaksi->user->no_telephone != null)

            <div class="mb-2">

                <a href="https://wa.me/62{{ $transaksi->user->no_telephone }}" class="btn w-100" style="background:white;color:#E96E00;border:1px solid #E96E00;font-size:15px"">Chat Pembeli</a>

            </div>

            @endif

            <div class="mb-2 mt-2">

                <a href="{{ url('/driver/order/selesai/'.$transaksi->id) }}" class="btn w-100" style="background:#E96E00;color:white;font-size:15px">Selesai</a>

            </div>

            <div class="mb-2">

                <a href="{{ url('driver/order/delete/'.$transaksi->id) }}" class="btn w-100" style="background:#E96E00;color:white;font-size:15px">Batalkan</a>

            </div>

            @endif

        @else

            @if($transaksi->user->no_telephone != null)

            <div class="mb-2">

                <a href="https://wa.me/62{{ $transaksi->user->no_telephone }}" class="btn w-100" style="background:white;color:#E96E00;border:1px solid #E96E00;font-size:15px"">Chat Pembeli</a>

            </div>

            @endif

            <div class="mb-2">

                <a href="{{ url('/driver/order/ambil/'.$transaksi->id) }}" class="btn w-100" style="background:#E96E00;color:white;font-size:15px">Ambil</a>

            </div>

            <div class="mb-2">

                <a href="#" onclick="window.history.back()" class="btn w-100" style="background:white;color:#E96E00;border:1px solid #E96E00;font-size:15px">Batal</a>

            </div>

        @endif

        </div>

    </div>



</div>



@endsection