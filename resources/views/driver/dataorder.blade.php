@forelse($transactions as $transaksi)
<div class="mb-2">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-start">
                        <div class="">
                            {{-- <img src="{{ asset('images/defaultavatar.jpg') }}" alt="" class="rounded-circle" style="width:50px;height:50px;object-fit:cover"> --}}
                        </div>
                        <div class="ms-3" style="flex:1">
                            <div class="font-600">{{ $transaksi->user->nama_depan }}</div>
                            <div class="font-400" style="font-size:small;color:#9B9B9B">{{ $transaksi->user->kecamatan }}</div>
                            <small style="font-size:small;color:#9B9B9B">{{ $transaksi->created_at->diffForHumans() }}</small>
                        </div>
                        <div class="">
                            <a class="btn w-100 font-500" style="background:#E96E00;color:white;font-size:small" href="{{ url('/driver/order/detail/'.$transaksi->id) }}">Detail</a>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
@empty
<div class="mt-3 text-center">
    <p>Tidak ada orderan</p>
</div>
@endforelse