@extends('layout.driver', ['title' => 'Driver - DKampus'])

@push('style')

@endpush

@section('content')
<div class="" style="background:#E96E00;padding:10px 15px;position:relative;overflow:hidden;height:24vh">
    <div style="background:#F39D4F;width:139px;height:137px;border-radius:100px;display:block;position:absolute;right:-2vw;top:-10vh;opacity:50%"></div>
    <div style="background:#E18028;width:169px;height:167px;border-radius:100px;display:block;position:absolute;right:-20vw;bottom:-70px;opacity:60%"></div>
</div>
<div style="background:url('{{ asset('images/bgdriver.png') }}')center;background-size:cover;border:0;height:100vh">
    <div class="container py-5" style="padding-left:3em;padding-right:3em">
        <div class=" animate__animated animate__fadeInUp">
            <h1 class="font-600 py-0 my-0 mb-1" style="font-size:34px">Selamat Datang Kembali <span style="color: #E18028">D'squad</span></h1>
            <p class="my-0 py-0 font-400" style="font-size:small">Masuk untuk melanjutkan</p>
        </div>
        <div class="form" style="margin-top:10vh">

            <div class="mb-2">
                @if(session('pesan'))
                <div class="alert alert-danger" style="font-size:small">{{ session('pesan') }}</div>
                @endif
            </div>

            <form action="{{ url('/driver/login/auth') }}" method="post">
                @csrf
                <div class="mb-4">
                    <input type="text" name="username" id="username" class="form-control" autocomplete="off" placeholder="Username">
                </div>
                <div class="mb-4">
                    <input type="password" name="password" id="password" class="form-control" autocomplete="off" placeholder="Password">
                </div>
                <div class="mt-5">
                    <button class="btn w-100 font-500" style="background:#E96E00;color:white" type="submit">SIGN IN</button>
                </div>
            </form>
            <div class="mt-3 text-center">
                <a href="{{ url('/driver/404') }}" class="text-white" style="font-size:small">Forgot you password?</a>
            </div>
        </div>
        <div class="text-center" style="position:fixed;bottom:25px;left:0;right:0">
            <a href="{{ url('/driver/create_account') }}" class="text-dark" style="font-size:small">Don't have an account? Sign up</a>
        </div>
    </div>
</div>

@endsection