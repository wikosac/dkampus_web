@extends('layout.driver', ['title' => 'Driver - Dcreativ'])

@section('content')

<div class="container my-5">

    <div class="">
        <h3 class="font-700">Riwayat Orderan</h3>
        <div class="">
        @forelse($transactions as $transaksi)
        <div class="mb-2">
            <div class="card border-0 shadow-sm">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-start">
                        <div class="">
                            {{-- <img src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="" class="rounded-circle" style="width:50px;height:50px;object-fit:cover"> --}}
                        </div>
                        <div class="ms-3" style="flex:1">
                            <div class="font-600">{{ $transaksi->user->nama_depan }}</div>
                            <div class="font-500" style="font-size:small">Rp {{ number_format($transaksi->total,2,',','.') }}</div>
                            <small style="font-size:small;color:#9B9B9B">@if($transaksi->status == 'proses') <span class="badge bg-warning text-dark">Sedang dikerjakan</span> @elseif($transaksi->status == 'selesai') <span class="badge bg-success">Selesai</span> @endif</small>
                        </div>
                        <div class="">
                            <a class="btn w-100 font-500" style="background:#E96E00;color:white;font-size:small" href="{{ url('/driver/order/detail/'.$transaksi->id) }}">Lihat</a>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @empty
        <div class="mt-3 text-center">Anda belum menyelesaikan orderan apapun.</div>
        @endforelse
        </div>
    </div>

</div>


@endsection