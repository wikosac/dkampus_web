@extends('layout.driver', ['title' => 'Buat Akun DSquad - DKampus'])

@section('content')

<div class="" style="background:#E96E00;padding:10px 15px;position:relative;overflow:hidden;height:24vh">
    <div style="background:#F39D4F;width:139px;height:137px;border-radius:100px;display:block;position:absolute;right:-2vw;top:-10vh;opacity:50%"></div>
    <div style="background:#E18028;width:169px;height:167px;border-radius:100px;display:block;position:absolute;right:-20vw;bottom:-70px;opacity:60%"></div>
    <h3 class="text-white font-600" style="position:relative;bottom:-12vh">Buat Akun D'Squad</h3>
    <h3 class="text-white font-600" style="position:relative;top:-1vh">
        <a href="#" onclick="window.history.back()" class="text-white">
            <i class="fa fa-arrow-left"></i>
        </a>
    </h3>
</div>
<div class="" style="background:#fff;padding:10px 15px;position:relative;top:-3vh;border-radius:20px 20px 0 0">
    <div class="my-3 px-3">
        <div class="mb-3">
            <h5 class="font-600 my-0 mb-1">Selamat Datang <br> Calon <span style="color: #E18028">D'squad</span></h5>
            <p class="my-0 py-0 font-400" style="font-size:small;color:#A0A0A0">Silahkan Mendaftar</p>
        </div>
        <div class="">
            <form action="{{ url('/driver/create/store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="mb-3 form-create">
                    <label for="">Name</label>
                    <input type="text" name="fullname" id="fullname" class="form-input-create" autocomplete="off" placeholder="Enter your fullname" value="{{ old('fullname') }}">
                    @if($errors->has('fullname'))
                        <div class="danger" style="color: red;">{{ $errors->first('fullname') }}</div>
                    @endif
                </div>
                <div class="mb-3 form-create">
                    <label for="">Username</label>
                    <input type="text" name="username" id="username" class="form-input-create" autocomplete="off" placeholder="Enter your username" value="{{ old('username') }}">
                    @if($errors->has('username'))
                        <div class="danger" style="color: red;">{{ $errors->first('username') }}</div>
                    @endif
                </div>
                <div class="mb-3 form-create">
                    <label for="">Email address</label>
                    <input type="email" name="email" id="email" class="form-input-create" autocomplete="off" placeholder="Enter your email" value="{{ old('email') }}">
                    @if($errors->has('email'))
                        <div class="danger" style="color: red;">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="mb-3 form-create">
                    <label for="">Password</label>
                    <input type="password" name="password" id="password" class="form-input-create" autocomplete="off" placeholder="Enter your password" value="{{ old('password') }}">
                    @if($errors->has('password'))
                        <div class="danger" style="color: red;">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                <div class="mb-3 form-create">
                    <label for="">No. Whatsapp</label>
                    <input type="number" name="whatsapp" id="whatsapp" class="form-input-create" autocomplete="off" value="{{ old('whatsapp') }}" placeholder="Enter your whatsapp">
                    @if($errors->has('whatsapp'))
                        <div class="danger" style="color: red;">{{ $errors->first('whatsapp') }}</div>
                    @endif
                </div>
                <div class="mb-3 form-create">
                    <div class="row">
                        <div class="col-6">
                            <div class="inputfile-custom">
                                <label for="">Upload KTP</label>
                                <input type="file" name="ktp" id="ktpfile" accept="image/*">
                                <label for="ktpfile" class="label-file-custom">
                                    <i class="fa fa-cloud-upload-alt"></i>
                                    Unggah
                                </label>
                            </div>
                            @if($errors->has('ktp'))
                                <div class="danger" style="color: red;">{{ $errors->first('ktp') }}</div>
                            @endif
                        </div>
                        <div class="col-6">
                            <div class="inputfile-custom">
                                <label for="">Upload SIM</label>
                                <input type="file" name="sim" id="simfile" accept="image/*">
                                <label for="simfile" class="label-file-custom">
                                    <i class="fa fa-cloud-upload-alt"></i>
                                    Unggah
                                </label>
                            </div>
                            @if($errors->has('sim'))
                                <div class="danger" style="color: red;">{{ $errors->first('sim') }}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="mb-4 form-create">
                    <label for="">NIK</label>
                    <input type="number" name="nik" id="nik" class="form-input-create" autocomplete="off" placeholder="Enter your nik" value="{{ old('nik') }}">
                    @if($errors->has('nik'))
                        <div class="danger" style="color: red;">{{ $errors->first('nik') }}</div>
                     @endif
                </div>
                <div class="mt-3">
                    <button class="btn w-100 font-500" style="background:#E96E00;color:white" type="submit">Sign Up</button>
                </div>
                <a href="" style="color: #E18028; font-size: 16px;">Butuh Tutorial Pendaftaran ? </a>
            </form>
        </div>
    </div>
</div>

@endsection