@extends('layout.driver', ['title' => 'Driver - Dcreativ'])

@push('style')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

@endpush

@section('content')

<div style="background:#E96E00;border:0;height:100vh;overflow:hidden">
    <div class="container py-5" style="padding-left:3em;padding-right:3em;">
        <div class="mt-4 text-center">
            <img src="{{ asset('images/oops.png') }}" alt="" class="animate__animated animate__fadeInUp" style="width:80%">
        </div>
        <div class="text-center" style="margin-top:10vh">
            <h1 class="text-white animate__animated animate__fadeInUp animate__delay-1s" style="font-size:23px;line-height:45px;font-weight:600">
                Sistem Sedang Dalam Pengembangan
            </h1>
        </div>
        <div class="text-center" style="margin-top:14vh">
            <a href="#" onclick="window.history.back()" class="btn w-100 font-500 animate__animated animate__fadeInUp animate__delay-1s" style="background:white;color:#E96E00">KEMBALI</a>
        </div>
    </div>
</div>

@endsection