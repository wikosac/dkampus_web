@extends('layout.template')

@section('contentkonsumen')

<div class="main-content">
    <div class="pink-page-heading">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 m-lg-auto">
              <h1>DI-Very</h1>
              <span><a href="/konsumen">Home</a>Daftar Usaha Anda</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="shop-page">
        <div class="container">
          <div class="row">
            <div class="col-lg-8">
              <div class="shop-products">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="course-item">
                      <a href="/profiltoko">
                        <div class="course-thumb">
                          {{-- <div class="ribon">
                            <h6>sale</h6>
                          </div> --}}
                          <img src="images/course-item-01.jpg" alt="">
                        </div>
                        <div class="down-content">
                          <h4>Bakso Bakar</h4>
                          {{-- <span><em>$49.99</em>$29.99</span> --}}
                          <span>$29.99</span>
                          <p>Shaman synth retro slow-carb vape and dermy twee, put a jean shorts franzen.</p>
                        </div>
                      </a>
                    </div>
                  </div>



                  <div class="col-lg-12">
                    <div class="shop-pagination">
                      <ul>
                        <li class="active"><a href="#">1</a></li>
                        <li ><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="shop-sidebar">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="shop-widget search">
                      <form>
                          <input type="search" value="" placeholder="Type to search..." required="">
                          <button type="submit" class="primary-button"><i class="fa fa-search"></i></button>
                      </form>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="shop-widget categories">
                      <div class="shop-heading">
                        <h4>Categories</h4>
                      </div>
                      <ul>
                        <li><a href="#">Tips &amp; Tricks<span>(2)</span></a></li>
                        <li><a href="#">Keywords<span>(5)</span></a></li>
                        <li><a href="#">SEO Optimization<span>(4)</span></a></li>
                        <li><a href="#">Digital Marketing<span>(8)</span></a></li>
                        <li><a href="#">E-Books PDF<span>(9)</span></a></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
</div>
@endsection
