@extends('layout.template')

@section('content')

<div class="main-content">
    <div class="main-content">
        <div class="pink-page-heading">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 m-lg-5">
                  <h1>DI-Very</h1>
                  <span><a href="/konsumen">Home</a>Daftar Usaha Anda</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <section class="contact-us">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 m-lg-5">
                  <div class="inner-content">
                    <section class="about-tips our-team-page">
                        <div class="container">
                          <div class="row">
                            <div class="col-lg-6 align-self-center">
                              <div class="video-thumb">
                                <a href="http://youtube.com"><i class="fa fa-play"></i></a>
                                <img src="images/video-thumb.jpg" alt="">
                              </div>
                            </div>


                            <div class="col-lg-6">
                              <div class="section-heading">
                                <h2>Baso Aci Tulang Rangu/Tulang Rawan Abah Acong</h2>
                              </div>
                              <div>

                                <div class="container">
                                  <div class="row">
                                    <div class="col-lg-12 ">
                                      <div class="inner-content">
                                        <h4>Rp.40.000</h4>

                                      </div>
                                    </div>
                                  </div>

                                </div>


                                  <table class="table table-responsive table-bordered">

                                      <tr>
                                          <td>Alamat Toko</td>
                                          <td>:</td>
                                          <td>Semuli Jaya</td>
                                      </tr>
                                      <tr>
                                        <td>Berat</td>
                                        <td>:</td>
                                        <td>1kg</td>
                                    </tr>
                                    <tr>
                                        <td>Jam Buka</td>
                                        <td>:</td>
                                        <td>Pukul . 07.00 WIB</td>

                                        <td>Sampai Dengan</td>

                                        <td>22.00 WIB</td>

                                    </tr>


                                  </table>

                              </div>

                              <div class="main-purple-button">
                                <a href="delivery">Beli</a>
                              </div>


                            </div>
                          </div>
                        </div>
                      </section>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="contact-us">

          </section>
</div>


@endsection
