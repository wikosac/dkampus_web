
@forelse($transaksis as $transaksi)
<div class="card">
    <div class="card-body">
        <div class="d-flex align-items-center mb-3">
            <div class="" style="font-weight:600;font-size:16px">
                <i class="bx bx-shopping-bag"></i> Belanja
            </div>
            <div class="ms-2">
                {{ date_format(date_create($transaksi->created_at), 'd M Y') }}
            </div>
            <div class="ms-2">
                @if($transaksi->status == "selesai")
                <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-success">{{ $transaksi->status }}</span>
                @elseif($transaksi->status == "belum")
                <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-info">{{ $transaksi->status }}</span>
                @elseif($transaksi->status == "proses")
                <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-warning">Sedang di{{ $transaksi->status }}</span>
                @endif
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-6 mb-3">
                
        @foreach($transaksi->orderans as $orderan)
                <div class="d-flex mb-2 align-items-center">
                    <div class="" style="width:80px">
                    <img src="{{ url($orderan->produk->url_image) }}" data-src="{{ url($orderan->produk->url_image) }}" class="lazy w-100" style="height:80px;object-fit:cover" alt="Image">
                    </div>
                    <div class="ms-3" style="flex:1">
                        <div style="font-size:17px;font-weight:600">{{ $orderan->produk->nama_produk }}</div>
                        {{ $orderan->produk->umkm->nama_toko }} ({{ $orderan->produk->umkm->kecamatan }})
                        <div class="">
                            {{ $orderan->qty }} barang x Rp{{ number_format($orderan->produk->harga,0,',','.')}}
                        </div>
                    </div>
                </div>
        @endforeach
        </div>
        <div class="col-lg-6 mb-3">
            <h6>Total Belanja</h6>
            <h3><strong>Rp{{ number_format($transaksi->total,0,',','.')}}</strong></h3>
            @if($transaksi->status != 'selesai' && $transaksi->status != 'proses')
            <div class="mt-3">
                <p class="text-danger">Pesanan yang dipesan tidak dapat dibatalkan</p>
            </div>
            @endif
        </div>
    </div>
</div>
    </div>
    @empty
        <div class="text-center mt-5">Kamu belum memesan apapun</div>
    @endforelse
        