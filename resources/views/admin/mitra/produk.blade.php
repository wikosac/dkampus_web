@extends('layout.templateadmin')



@section('content')



<div class="row">

    <div class="col-lg-12">



    @if(session('error'))

    <div class="alert alert-danger">{{ session('error') }}</div>

    @elseif(session('success'))

    <div class="alert alert-success">{{ session('success') }}</div>

    @endif



        <div class="card">

            <div class="card-header">

                <h4 class="card-title">Produk {{ $mitra->nama }}</h4>

            </div>

            <div class="card-body">

                <div class="table-responsive">

                    <table class="table table-responsive-xs">

                        <thead>

                            <tr>

                                <th style="width:200px">Foto</th>

                                <th>Nama Produk</th>

                                <th>Harga</th>

                                <th>Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                        @forelse($mitra->produks as $produk)

                            <tr>

                                <td style="width:200px"><img src="{{ asset('foto_produk/'.$produk->foto) }}" alt="" class="w-100"></td>

                                <td>{{ $produk->nama }}</td>

                                <td>Rp {{ number_format($produk->harga, 2, ',', '.') }}</td>

                                <td>

                                    <a href="{{ url('/admin/mitra/produk/edit/'.$produk->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Edit</a>
                                    <a href="{{ url('/admin/mitra/produk/delete/'.$produk->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>

                                </td>

                            </tr>

                        @empty

                        <tr>

                            <td class="text-center" colspan="4">Belum ada produk di UMKM ini</td>

                        </tr>

                        @endforelse

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



@endsection

