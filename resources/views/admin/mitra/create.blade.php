@extends('layout.templateadmin')

@section('content')
<div class="main-content">
<div class="row">
    <div class="col-lg-12">

        <form action="{{ url('admin/mitra/produk/store') }}" method="post" enctype="multipart/form-data">

            <div class="row">
                @csrf

                <div class="col-lg-12 m-md-2">
                    <label for="" class="form-label">Foto Produk</label><br>
                    <input class="" type="file" id="formFile" name="gambar_produk">
                    <div class="text-denger">
                        @error('gambar_produk')
                        {{ $message }}
                        @enderror
                    </div>
                 </div>

                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="nama_produk" class="form-label">Nama Produk</label>
                    <input name="nama" type="text" class="form-control" id="nama_produk" placeholder="Nama Produk" required="" value="{{ old('nama_produk') }}">
                    <div class="text-denger">
                        @error('nama_produk')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="harga" class="form-label">Harga</label>
                    <input name="harga" type="number" class="form-control" id="harga" placeholder="Harga Produk" required="" value="{{ old('harga') }}">
                    <div class="text-denger">
                        @error('harga')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="deskripsi" class="form-label">Deskripsi</label>
                    <textarea class="form-control" cols="10" rows="10" name="deskripsi">{{ old('deskripsi') }}</textarea>
                    <div class="text-denger">
                        @error('deskripsi')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="link_tokped" class="form-label">Link Produk Tokopedia</label>
                    <input name="link_tokped" type="text" class="form-control" id="link_tokped" placeholder="Link Tokped" value="{{ old('link_tokped') }}">
                    <div class="text-denger">
                        @error('link_tokped')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="link_shopee" class="form-label">Link Produk Shopee</label>
                    <input name="link_shopee" type="text" class="form-control" id="link_shopee" placeholder="Link Shopee" value="{{ old('link_shopee') }}">
                    <div class="text-denger">
                        @error('link_shopee')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="link_bukalapak" class="form-label">Link Produk Bukalapak</label>
                    <input name="link_bukalapak" type="text" class="form-control" id="link_bukalapak" placeholder="Link Bukalapak" value="{{ old('link_bukalapak') }}">
                    <div class="text-denger">
                        @error('link_bukalapak')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                    
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="mitra" class="form-label">Produk dari Mitra</label>
                        <select name="mitra" class="form-control">
                            @foreach($mitras as $mitra)
                                <option value="{{ $mitra->id }}">{{ $mitra->nama }}</option>
                            @endforeach
                        </select>
                    </div>




            <div class="form-group mt-2 ml-3">
                <button class="btn btn-primary btn-sm" type="submit">Simpan Produk</button>
            </div>

        </form>
    </div>
</div>
</div>



@endsection
