@extends('layout.templateadmin')



@section('content')



<div class="row">

    <div class="col-lg-12">



    @if(session('error'))

    <div class="alert alert-danger">{{ session('error') }}</div>

    @elseif(session('success'))

    <div class="alert alert-success">{{ session('success') }}</div>

    @endif



        <div class="card">

            <div class="card-header">

                <h4 class="card-title">Semua Mitra</h4>

            </div>

            <div class="card-body">

                <div class="table-responsive">

                    <table class="table table-responsive-xs">

                        <thead>

                            <tr>

                                <th>Foto Mitra</th>

                                <th>Nama Mitra</th>

                                <th style="text-align:center">Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                        @foreach($mitras as $mitra)

                            <tr>

                                <td><img src="{{ asset('foto_mitra/'.$mitra->foto) }}" style="width:80px"></td>

                                <td>{{ $mitra->nama }}</td>

                                <td>

                                    <div class="d-flex">

                                        <a href="{{ url('/admin/mitra/edit/'.$mitra->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Edit</a>

                                        <a href="{{ url('/admin/mitra/'.$mitra->id.'/produks') }}" class="btn btn-primary shadow btn-xs mx-1 my-1">Produk</a><br/>

                                        <a href="https://api.whatsapp.com/send?phone=62{{ $mitra->no_wa }}&text=Selamat anda telah terdaftar" class="btn btn-info shadow btn-xs mx-1 my-1">Chat</a>

                                        <a href="{{ url('/admin/mitra/delete/'.$mitra->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>

                                    </div>

                                </td>

                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>



@endsection

