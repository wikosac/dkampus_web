@extends('layout.templateadmin')

@section('content')
<div class="main-content">
<div class="row">
    <div class="col-lg-12">

        <form action="{{ url('admin/mitra/edit') }}" method="post" enctype="multipart/form-data">

            <div class="row">
                @csrf

                <!-- <div class="col-lg-12 m-md-2">
                    <label for="" class="form-label">Foto</label><br>
                    <input class="" type="file" id="formFile" name="foto">
                    <div class="text-denger">
                        @error('foto')
                        {{ $message }}
                        @enderror
                    </div>
                 </div> -->

                 <input type="hidden" name="id" value="{{ $mitra->id }}">
                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="nama_produk" class="form-label">Nama</label>
                    <input name="nama" type="text" class="form-control" id="nama_produk" placeholder="Nama" required="" value="{{ $mitra->nama }}">
                    <div class="text-denger">
                        @error('nama')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input name="alamat" type="text" class="form-control" id="alamat_produk" placeholder="alamat" required="" value="{{ $mitra->alamat }}">
                    <div class="text-denger">
                        @error('alamat')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="tokopedia" class="form-label">Tokopedia</label>
                    <input name="tokopedia" type="text" class="form-control" id="tokopedia_produk" placeholder="tokopedia" required="" value="{{ $mitra->tokopedia }}">
                    <div class="text-denger">
                        @error('tokopedia')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="bukalapak" class="form-label">Bukalapak</label>
                    <input name="bukalapak" type="text" class="form-control" id="bukalapak_produk" placeholder="bukalapak" required="" value="{{ $mitra->bukalapak }}">
                    <div class="text-denger">
                        @error('bukalapak')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="shopee" class="form-label">Shopee</label>
                    <input name="shopee" type="text" class="form-control" id="shopee_produk" placeholder="shopee" required="" value="{{ $mitra->shopee }}">
                    <div class="text-denger">
                        @error('shopee')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="ig" class="form-label">Instagram</label>
                    <input name="ig" type="text" class="form-control" id="ig_produk" placeholder="ig" required="" value="{{ $mitra->ig }}">
                    <div class="text-denger">
                        @error('ig')
                        {{ $message }}
                        @enderror
                    </div>
                </div>
                 <div class="col-lg-12 col-md-12 col-sm-12">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <textarea name="keterangan" cols="30" rows="10" class="form-control">{{ $mitra->keterangan }}</textarea>
                    <div class="text-denger">
                        @error('keterangan')
                        {{ $message }}
                        @enderror
                    </div>
                </div>


            <div class="form-group mt-2 ml-3">
                <button class="btn btn-primary btn-sm" type="submit">Perbarui</button>
            </div>

        </form>
    </div>
</div>
</div>



@endsection
