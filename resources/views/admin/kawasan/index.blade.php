@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua Kawasan</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>Icon</th>
                                <th>Nama</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($kawasans as $kawasan)
                            <tr>
                                @if ($kawasan->icon == null)
                                <td><img src="{{asset('images/icons/default.svg')}}"></td>
                                @else
                                <td><img src="{{asset('images/icons/' . $kawasan->icon )}}"></td>
                                @endif
                                <td>{{ $kawasan->nama }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ url('/admin/kawasan/edit/'.$kawasan->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Edit</a>
                                        <a href="{{ url('/admin/kawasan/delete/'.$kawasan->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
