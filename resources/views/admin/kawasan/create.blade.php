@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Tambah Kawasan</h4>
            </div>
            <div class="card-body">
                <form action="{{ url('/admin/kawasan/add') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4">
                            <input type="file" name="icon" id="icon" class="form-control" accept="image/*">
                        </div>
                        <div class="col-lg-4">
                            <input type="text" placeholder="Nama kawasan" name="nama" id="nama" class="form-control">
                        </div>
                        <div class="col-lg-4 mt-2">
                            <input type="submit" value="Input" class="btn btn-md btn-primary w-100">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
