@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua Ongkir</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>Jarak</th>
                                <th>Harga</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($ongkirs as $ongkir)
                            <tr>
                                <td>{{ $ongkir->jarak }} Km</td>
                                <td>Rp {{ number_format($ongkir->harga, 2, ',','.') }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ url('/admin/ongkir/delete/'.$ongkir->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
