@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Input Ongkir</h4>
            </div>
            <div class="card-body">
                <form action="{{ url('/admin/ongkir/input/store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4 mb-2">
                            <label for="jarak">Jarak (km)</label>
                            <input type="text" name="jarak" id="jarak" class="form-control">
                        </div>
                        <div class="col-lg-4 mb-2">
                            <label for="harga">Harga (Rupiah)</label>
                            <input type="text" name="harga" id="harga" class="form-control">
                        </div>
                        <div class="col-lg-12 mt-2">
                            <input type="submit" value="Input" class="btn btn-md btn-primary w-100">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
