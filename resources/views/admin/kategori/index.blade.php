@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua Kategori</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>icon</th>
                                <th>Nama</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($kategoris as $kategori)
                            <tr>
                                <td><img src="{{asset('images/kategori/'. $kategori->icon)}}" alt="{{$kategori->icon}}" style="width:50%;height:50%;"></td>
                                <td>{{ $kategori->nama }}</td>
                                <td>
                                    <div class="d-flex justify-content-center">
                                        <a href="{{ url('/admin/kategori/edit/'.$kategori->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Edit</a>
                                        <a href="{{ url('/admin/kategori/delete/'.$kategori->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
