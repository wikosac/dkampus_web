@extends('layout.templateadmin')
@section('content')

@if(session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@elseif(session('success'))
<div class="alert alert-success">{{ session('success') }}</div>
@endif
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Semua Transaksi Toko</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-responsive-xs">
                <thead>
                    <tr>
                        <th>Nama Toko</th>
                        <th>Nama Produk</th>
                        <th>Jumlah Pesanan</th>
                        <th>Tanggal Pemesanan</th>
                        <th>Total</th>
                </tr>
            </thead>
            <tbody>
            @foreach($transaksiToko as $transaksi)
                <tr>
                    <td>{{ $transaksi->nama_toko }}</td>
                    <td>{{$transaksi->nama_produk}}</td>
                    <td>{{$transaksi->qty}}</td>
                    <td>Rp.{{number_format($transaksi->total)}}</td>
                    <td>{{Carbon\Carbon::parse($transaksi->created_at)->format('d / M / Y')}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>

@endsection