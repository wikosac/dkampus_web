@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Semua Transaksi Driver</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-responsive-xs">
                            <thead>
                                <tr>
                                    <th>Nama Driver</th>
                                    <th>Biaya Penanganan</th>
                                    <th>Administrasi Ojek</th>
                                <th>Total</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($transaksis as $transaksi)
                            <tr>
                                <td>{{ $transaksi->fullname }}</td>
                                <td>Rp {{ number_format($transaksi->total_order * 1000, 2, ',', '.') }}</td>
                                <td>Rp {{ number_format($transaksi->fee_ojek * 100, 2, ',', '.') }}</td>
                                <td>Rp {{ number_format($transaksi->fee_ojek * 100 + $transaksi->total_order * 1000, 2, ',', '.') }}</td>
                                <td>
                                    <a href="https://api.whatsapp.com/send?phone=62{{ $transaksi->no_telephone }}" class="btn btn-success btn-sm">Chat Driver</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
