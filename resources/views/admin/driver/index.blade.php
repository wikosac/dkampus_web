@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua Driver</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>No. Whatsapp</th>
                                <th>Tanggal Gabung</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($drivers as $driver)
                            <tr>
                                <td>{{ $driver->fullname }}</td>
                                <td>{{ $driver->no_telephone }}</td>
                                <td>{{ date_format(date_create($driver->created_at), 'd/m/Y') }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ url('/admin/driver/detail/'.$driver->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Detail</a>
                                        <a href="https://api.whatsapp.com/send?phone=62{{ $driver->no_telephone }}&text=Selamat anda telah tergabung kedalam driver Dcreativ Dilevery" class="btn btn-info shadow btn-xs mx-1 my-1">Chat</a>
                                        <a href="{{ url('/admin/driver/delete/'.$driver->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1" onclick="return confirm('Anda yakin ingin menghapus produk ini?')">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
