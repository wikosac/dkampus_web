@extends('layout.templateadmin')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Detail Driver</h4>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12 font-600">Nama Lengkap</div>
                    <div class="col-lg-12">{{ $driver->fullname }}</div>
                </div>
                <div class="row mb-4">
                    <div class="col-lg-6 mb-2">
                        <div class="font-600">Foto KTP</div>
                        <div class=""><img src="{{ asset('foto_ktp/'.$driver->ktp) }}" alt="" style="width:150px"></div>
                    </div>
                    <div class="col-lg-6 mb-2">
                        <div class="font-600">Foto SIM</div>
                        <div class=""><img src="{{ asset('foto_sim/'.$driver->sim) }}" alt="" style="width:150px"></div>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-lg-12 font-600">NIK</div>
                    <div class="col-lg-12">{{ $driver->nik }}</div>
                </div>
                @if($driver->status == 'belum')
                <div class="row">
                    <div class="col-lg-12" style="text-align:right">
                        <a href="{{ url('/admin/driver/approve/'.$driver->id) }}" class="btn btn-primary btn-md">Approve</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
