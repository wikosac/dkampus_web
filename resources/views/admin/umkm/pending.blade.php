@extends('layout.templateadmin')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Pending UMKM</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>Nama Toko</th>
                                <th>Alamat</th>
                                <th>No. Whatsapp</th>
                                <th>Tanggal Gabung</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($umkms as $umkm)
                            <tr>
                                <td>{{ $umkm->nama_toko }}</td>
                                <td>{{ $umkm->kecamatan }}</td>
                                <td>{{ $umkm->no_telephone }}</td>
                                <td>{{ date_format(date_create($umkm->created_at), 'd/m/Y') }}</td>
                                <td>
                                    <a href="{{ url('/admin/umkm/detail/'.$umkm->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Detail</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>   
                    </table>
                    </div>  
            </div>
        </div>
    </div>
</div>

@endsection
