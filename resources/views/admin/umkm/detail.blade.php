@extends('layout.templateadmin')

@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Detail UMKM</h4>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12 font-600">Nama Toko</div>
                    <div class="col-lg-12">{{ $umkm->nama_toko }}</div>
                </div>
                <div class="row mb-4">
                    <div class="col-lg-4 mb-2">
                        <div class="font-600">Provinsi</div>
                        <div class="">{{ $umkm->provinsi }}</div>
                    </div>
                    <div class="col-lg-4 mb-2">
                        <div class="font-600">Kabupaten</div>
                        <div class="">{{ $umkm->kabupaten }}</div>
                    </div>
                    <div class="col-lg-4 mb-2">
                        <div class="font-600">Kecamatan</div>
                        <div class="">{{ $umkm->kecamatan }}</div>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-lg-12 font-600">Keterangan</div>
                    <div class="col-lg-12">{{ $umkm->keterangan }}</div>
                </div>
                <div class="row mb-4">
                    <div class="col-lg-6 mb-2">
                        <div class="font-600">Foto KTP</div>
                        <div class=""><img src="{{ asset('foto_ktp/'.$umkm->gambar_ktp) }}" alt="" style="width:150px"></div>
                    </div>
                    <div class="col-lg-6 mb-2">
                        <div class="font-600">NIK</div>
                        <div class="">{{ $umkm->nik }}</div>
                    </div>
                </div>
                @if($umkm->status == 'belum')
                <div class="row">
                    <div class="col-lg-12" style="text-align:right">
                        <a href="{{ url('/admin/umkm/approve/'.$umkm->id) }}" class="btn btn-primary btn-md">Approve</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
