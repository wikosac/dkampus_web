@extends('layout.templateadmin')

@section('content')

<div class="row">
    <div class="col-lg-12">

    @if(session('error'))
    <div class="alert alert-danger">{{ session('error') }}</div>
    @elseif(session('success'))
    <div class="alert alert-success">{{ session('success') }}</div>
    @endif

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua UMKM</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-responsive-xs">
                        <thead>
                            <tr>
                                <th>Nama Toko</th>
                                <th>Alamat</th>
                                <th>No. Whatsapp</th>
                                <th>Tanggal Gabung</th>
                                <th style="text-align:center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($umkms as $umkm)
                            <tr>
                                <td>{{ $umkm->nama_toko }}</td>
                                <td>{{ $umkm->kecamatan }}</td>
                                <td>{{ $umkm->no_telephone }}</td>
                                <td>{{ date_format(date_create($umkm->created_at), 'd/m/Y') }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ url('/admin/umkm/detail/'.$umkm->id) }}" class="btn btn-success shadow btn-xs mx-1 my-1">Detail</a>
                                        <a href="{{ url('/admin/umkm/'.$umkm->id.'/produk') }}" class="btn btn-primary shadow btn-xs mx-1 my-1">Produk</a><br/>
                                        <a href="https://api.whatsapp.com/send?phone=62{{ $umkm->no_telephone }}&text=Selamat anda telah terdaftar" class="btn btn-info shadow btn-xs mx-1 my-1">Chat</a>
                                        <a href="{{ url('/admin/umkm/delete/'.$umkm->id) }}" class="btn btn-danger shadow btn-xs mx-1 my-1">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
