@extends('layout.template')

@section('content')



      <div class="main-content">


        <!-- Main Banner -->
      <div class="parallax-banner">
        <!--Content before waves-->
        <div class="inner-header">
          <div class="inner-content">
            <div class="row">
            <div class="col-lg-6 align-self-center">
            <h2> Mau bisnis mu terus   berkembang dan maju?</h2>
            <h3>Jika jawaban anda adalah MAU, anda butuh D'Creativ untuk Penjualan Digital bisnis anda.</h3>
          </div>
          <div class="col-lg-6 align-self-center">
            <div class="main-decoration ">
              <img src="{{ asset('assets/images/DIToko.png') }}" alt="">
            </div>
            </div>
          </div>
          </div>
        </div>

        <!--Waves Container-->
        <div>
          <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
          viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
          <defs>
          <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
          </defs>
          <g class="parallax">
          <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
          <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
          <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
          <use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
          </g>
          </svg>
        </div>
        <!--Waves end-->
      </div>

      <!-- Features -->
      <section class="features">
        <div class="container">
          <div class="row">
            <!-- <div class="col-lg-4">
              <div class="feature-item">
                <div class="icon">
                  <img src="images/features-icon-01.png" alt="">
                </div>
                <h4>Actual Real Results</h4>
                <p>Shaman synth retro slow-carb vape dermy, put and a bird jean shorts franzen unicorn knausgaard coloring book.</p>
              </div>
              <div class="main-purple-button">
                <a href="#">Continue Reading</a>
              </div>
            </div> -->
            <div class="col-lg-4 m-auto">
              <div class="feature-item">
                <div class="icon">
                  <img src="{{ asset('assets/images/Toko.png') }}" alt="">
                </div>
                <h4>D-Toko</h4>
                <p>Merupakan Layanan UMKM untuk menjual produk secara Digital dengan mudah</p>

              </div>
              <div class="main-purple-button">
                <a href="{{ url('/dtoko') }}">Ayo Pergi!</a>
              </div>
            </div>
            <!-- <div class="col-lg-4">
              <div class="feature-item">
                <div class="icon">
                  <img src="images/features-icon-03.png" alt="">
                </div>
                <h4>Easy Useful Dashboard</h4>
                <p>Shaman synth retro slow-carb vape dermy, put and a bird jean shorts franzen unicorn knausgaard coloring book.</p>
              </div>
              <div class="main-purple-button">
                <a href="#">Continue Reading</a>
              </div>
            </div> -->
          </div>
        </div>
      </section>

        <!-- Good Tips -->
      <section class="good-tips">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="section-heading">
                <h6>Tentang Kami</h6>
                <h2>D’Creativ Indonesia</h2>
                <p>Kami hadir sebagai pionir Startup Digital Agency untuk membantu pelaku bisnis Micro-Small-Medium Enterprise (UMKM) hingga perusahaan kelas atas untuk memperluas jangkauan pasar dan meningkatkan branding terutama di Social Media. </p>
              </div>

            </div>
            <div class="col-lg-6 align-self-center">
              <div class="right-image">
                <img src="{{ asset('assets/images/Marketing Agency.png') }}" alt="">
              </div>
            </div>
          </div>
        </div>
      </section>




      {{-- <!-- Clients Love -->
      <section class="clients-love">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="section-heading">
                <h6>Testimoni</h6>
                <h2>D'Creativ</h2>
              </div>
            </div>
            <div class="col-lg-10 offset-lg-1">
              <div class="owl-testimonials owl-carousel">
                <div class="item">
                  <div class="testimonial-item">
                    <div class="image">
                      <img src="http://placehold.it/90x90" alt="">
                    </div>
                    <h4>James D. Lapierre</h4>
                    <span>Full-stack Designer</span>
                    <p>“Shaman synth retro slowcarbvape taxiderm twee, put a bird on it jean shorts franzen knausgaard coloring book.”</p>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial-item">
                    <div class="image">
                      <img src="http://placehold.it/90x90" alt="">
                    </div>
                    <h4>Kevin L. Phipps</h4>
                    <span>General Manager</span>
                    <p>“Shaman synth retro slowcarbvape taxiderm twee, put a bird on it jean shorts franzen knausgaard coloring book.”</p>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial-item">
                    <div class="image">
                      <img src="http://placehold.it/90x90" alt="">
                    </div>
                    <h4>Virgilio J. Dumas</h4>
                    <span>Full-stack Developer</span>
                    <p>“Shaman synth retro slowcarbvape taxiderm twee, put a bird on it jean shorts franzen knausgaard coloring book.”</p>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial-item">
                    <div class="image">
                      <img src="http://placehold.it/90x90" alt="">
                    </div>
                    <h4>Charles K. Duckett</h4>
                    <span>Creative Director</span>
                    <p>“Shaman synth retro slowcarbvape taxiderm twee, put a bird on it jean shorts franzen knausgaard coloring book.”</p>
                  </div>
                </div>
                <div class="item">
                  <div class="testimonial-item">
                    <div class="image">
                      <img src="http://placehold.it/90x90" alt="">
                    </div>
                    <h4>Nicholas S. Rodriguez</h4>
                    <span>Sale Agent</span>
                    <p>“Shaman synth retro slowcarbvape taxiderm twee, put a bird on it jean shorts franzen knausgaard coloring book.”</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> --}}



</div>




@endsection
