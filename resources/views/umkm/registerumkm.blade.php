@extends('layout.template')

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/select2-4.0.6-rc.1/dist/css/select2.min.css')}}">
    <link
      href="{{ asset('frontend/css/bootstrap.min.css') }}"
      id="bootstrap-style"
      rel="stylesheet"
      type="text/css"
    />
@endpush

@section('content')


<div class="main-content">
<!-- Pink Heading -->
<div class="pink-page-heading">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 m-lg-auto">
          <h1>Daftar Usaha Anda</h1>
          <span><a href="/">Home</a>Daftar Usaha Anda</span>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- Contact Us -->
    <section class="contact-us m-md-5" >
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="inner-content">
                <div class="block-heading">
                  <h4>Daftarkan Usaha Anda </h4>
                  {{-- {{ session()->get('user_id') }} --}}
                </div>
                <form action="{{ url('umkm/insert') }}" method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <input name="nama_toko" type="text" class="form-control" id="nama_toko" placeholder="Nama Toko" required="" value="{{ old('nama_toko') }}">
                        <div class="text-denger">
                            @error('nama_toko')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

              <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                <label class="form-label">Provinsi:</label>
                <div class="">
                  <select class="form-control" name="provinsi" id="provinsi">
                    <option></option>
                    @foreach($provinces as $provinsi)
                    <option value="{{$provinsi->name}}" data-id="{{$provinsi->id}}">{{$provinsi->name}}</option>
                    @endforeach
                  </select>
              </div>
              </div>
              <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                <label class="form-label" >Kota/Kabupaten:</label>
                <div class="">          
                  <select class="form-control" name="kota" id="kota">
                    <option></option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                <label class="form-label" >Kecamatan:</label>
                <div class="">          
                  <select class="form-control" name="kecamatan" id="kecamatan">
                    <option></option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-12 col-sm-12 mt-3">
                <label class="form-label" >Kelurahan:</label>
                <div class="">          
                  <select class="form-control" name="kelurahan" id="kelurahan">
                    <option></option>
                  </select>
                </div>
              </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 mt-4">
                        <input name="nowa" type="text" class="form-control" id="nowa" placeholder="Nomor Whatsapp (Aktif)" required="" value="{{ old('nowa') }}">
                        <div class="text-denger">
                            @error('nowa')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <input name="alamat" type="text" class="form-control" id="alamat" placeholder="Alamat Lengkap" required="" value="{{ old('alamat') }}">
                        <div class="text-denger">
                            @error('alamat')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <input name="keterangan" type="text" class="form-control" id="keterangan" placeholder="Keterangan Usaha Anda" required="" value="{{ old('keterangan') }}">
                        <div class="text-denger">
                            @error('keterangan')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>




                    <div class="col-lg-6 ">
                        <label>Uploud KTP </label>
                        <input class="" type="file" id="formFile" name="gambar_ktp">
                      </div>

                      <div class="col-lg-6 col-md-12 col-sm-12">
                        <label>NIK KTP </label>
                        <input name="nik" type="number" class="form-control" id="nik" placeholder="NIK" required="" value="{{ old('nik') }}">
                        <div class="text-denger">
                            @error('nik')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                      <div class="form-group">
                        <button class="btn btn-primary btn-sm" type="submit">Daftarkan</button>
                    </div>


                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    @endsection

    @push('scripts')

<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/select2.min.js') }}"></script>   
<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/i18n/id.js') }}"></script> 
<script src="{{ asset('assets/js/register.js') }}"></script>

@endpush
