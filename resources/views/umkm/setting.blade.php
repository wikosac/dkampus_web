@extends('layout.templateumkm')

@section('content')
<div class="main-content">
<div class="row">
    <div class="col-lg-12">
     @if(session('danger'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('danger')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>

    @endif
    </div>
    <div class="col-lg-12">
        <form action="{{ route('umkm.update-setting') }}" method="post">
            @csrf
            <input type="hidden" name="umkmId" value="{{ $umkm->id }}">
            <div class="mb-3">
                <label for="" class="form-label">Latitude</label><br>
                <input type="text" id="lat" class="form-control" value="{{ $umkm->lat }}" name="lat">
                <div class="text-denger">
                    @error('lat')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Longitude</label><br>
                <input type="text" id="lng" class="form-control" value="{{ $umkm->lng }}" name="lng">
                <div class="text-denger">
                    @error('lng')
                    {{ $message }}
                    @enderror
                </div>
            </div>
            <input type="submit" value="Simpan" class="btn btn-primary">
        </form>
        <div class="mb-3 mt-4">
            <div id="map"></div>
        </div>
    </div>
</div>
</div>
@endsection

@push('style')
<style>

    #map{

        height:50vh;

    }

    .custom-map-control-button{

        background:orange;

        color:white;

        padding:10px 30px;

        border:1px solid orange;

        margin-top:43vh;

    }

</style>
@endpush
@push('script')



<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/select2.min.js') }}"></script>   

<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/i18n/id.js') }}"></script> 

<script

src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&callback=initMap&v=weekly"

async

></script>

<script>

  let map, infoWindow;



function initMap() {

navigator.geolocation.getCurrentPosition(

(position) => {

const pos = {

  lat: position.coords.latitude,

  lng: position.coords.longitude,

};



localStorage['lat'] = pos.lat

localStorage['lng'] = pos.lng

console.log("Ajax running")





})



const mapOptions = {

center: {lat:parseFloat({{ $umkm->lat }}) ? parseFloat({{ $umkm->lat }}) : -1.0, lng:parseFloat({{ $umkm->lng }}) ? parseFloat({{ $umkm->lng }}) : 0.0},

zoom:15

}



map = new google.maps.Map(document.getElementById("map"), mapOptions);





infoWindow = new google.maps.InfoWindow();



const marker = new google.maps.Marker({

position: { lat:localStorage['lat'] ? parseFloat(localStorage['lat']) : 0, lng:localStorage['lng'] ? parseFloat(localStorage['lng']) : 0 },

map: map,

});



const locationButton = document.createElement("button");



locationButton.textContent = "Posisikan otomatis";

locationButton.classList.add("custom-map-control-button");

map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);

locationButton.addEventListener("click", () => {

// Try HTML5 geolocation.

if (navigator.geolocation) {

navigator.geolocation.getCurrentPosition(

  (position) => {

    const pos = {

      lat: position.coords.latitude,

      lng: position.coords.longitude,

      accuracy: position.coords.accuracy

    };

    localStorage['lat'] = pos.lat

    localStorage['lng'] = pos.lng

    infoWindow.setPosition(pos);

    infoWindow.setContent("Ini lokasi anda");

    infoWindow.open(map);

    map.setCenter(pos);


    $.ajax({

    method:"POST",

    url:"/umkm/update-location",

    data:{

        lat:pos.lat,

        lng:pos.lng,

        umkmId:{{ $umkm->id }}

    },

    success:function(res){

        console.log("Ajax success")

        console.log(res)

    }

    })


  },

  () => {

    handleLocationError(true, infoWindow, map.getCenter());

  }

);

} else {

// Browser doesn't support Geolocation

handleLocationError(false, infoWindow, map.getCenter());

}

});

}



function handleLocationError(browserHasGeolocation, infoWindow, pos) {

infoWindow.setPosition(pos);

infoWindow.setContent(

browserHasGeolocation

? "Error: Akses lokasi tidak diizinkan."

: "Error: Your browser doesn't support geolocation."

);

infoWindow.open(map);

}




</script>

@endpush
