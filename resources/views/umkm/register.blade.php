@extends('layout.template')

@section('content')


<div class="main-content">
<!-- Pink Heading -->
<div class="pink-page-heading">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 m-lg-auto">
          <h1>Daftar Usaha Anda</h1>
          <span><a href="/">Home</a>Daftar Usaha Anda</span>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- Contact Us -->
    <section class="contact-us m-md-5" >
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              @if(session('error'))

                <div style="background:rgb(255, 111, 111);color:brown;padding:14px 18px;margin-bottom:10px;border-radius:8px">
                    {{ session('error') }}
                </div>

              @endif
              <div class="inner-content">
                <div class="block-heading">
                  <h4>Daftarkan Diri Anda</h4>
                </div>
                <form action="{{ url('umkm/insertuser') }}" method="post">
                    @csrf
                  <div class="row">

                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <input name="nama_depan" type="text" class="form-control" id="nama_depan" placeholder="Nama Depan" required>
                        <div class="text-denger">
                            @error('nama_depan')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <input name="nama_belakang" type="text" class="form-control" id="nama_belakang" placeholder="Nama Belakang" required>
                        <div class="text-denger">
                            @error('nama_belakang')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12">
                      <input name="username" type="username" class="form-control" id="username" placeholder="Username" required>
                      <div class="text-denger">
                        @error('username')
                        {{ $message }}
                        @enderror
                    </div>
                  </div>


                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <input name="email" type="email" class="form-control" id="email" placeholder="Email (Optional)">
                        <div class="text-denger">
                            @error('email')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>

                    <div class="col-6 mb-20"><input class="form-control" type="password" name="password1" placeholder="Password" required></div>


                    <div class="col-6 mb-20"><input class="form-control" type="password"  name="password2" placeholder="Konfirmasi Password" required></div>

                    {{-- <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="" style="margin-bottom:7px;display:block;margin-left:10px">Tanggal Lahir</label>
                        <input name="tanggal" type="date" class="form-control" id="date" placeholder="Tanggal Lahir">
                    </div> --}}

                    <div class="form-group">
                        <button class="btn btn-primary btn-sm" type="submit">Lanjut</button>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    @endsection
