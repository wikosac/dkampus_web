@extends('layout.templateumkm')

@section('content')
<div class="main-content">
<div class="row">
    <div class="col-lg-12">

        <form action="{{ url('umkm/insert_produk') }}" method="post" enctype="multipart/form-data">

            <input type="hidden" name="level" value="{{session('data')->level}}">
            <div class="row">
                @csrf

                <div class="col-lg-12 m-md-2">
                    <label for="" class="form-label">Foto Produk</label><br>
                    <input class="" type="file" id="formFile" name="gambar_produk">
                    <div class="text-denger">
                        @error('gambar_produk')
                        {{ $message }}
                        @enderror
                    </div>
                 </div>

                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="nama_produk" class="form-label">Nama Produk</label>
                    <input name="nama_produk" type="text" class="form-control" id="nama_produk" placeholder="Nama Produk" required="" value="{{ old('nama_produk') }}">
                    <div class="text-denger">
                        @error('nama_produk')
                        {{ $message }}
                        @enderror
                    </div>
                </div>


                <div class="col-lg-6 col-md-12 col-sm-12">
                <label for="rak_produk" class="form-label">Rak Toko</label>
                <select class="form-control"  name="rak_produk" id="rak_produk" required="" value="{{ old('rak_produk') }}">
                    <div class="text-denger">
                        @error('rak_produk')
                        {{ $message }}
                        @enderror
                    </div>
                    @foreach($kategoris as $kategori)
                        <option value="{{ $kategori->nama }}">{{ $kategori->nama }}</option>
                    @endforeach
                </select>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="harga" class="form-label">Harga</label>
                    <input name="harga" type="number" class="form-control" id="harga" placeholder="Harga Produk" required="" value="{{ old('harga') }}">
                    <small>Jangan gunakan tanda , atau . saat mengisi harga. <br/>Cukup ketik besarnya nilai. ex : 10000</small>
                    <div class="text-denger">
                        @error('harga')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="promo" class="form-label">Apakah barang ini promo?</label>
                    <select class="form-control"  name="promo" id="promo">
                        <option value="tidak" >Tidak</option>
                        <option value="iya" >Iya</option>
                    </select>
                    <small>Jika anda akan mengadakan promo terhadap produk anda, maka pilih Iya</small>
                </div>




                {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="satuan" class="form-label">Satuan Pesanan</label>
                    <select class="form-control"  name="satuan" id="satuan" required="" value="{{ old('satuan') }}">
                        <div class="text-denger">
                            @error('satuan')
                            {{ $message }}
                            @enderror
                        </div>
                        <option value="pcs" >PCS </option>
                    </select>
                </div> --}}



                {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="berat" class="form-label">Jumlah berat</label>
                    <input name="berat" type="text" class="form-control" id="berat" placeholder="Jumlah Berat" required="" value="{{ old('berat') }}">
                    <div class="text-denger">
                        @error('berat')
                        {{ $message }}
                        @enderror
                    </div>
                </div> --}}

                    {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                        <label for="berat" class="form-label">Satuan Berat</label>
                        <select class="form-control"  name="berat" id="berat" required="" value="{{ old('berat') }}">
                            <div class="text-denger">
                                @error('berat')
                                {{ $message }}
                                @enderror
                            </div>
                            <option value="pcs " >Gram </option>
                        </select>
                    </div> --}}

                    <div class="col-lg-12 col-md-12 col-sm-12 mb-2">
                        <label for="keterangan" class="form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" cols="10" rows="15" id="keterangan" required>{{ old('keterangan') }}</textarea>
                        <div class="text-denger">
                            @error('keterangan')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="visibilitas" class="form-label">Visibilitas Produk</label>
                        <select name="visibilitas" class="form-control">
                            <option value="terlihat">Terlihat</option>
                            <option value="sembunyikan">Sembunyikan</option>
                        </select>
                    </div>




            <div class="form-group mt-2 ml-3">
                <button class="btn btn-primary btn-sm" type="submit">Simpan Produk</button>
            </div>

        </form>
    </div>
</div>
</div>
@endsection
