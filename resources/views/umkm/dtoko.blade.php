@extends('layout.template')

@section('content')



      <div class="main-content">

        <!-- Main Banner -->
        <div class="parallax-banner">

            <!--Content before waves-->
            <div class="inner-header">
              <div class="inner-content">
                <div class="row">
                <div class="col-lg-6 align-self-center">
                <h1> DI-Toko</h1>
                <h3>Solusi melakukan penjualan dengan mudah dengan berbagai layanan pembuatan web toko mu dengan mudah, desain produk , desain kartu nama, desain poster toko mu semua ini untuk meningkatkan value UMKM anda di dunia digital</h3>
              </div>
              <div class="col-lg-6 align-self-center">
                <div class="main-decoration ">
                  <img src="{{ url('assets/images/DIToko.png') }}" alt="">
                </div>
                </div>
              </div>
              </div>
            </div>

            <!--Waves Container-->
            <div>
              <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
              viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
              <defs>
              <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
              </defs>
              <g class="parallax">
              <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7" />
              <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
              <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
              <use xlink:href="#gentle-wave" x="48" y="7" fill="#fff" />
              </g>
              </svg>
            </div>
            <!--Waves end-->

          </div>





      <!-- About Tips -->
      <section class="about-tips">

        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="section-heading">
                <h6>Fun Fact!</h6>
                <h2>Tahukah anda Bahwa Jika Produk anda Digital Lebih Menguntungkan!</h2>
              </div>
              <p>Jika Anda selama ini hanya menjual produk secara <b>konvensional/di tempat anda saja</b>, maka Anda harus coba meningkatkan penjualan anda dengan Digital.<br>
                Apa yang konsumen butuhkan? Salah satunya adalah <b>“Penjualan Digital”</b>. Karena dengan ini konsumen dapat mudah membeli tanpa datang ke usaha anda.
                </p>

            </div>
            <div class="col-lg-6 align-self-center">

                <img src="{{ url('assets/images/Food Truck 1.png') }}" alt="">

            </div>

          </div>

        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FF9D57" fill-opacity="1" d="M0,128L48,117.3C96,107,192,85,288,106.7C384,128,480,192,576,224C672,256,768,256,864,234.7C960,213,1056,171,1152,170.7C1248,171,1344,213,1392,234.7L1440,256L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>

      </section>


      <!-- Skill -->
      <section class="contact-info">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="info-item">
                <div class="icon">
                    <img src="{{ url('assets/images/like.png') }}" alt="">
                </div>
                <h4>Meningkatkan Trust<br></h4>
                <p>Desain yang professional, salah satu indikator orang akan percaya terhadap brand Anda.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="info-item">
                <div class="icon">
                    <img src="{{ url('assets/images/grafik.png') }}" alt="">
                </div>
                <h4>MeningkatkanPenjualan</h4>
                <p>Semakin banyak Interaksi, semakin banyak transaksi. Kombinasikan keduanya usaha Anda.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="info-item">
                <div class="icon">
                    <img src="{{ url('assets/images/chat.png') }}" alt="">
                </div>
                <h4>Meningkatkan
                    <br>Interaksi</h4>
                <p>Desain yang menarik, akan lebih mudah membuat interaksi dengan konsumen</p>
              </div>
            </div>
          </div>
        </div>
      </section>

 <!-- Features -->
      <section class="features">

        <div class="container">
            <div class="section-heading center">
                <center><h6>Layanan</h6>
                <h2>Yang D’Creativ Sediakan</h2></center>


          <div class="row">

            <div class="col-lg-6 m-auto">
              <div class="feature-item">
                <div class="icon">
                  <img src="{{ url('assets/images/Toko.png') }}" alt="">
                </div>
                <h4>DI- Produk</h4>
                <p>Merupakan Layanan UMKM untuk menjual produk secara Digital dengan mudah</p>

              </div>
              <div class="main-purple-button">
                <a href="{{ url('/umkm/register') }}">Ayo Pergi!</a>
              </div>
            </div>
            <div class="col-lg-6 m-auto">
                <div class="feature-item">
                  <div class="icon">
                    <img src="{{ url('assets/images/Toko.png') }}" alt="">
                  </div>
                  <h4>DI-Desain Produk</h4>
                  <p>Merupakan Layanan UMKM untuk menjual produk secara Digital dengan mudah</p>

                </div>
                <div class="main-purple-button">
                  <a href="{{ url('/error') }}">Coming Soon!</a>
                </div>
              </div>

          </div>
        </div>
      </section>


</div>




@endsection
