@extends('layout.template')
@section('content')

<div class="main-content">

    <!-- Page Heading -->
    <div class="pink-page-heading">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Halaman Sedang Dalam Pengembangan</h1>
            <span><a href="{{ url('/umkm') }}">Beranda</a>Error Page</span>
          </div>
        </div>
      </div>
    </div>


    <!-- Error Page -->
    <section class="error-page">
      <div class="container">
        <div class="col-lg-10 offset-lg-1">
          <div class="error-container">
            <div class="row">
              <div class="col-lg-6 align-self-center">
                <h1>404</h1>
              </div>
              <div class="col-lg-6">
                <h4>Oops! Halaman Sedang Dalam Pengembangan!</h4>
                <p>Shaman synth retro slow-carb. Vape taxidermy twee, putting a bird onixit fran xezen celiac unic knausgaard fingerstache.</p>
                <div class="main-white-button">
                  <a href="{{ url('/umkm') }}">Kembali ke Beranda</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


  </div>

@endsection
