@extends('layout.templateumkm')

@section('content')
<div class="main-content">
<div class="row">
    <div class="col-lg-12">

        <form action="{{ url('umkm/update_produk') }}" method="post" enctype="multipart/form-data">

            <div class="row">
                @csrf
                <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                <div class="col-lg-12 mb-3">
                    <div class="row">
                        <div class="col-lg-1">
                            <img src="{{ url($produk->url_image) }}" alt="{{ $produk->nama_produk }}" class="radius" style="width:100px">
                        </div>
                        <div class="col-lg-10 ml-4">
                            <label for="" class="form-label">Foto Produk (Optional)</label><br>
                            <input class="" type="file" id="formFile" name="gambar_produk">
                        </div>
                    </div>
                 </div>

                 <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="nama_produk" class="form-label">Nama Produk</label>
                    <input name="nama_produk" type="text" class="form-control" id="nama_produk" value="{{ $produk->nama_produk }}" required="" value="{{ old('nama_produk') }}">
                    <div class="text-denger">
                        @error('nama_produk')
                        {{ $message }}
                        @enderror
                    </div>
                </div>


                <div class="col-lg-6 col-md-12 col-sm-12">
                <label for="rak_produk" class="form-label">Rak Toko</label>
                <select class="form-control"  name="rak_produk" id="rak_produk" required="" value="{{ old('rak_produk') }}">
                    <div class="text-denger">
                        @error('rak_produk')
                        {{ $message }}
                        @enderror
                    </div>
                    @foreach($kategoris as $kategori)
                        <option value="{{ $kategori->nama }}" {{$kategori->nama == $produk->rak_produk  ? 'selected' : ''}} >{{ $kategori->nama }}</option>
                    @endforeach
                </select>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="harga" class="form-label">Harga</label>
                    <input name="harga" type="number" class="form-control" id="harga" value="{{ $produk->harga }}" required="" value="{{ old('harga') }}">
                    <div class="text-denger">
                        @error('harga')
                        {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 mb-2 mt-2">
                    <label for="promo" class="form-label">Apakah barang ini promo?</label>
                    <select class="form-control"  name="promo" id="promo">
                        <option value="tidak" {{ ($produk->promo == 'tidak') ? 'selected' : '' }}>Tidak</option>
                        <option value="iya" {{ ($produk->promo == 'iya') ? 'selected' : '' }}>Iya</option>
                    </select>
                    <small>Jika anda akan mengadakan promo terhadap produk anda, maka pilih Iya</small>
                </div>




                {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="satuan" class="form-label">Satuan Pesanan</label>
                    <select class="form-control"  name="satuan" id="satuan" required="" value="{{ old('satuan') }}">
                        <div class="text-denger">
                            @error('satuan')
                            {{ $message }}
                            @enderror
                        </div>
                        <option value="pcs" >PCS </option>
                    </select>
                </div> --}}



                {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                    <label for="berat" class="form-label">Jumlah berat</label>
                    <input name="berat" type="text" class="form-control" id="berat" value="Jumlah Berat" required="" value="{{ old('berat') }}">
                    <div class="text-denger">
                        @error('berat')
                        {{ $message }}
                        @enderror
                    </div>
                </div> --}}

                    {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                        <label for="berat" class="form-label">Satuan Berat</label>
                        <select class="form-control"  name="berat" id="berat" required="" value="{{ old('berat') }}">
                            <div class="text-denger">
                                @error('berat')
                                {{ $message }}
                                @enderror
                            </div>
                            <option value="pcs " >Gram </option>
                        </select>
                    </div> --}}

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="keterangan" class="form-label">Keterangan</label>
                        <textarea name="keterangan" class="form-control" cols="10" rows="15" id="keterangan" required>{{ $produk->keterangan }}</textarea>
                        <div class="text-denger">
                            @error('keterangan')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="visibilitas" class="form-label">Visibilitas Produk</label>
                        <select name="visibilitas" class="form-control">
                            <option value="terlihat" {{ ($produk->visibilitas == 'terlihat') ? 'selected' : '' }}>Terlihat</option>
                            <option value="sembunyikan"  {{ ($produk->visibilitas == 'sembunyikan') ? 'selected' : '' }}>Sembunyikan</option>
                        </select>
                    </div>




            <div class="form-group mt-2 ml-3">
                <button class="btn btn-primary btn-sm" type="submit">Edit Produk</button>
            </div>

        </form>
    </div>
</div>
</div>



@endsection
