@extends('layout.templateumkm')

@section('content')

<div class="main-content">
@if(session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@elseif(session('success'))
<div class="alert alert-success">{{ session('success') }}</div>
@endif

<table class="table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th>NO</th>
            <th>Gambar Produk</th>
            <th>Nama Produk</th>
            <th>Harga Produk</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>

        <?php $no=1; ?>
        @foreach ($manage_produk as $data)
<tr>
    <td>{{ $no++ }}</td>
    @if ($data->gambar_produk == "default_food.png" || $data->url_image == "foto_produk.png")
        <td><img src="{{ asset($data->url_image) }}" alt="{{$data->url_image}}" style="width:250px;height:250px;object-fit:contain"></td>
    @else
        <td><img src="{{ url($data->url_image) }}" alt="" style="width:250px;height:250px;object-fit:contain"></td>
    @endif
    <td>{{ $data->nama_produk }}</td>
    <td>Rp {{ number_format($data->harga, 0,',','.') }}</td>
    <td>
        <a href="/umkm/edit_produk/{{ $data->id }}" class="btn btn-sm btn-success mb-2"> Edit </a>
        <a href="/umkm/delete/{{ $data->id}}" class="btn btn-sm btn-danger mb-2" id="delete {{ $data->id }} " onclick="return confirm('Anda yakin ingin menghapus produk ini?')"> Delete </a>
        @if ($data->visibilitas == "terlihat")
        <a href="/umkm/visibilitas/off/{{ $data->id }}" class="btn btn-sm btn-danger mb-2"> Sembunyikan </a>
        @else
        <a href="/umkm/visibilitas/on/{{ $data->id }}" class="btn btn-sm btn-success mb-2"> Terlihat </a>
        @endif
    </td>
</tr>
@endforeach
    </tbody>
</table>

<div>
    {{ $manage_produk->links() }}
</div>

</div>



@endsection
