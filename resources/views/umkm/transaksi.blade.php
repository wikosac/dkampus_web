@extends('layout.templateumkm')

@section('content')

<div class="main-content">
@if(session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@elseif(session('success'))
<div class="alert alert-success">{{ session('success') }}</div>
@endif

<table class="table table-bordered table-striped table-responsive">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Produk</th>
            <th>Jumlah Transaksi</th>
            <th>Harga Produk</th>
            <th>Tanggal Transaksi</th>
            <th>Total Transaksi</th>
        </tr>
    </thead>

    <tbody>

        <?php $no=1; ?>
        @forelse ($umkm->tokotransaksis as $data)

<tr>
    <td>{{ $no++ }}</td>
    <td>{{ $data->produk->nama_produk }}</td>
    <td>{{ $data->qty }}</td>
    <td>Rp {{ number_format($data->produk->harga,2,',','.') }}</td>
    <td>{{ date_format(date_create($data->created_at), 'd/m/Y') }}</td>
    <td>Rp {{ number_format($data->total,2,',','.') }}</td>
</tr>
@empty
<tr>
    <td colspan="6" class="text-center">Belum ada transaksi</td>
</tr>
@endforelse
<tr>
    <td colspan="5"><strong>TOTAL</strong></td>
    <td><strong>Rp {{ number_format($total,2,',','.') }}</strong></td>

</tr>
    </tbody>
</table>
</div>



@endsection
