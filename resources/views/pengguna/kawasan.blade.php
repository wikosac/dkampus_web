@extends('layout.pengguna', ['title' => 'Hasil Pencarian '.Request()->q ])

@push('info')

<div class="d-none d-md-block d-lg-block">
    <div class="container-fluid d-flex justify-content-between py-2 px-4">
        <div class="">
            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>
        </div>
        <div class="">
            @if(session()->has('data') == '')
            <a href="{{ url('/login') }}" class="mx-1">Login</a> 
            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>
            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>
            @endif
        </div>
    </div>
</div>

@endpush

@push('style')

<!-- owl.carousel css -->
<link
    rel="stylesheet"
    href="assets/libs/owl.carousel/assets/owl.carousel.min.css"/>

<link
    rel="stylesheet"
    href="assets/libs/owl.carousel/assets/owl.theme.default.min.css"/>

@endpush

@section('content')
<section class="row mt-2">
    <div class="col-lg-12">
        <!-- Nav tabs -->
<ul
    class="nav nav-tabs nav-tabs-custom"
    role="tablist"
>
    <li class="nav-item">
    <a
        class="nav-link active"
        data-bs-toggle="tab"
        href="#profile1"
        role="tab"
    >
        <span class="d-block d-sm-block" style="font-size:17px;font-weight:600"><i class="bx bx-store"></i> Toko</span>
    </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content p-3 text-muted mb-5">
    <div class="tab-pane active" id="profile1" role="tabpanel">
    <div class="row mt-3">
        @foreach($tokos as $toko)
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 mb-2" title="{{ $toko->nama_toko }}">
                <div class="card border">
                    <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="ms-2" style="flex:1">
                            <div>
                                <a href="{{ url('/toko/'.$toko->id) }}" class="text-dark">
                                    <b>{{ substr($toko->nama_toko, 0, 16) }}..</b>
                                </a>
                            </div>
                            <small>{{ $toko->kecamatan }}</small>
                        </div>
                        <div class="" style="width:40px">
                            <a href="{{ url('/toko/'.base64_encode($toko->id)) }}" class="btn btn-primary btn-sm">Lihat</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </div>
</div>
    </div>
</section>
@endsection

@push('script')

    <!-- owl.carousel js -->
    <script src="assets/libs/owl.carousel/owl.carousel.min.js"></script>
    
    <script>
    $("#carousel").owlCarousel({
        items:1,
        loop:1,
        margin:16,
        nav:!1,
        dots:!0,
        autoplay:1
        });
    </script>

@endpush