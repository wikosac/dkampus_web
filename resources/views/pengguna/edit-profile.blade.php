@extends('layout.pengguna', ['title' => 'Edit Profile - '.session('data')->username ])
@section('content')
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/select2-4.0.6-rc.1/dist/css/select2.min.css')}}">
    <link href="{{ asset('asset1/css/cart.css') }}" rel="stylesheet">
    <style>
        #map{
            height:50vh;
        }
        .custom-map-control-button{
            background:orange;
            color:white;
            padding:10px 30px;
            border:1px solid orange;
            margin-top:43vh;
        }
    </style>
@endpush

<div class="mt-3">
<div class="row">
<div class="col-lg-12">
<button onclick="history.back()" class="btn btn-sm btn-primary mb-4">Go Back</button>
<h4 class="mb-4 mb-4"><b>Edit Profile</b></h4>

    @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success')}}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>

    @endif

    @if(session('danger'))

    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('danger')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

    @endif

    @if(session('google_login'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('google_login')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <!-- Nav tabs -->
<ul
    class="nav nav-tabs nav-tabs-custom"
    role="tablist"
>
    <li class="nav-item">
    <a
        class="nav-link active"
        data-bs-toggle="tab"
        href="#home1"
        role="tab"
    >
        <span class="d-block d-sm-block">Data Pribadi</span>
    </a>
    </li>
    <li class="nav-item">
    <a
        class="nav-link"
        data-bs-toggle="tab"
        href="#profile1"
        role="tab"
    >
        <span class="d-block d-sm-block">Posisi</span>
    </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content mt-2 text-muted mb-5">
    <div class="tab-pane active" id="home1" role="tabpanel">
        <section class="py-3">
        <form action="{{ route('edit-profile-proses') }}" method="post">
            <div class="form_container">
            @csrf
                <div class="private box">
                    <div class="row no-gutters">
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <label for="" class="form-label">Nama Depan</label>
                                <input type="text" name="nama_depan" class="form-control" value="{{ $user->nama_depan }}" placeholder="Nama Depan" required>
                            </div>
                        </div>
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <label for="" class="form-label">Nama Belakang</label>
                                <input type="text" name="nama_belakang" class="form-control" value="{{ $user->nama_belakang }}" placeholder="Nama Belakang" required>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <label for="" class="form-label">Email (Opsional)</label>
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email (Optional)">
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <label for="" class="form-label">No Whatsapp</label>
                                <input type="number" name="telepon" class="form-control" value="{{ $user->no_telephone }}" placeholder="Nomor Whatsapp aktif" required>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="text-danger mb-2">
                                @error('username')
                                {{ $message }}
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">Username</label>
                                <input type="text" name="username" value="{{ $user->username }}" class="form-control" placeholder="Username" required>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <label for="" class="form-label">Password</label>
                                <input type="password" id="myInput" name="password1" class="form-control" value="{{-- $user->password1 --}}" placeholder="Password">
                                <input type="checkbox" class="mt-3" onclick="myFunction()"> Show Password
                                <script>
                                    function myFunction() {
                                        var x = document.getElementById("myInput");
                                        if (x.type === "password") {
                                                x.type = "text";
                                            } else {
                                                x.type = "password";
                                            }
                                        }
                                </script>
                            </div>
                        </div>
                    </div>

                    <!-- /row -->
                    <div class="row">
                        <div class="col-lg-3 mt-3">
                            <label class="form-label">Provinsi:</label>
                            <div class="">
                                <select class="form-control" name="provinsi" id="provinsi">
                                    <option>Pilih Provinsi</option>
                                    @foreach($provinces as $provinsi)
                                        <option value="{{$provinsi->id}}">{{$provinsi->name}}</option>
                                    @endforeach
                                </select>
                                <div>
                                    <div class="float-start mt-2 badge bg-info">
                                        Pronvisi sekarang : {{ $user->provinsi ? $user->provinsi : 'Belum diisi'}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <label class="form-label" >Kota/Kabupaten:</label>
                            <div class="">          
                                <select class="form-control" name="kota" id="kota"></select>
                                <div>
                                    <div class="float-start mt-2 badge bg-info">
                                        Kabupaten sekarang : {{ $user->kabupaten ? $user->kabupaten : 'Belum diisi' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <label class="form-label" >Kecamatan:</label>
                            <div class="">          
                                <select class="form-control" name="kecamatan" id="kecamatan"></select>
                                <div>
                                    <div class="float-start mt-2 badge bg-info">
                                        Kecamatan sekarang : {{ $user->kecamatan ? $user->kecamatan : 'Belum diisi' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 mt-3">
                            <label class="form-label" >Kelurahan:</label>
                            <div class="">          
                                <select class="form-control" name="kelurahan" id="kelurahan"></select>
                                <div>
                                    <div class="float-start mt-2 badge bg-info">
                                        Kelurahan sekarang : {{ $user->kelurahan ? $user->kelurahan : 'Belum diisi' }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 pl-1 mt-3">
                            <div class="form-group">
                                <label for="" class="form-label">Alamat Lengkap</label>
                                <input type="text" name="alamat" value="{{ $user->alamat }}" class="form-control" placeholder="Alamat" required>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /private -->
                <hr>
                <div class=""><input type="submit" value="Perbarui" class="btn btn-primary"></div>
            </div>
        </form>
        </section>
    </div>
    <div class="tab-pane" id="profile1" role="tabpanel">
        <div id="map"></div>
    </div>
</div>
</div>
</div>
<!-- /page_header -->
</div>
<!-- /container -->
<!-- /box_cart -->
</main>
<!--/main-->
</div>
@endsection
@push('script')

<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/select2.min.js') }}"></script>   
<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/i18n/id.js') }}"></script> 
<script src="{{ asset('assets/js/register.js') }}"></script>

<!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&callback=initMap&v=weekly" async></script>

<script>
    let map, infoWindow;

    function initMap() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
            localStorage['lat'] = pos.lat
            localStorage['lng'] = pos.lng
        })

        const mapOptions = {
            center: {lat:parseFloat({{ $user->lat }}) ? parseFloat({{ $user->lat }}) : -1.0, 
            lng:parseFloat({{ $user->lng }}) ? parseFloat({{ $user->lng }}) : 0.0},
            zoom:15
        }
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        infoWindow = new google.maps.InfoWindow();

        const marker = new google.maps.Marker({
            position: {lat:parseFloat({{ $user->lat }}) ? parseFloat({{ $user->lat }}) : -1.0, lng:parseFloat({{ $user->lng }}) ? parseFloat({{ $user->lng }}) : 0.0},
            map: map,
        });

        const locationButton = document.createElement("button");
        locationButton.textContent = "Posisikan otomatis";
        locationButton.classList.add("custom-map-control-button");
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);

        locationButton.addEventListener("click", () => {
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        const pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude,
                            accuracy: position.coords.accuracy
                        };
                        localStorage['lat'] = pos.lat
                        localStorage['lng'] = pos.lng
                        infoWindow.setPosition(pos);
                        infoWindow.setContent("Ini lokasi anda");
                        infoWindow.open(map);
                        map.setCenter(pos);
                        
                        $.ajax({
                            method:"POST",
                            url:"/update-location",
                            data:{
                                lat:pos.lat,
                                lng:pos.lng,
                                userId:{{ session('data')->id }}
                            },
                            success:function(res){
                                console.log(res)
                            }
                        })
                    },
                    () => { handleLocationError(true, infoWindow, map.getCenter()); }
                );
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }
        });
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(
            browserHasGeolocation
            ? "Error: Akses lokasi tidak diizinkan."
            : "Error: Your browser doesn't support geolocation."
        );
        infoWindow.open(map);
    }
</script>

<script>
    $(function() {
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $(function() {
            $('#provinsi').on('change', function() {
                let id_provinsi = $('#provinsi').val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route("getKota") }}',
                    data: {id_provinsi: id_provinsi},
                    cache: false,
                    success: function(msg) {
                        $('#kota').html(msg);
                        $('#kecamatan').html('');
                        $('#kelurahan').html('');
                    },
                    error: function(data) {
                        console.log('error:', data)
                    },
                })
            })
            $('#kota').on('change', function() {
                let id_kota = $('#kota').val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route("getKecamatan") }}',
                    data: {id_kota: id_kota},
                    cache: false,
                    success: function(msg) {
                        $('#kecamatan').html(msg);
                        $('#kelurahan').html('');
                    },
                    error: function(data) {
                        console.log('error:', data)
                    },
                })
            })
            $('#kecamatan').on('change', function() {
                let id_kecamatan = $('#kecamatan').val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route("getKelurahan") }}',
                    data: {id_kecamatan: id_kecamatan},
                    cache: false,
                    success: function(msg) {
                        $('#kelurahan').html(msg);
                    },
                    error: function(data) {
                        console.log('error:', data)
                    },
                })
            })
        })
    })
</script>

@endpush

