@extends('layout.pengguna', ['title' => 'Daftar Akun - DKampus']
)

@section('content')

<div class="account-pages pt-sm-5">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-md-8 col-lg-6 col-xl-5">
            <div class="card overflow-hidden">
              <div class="bg-primary bg-soft">
                <div class="row">
                  <div class="col-7">
                    <div class="text-primary p-4">
                      <h5 class="text-primary">Selamat Datang !</h5>
                      <p>Buat Akun anda.</p>
                    </div>
                  </div>
                  <div class="col-5 align-self-end">
                    <img
                      src="assets/images/profile-img.png"
                      alt=""
                      class="img-fluid"
                    />
                  </div>
                </div>
              </div>
              <div class="card-body pt-0">
                <div class="auth-logo">
                  <a href="#" class="auth-logo-light">
                    <div class="avatar-md profile-user-wid mb-4">
                      <span class="avatar-title rounded-circle bg-light">
                        <img
                          src="assets/images/logo-light.svg"
                          alt=""
                          class="rounded-circle"
                          height="34"
                        />
                      </span>
                    </div>
                  </a>

                  <a href="#" class="auth-logo-dark">
                    <div class="avatar-md profile-user-wid mb-4">
                      <span class="avatar-title rounded-circle bg-light">
                        <img
                          src="assets/images/logo.svg"
                          alt=""
                          class="rounded-circle"
                          height="34"
                        />
                      </span>
                    </div>
                  </a>
                </div>
                <div class="p-2">
                <form action="{{ url('register/store') }}" method="post">
            <div class="form_container">
            @csrf
                <div class="private box">
                    <div class="row no-gutters">
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="text" name="nama_depan" class="form-control" placeholder="Nama Depan" required>
                            </div>
                        </div>
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="text" name="nama_belakang" class="form-control" placeholder="Nama Belakang" required>
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Email (Optional)">
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="text-danger mb-2">
                                @error('username')
                                {{ $message }}
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Username" required>
                            </div>
                        </div>
                        <div class="col-6 mb-3" >
                            <div class="form-group">
                                <input type="password2" name="password1" class="form-control" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="password2" name="password2" class="form-control" placeholder="Konfirmai Password" required>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                    <div class="row no-gutters">
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <input type="text" name="telepon" class="form-control" placeholder="Nomor Whatsapp" required>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <div class="form-group">
                                <div class="custom-select-form">
                                    <select class="form-control add_bottom_10" name="provinsi" id="provinsi" required>
                                            <option value="Lampung" selected>Lampung </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <div class="form-group">
                                <div class="custom-select-form">
                                    <select class="form-control add_bottom_10" name="kabupaten" id="kabupaten" required>
                                            <option value="Lampung Utara" selected>Lampung Utara</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <div class="form-group">
                                <div class="custom-select-form">
                                    <select class="form-control add_bottom_10" name="kecamatan" id="kecamatan" required>
                                            <option value="Abung Semuli" selected>Abung Semuli</option>
                                            <option value="Abung Selatan" selected>Abung Selatan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pl-1">
                            <div class="form-group">
                                <input type="text" name="alamat" class="form-control" placeholder="Alamat" required>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /private -->
                <hr>
                <div class="text-center"><input type="submit" value="Register" class="btn btn-primary w-100"></div>
            </div>
            </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
