@extends('layout.pengguna', ['title' => 'Keranjang - DKampus'])

@section('content')

    @if ($errors->any())
        <h4 style="color:red">{{ $errors->first() }}</h4>
    @endif

    <section class="row mb-5 body-reload">
        @if (count($carts) != 0)
            <div class="col-lg-8">
                @if ($message = Session::get('failed'))
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @endif
                <h4 class="mt-4"><b>Keranjang</b></h4>
                <div class="row keranjang">
                    <div class="col-lg-6">
                    </div>
                    @foreach ($carts as $cart)
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="mb-4">
                                        <div class="" style="font-size:14px">
                                            <a href="{{ url('/toko/' . base64_encode($cart->produk->umkm->id)) }}"
                                                class="text-dark">
                                                <b>{{ $cart->produk->umkm->nama_toko }}</b>
                                            </a>
                                        </div>
                                        <div class="">
                                            <div class="d-flex">
                                                <div style="width:80px">
                                                    <img src="{{ url($cart->produk->url_image) }}" alt=""
                                                        class="w-100 rounded">
                                                </div>
                                                <div class="ms-3" style="flex:1">
                                                    <div class="mb-2" style="font-size:14px">
                                                        <a href="{{ url('/detail/' . $cart->produk->id) }}"
                                                            class="text-dark">
                                                            {{ $cart->produk->nama_produk }}
                                                        </a>
                                                    </div>
                                                    <div class="">
                                                        <h5><b>Rp {{ number_format($cart->produk->harga, 0, ',', '.') }}</b>
                                                        </h5>
                                                    </div>
                                                    <div class="">Jumlah : {{ $cart->qty }}<b>
                                                            <div class="col-lg-12 col-2">
                                                                <div class="mt-3 mb-2">
                                                                    <div class="row align-items-center"
                                                                        style="width: 200px;">
                                                                        <div class="col-lg-12 col-12">
                                                                            <div class="mt-3 mb-2">
                                                                                <input type="hidden"
                                                                                    value="{{ $cart->qty }}"
                                                                                    id="qty" name="qty"
                                                                                    width="500" />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    {{-- Input after changed --}}
                                                                    <input type="hidden" id="amount_changed"
                                                                        class="amount_changed" value="{{ $cart->qty }}">

                                                                    {{-- id product --}}
                                                                    <input type="hidden" id="id_product" class="id_product"
                                                                        value="{{ $cart->produk_id }}">

                                                                    <input type="hidden" name="subtotalHidden"
                                                                        value="{{ $subtotal }}" id="hideSubtotal">
                                                                </div>
                                                            </div>
                                                        </b>
                                                    </div>
                                                </div>

                                                <div class="">
                                                    @if ($message = Session::has('failed'))
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                data-cart="{{ $cart->id }}" name="checkDelete[]"
                                                                id="checkDelete">
                                                        </div>
                                                    @else
                                                        <a href="{{ url('cart/delete/' . $cart->id) }}"
                                                            class="text-secondary text-soft" style="font-size:25px"><i
                                                                class="bx bx-trash"></i></a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-4">
                <div class="alert alert-info text-wrap">Pembayaran pesanan menggunakan dompet digital, QR code akan
                    diberikan oleh D'squad melalui pesan whatsapp ketika pemesanan berlangsung</div>

                <div class="d-flex justify-content-between pb-1 mt-3">
                    <span class="font-weight-bold" style="font-size:22px;border-width:3px  "><img
                            src="{{ asset('asset1/img/ovo.png') }}" alt="" width="25" height="35"></span>

                    <span class="font-weight-bold" style="font-size:22px;border-width:3px"><img
                            src="{{ asset('asset1/img/gopay.png') }}" alt="" width="45" height="25"></span>

                    <span class="font-weight-bold" style="font-size:22px;border-width:3px "><img
                            src="{{ asset('asset1/img/dana.png') }}" alt="" width="45" height="25"></span>

                    <span class="font-weight-bold" style="font-size:22px;border-width:3px "><img
                            src="{{ asset('asset1/img/shopee_pay.png') }}" alt="" width="35"
                            height="25"></span>
                </div>

                <div class="card">
                    <div class="card-body">
                        <h5 class="mb-4"><b>Ringkasan Belanja</b></h5>
                        {{-- <div class="" id="jarakMaps" style="font-weight:700;font-size:22px">

                    </div>
                    <a href="{{url('edit-profile')}}" target="_blank" rel="noopener noreferrer">Lokasimu Kurang Akurat ? yuk cek posisimu!</a> --}}
                        {{-- <div class="mt-3">
                        <label class="" for="catatan">Catatan</label>
                        <input class="" type="text" name="catatan" id="catatan" value=" {{ $cart->catatan }}">
                    </div> --}}

                        <div class="d-flex justify-content-between pb-3 mt-3" style="border-width:3px !important;">
                            <div class="">
                                Total Harga
                            </div>
                            <div class="" id="subtotalShow">Rp.{{ number_format($subtotal) }}</div>
                        </div>
                        {{-- <div class="d-flex justify-content-between pb-3" style="border-width:3px !important;">
                        <div class="">
                            Ongkos Pengiriman
                        </div>
                        <div class="" id="ongkir">Rp.0</div>

                    </div> --}}
                        <div class="d-flex justify-content-between" style="border-width:3px !important;">
                            <div class="">
                                Biaya Layanan & lain-lain
                            </div>
                            <div class="" id="biayaJasa">
                                Rp.{{ number_format($jumlah_barang > 5 ? 1000 + 1000 : 1250) }}</div>
                        </div>
                        <div class="d-flex justify-content-between border-bottom pb-3 mt-3"
                            style="border-width:3px !important;">
                            <div class="">
                                Alamat Pengiriman
                            </div>
                            <div class="" id="">{{ $user->alamat }}</div>
                        </div>
                        {{-- <div class="d-flex justify-content-between border-bottom pb-3 mt-2" style="border-width:3px !important;">
                        <div class="" id="">{{ $user->alamat }}</div>
                    </div> --}}
                        <div class="d-flex justify-content-between border-bottom pb-3 mt-3"
                            style="border-width:3px !important;">
                            <a link="#" class="text-wrap" style="color: #f1734f; font-size:12px;">Biaya ongkir akan
                                diberikan melalui chat Whatsapp tenang aja, ongkirnya cuman Rp.5000 / Rp.10.000 aja kok!</a>
                        </div>
                        <div class="d-flex justify-content-between py-3">
                            <div class="">
                                <h5><b>Total Harga</h5></b>
                                <a link="#" class="fw-200" style="color:red; font-size:12px;">*Harga belum termasuk
                                    ongkir</a>
                            </div>
                            <div class="">
                                <h5 id="total" style="font-weight:700">Rp.{{ number_format($subtotal + 1250) }}</h5>
                            </div>
                        </div>
                        <div class="">
                            @if (session()->get('data')->lat and session()->get('data')->lng)
                                @if ($message = Session::has('failed'))
                                @else
                                    <form action="{{ url('/order') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="subtotal" value="{{ $subtotal }}">
                                        {{-- <input type="hidden" name="ongkirmap" id="ongkirmap"> --}}
                                        <input type="hidden" name="total" id="totalHarga"
                                            value="{{ $subtotal + 1250 }}">
                                        <input type="hidden" name="lat">
                                        <input type="hidden" name="lng">


                                        <button id="tombolbeli" type="submit" class="btn btn-primary w-100 btn-lg">Beli
                                            ({{ $jumlah_barang }})</button>
                                    </form>
                                @endif
                            @else
                                <div class="alert alert-info">Upss! silahkan update terlebih dahulu posisi anda untuk
                                    melakukan pembelian, <a href="{{ url('/edit-profile') }}"><strong>Klik
                                            disini<strong></a></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-lg-12 text-center align-item-center justify-content-center h-100">
                <div class="mt-5">
                    <h3><b>Wah Keranjang Belanjamu Kosong</b></h3>
                    <p>Yuk beli produk yang kamu inginkan sekarang</p>
                </div>
            </div>
        @endif

        <button id="tombolbeli" type="submit" class="invisible">Beli ({{ $jumlah_barang }})</button>
    </section>


    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

@endsection

@push('script')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&callback=initMap&v=weekly"
        async></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @if ($carts->isEmpty())
        {{-- Do Nothing --}}
    @else
        <script>
            // id button delete = hapusPesanan
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#hapusPesanan').click(function() {
                    let dataId = new Array();
                    $('#checkDelete:checked').each(function() {
                        dataId.push($(this).attr('data-cart'));
                    });
                    console.log(dataId);
                    //url
                    let url = '{{ url('cart/delete/collection') }}';
                    //delete logic
                    if (dataId.length > 0) {
                        Swal.fire({
                            title: "Apakah anda yakin ?",
                            html: 'anda akan menghapus <b>(' + dataId.length + ')</b> Pesanan',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonText: 'Hapus',
                            cancelButtonText: 'Batal',
                            allowOutsideClick: false
                        }).then(function(result) {
                            if (result.value) {
                                $.post(url, {
                                    id: dataId
                                }, function(data) {
                                    if (data.code == 1) {
                                        $(document).ajaxStop(function() {
                                            window.location.reload();
                                            Swal.fire({
                                                icon: 'success',
                                                title: data.msg,
                                                showConfirmButton: false,
                                                timer: 10000,
                                                allowOutsideClick: false
                                            });
                                        });
                                    } else {
                                        $(document).ajaxStop(function() {
                                            window.location.reload();
                                            Swal.fire({
                                                icon: 'error',
                                                title: data.msg,
                                                showConfirmButton: false,
                                                timer: 20000,
                                                allowOutsideClick: false
                                            });
                                        });
                                    }
                                }, 'json');
                            }
                        });
                    }
                });
            });
        </script>

        {{-- <script>
    var ongkir = document.getElementById('ongkir');
    var ongkirmap = document.getElementById('ongkirmap');
    var tombolbeli = document.getElementById('tombolbeli');
    $(document).ready(function(){
        tombolbeli.style.display = 'none'
        console.info("Hati-hati dengan tindakan pengubah sumber kode")
        initMap()
        $('#failedModal').modal('show');
    })

        const jarakMaps = document.getElementById('jarakMaps')

        function initMap(){
            var directionsService = new google.maps.DirectionsService();

            var start = new google.maps.LatLng({{ session()->get('data')->lat }}, {{ session()->get('data')->lng }});
            var end = new google.maps.LatLng({{ $carts[0]->produk->umkm->lat }}, {{ $carts[0]->produk->umkm->lng }});

            var request = {
                origin : start,
                destination : end,
                travelMode : "DRIVING"
            }

            directionsService.route(request, function(response, status){
                if(status == "OK"){
                    tombolbeli.style.display = 'block'
                    const res = JSON.stringify(response)
                    const data = JSON.parse(res)    
                    jarakMaps.innerHTML = data['routes'][0]['legs'][0]['distance']['text']
                    
                    //Script Peringatan jarak tidak lebih dari 5KM
                    if(data['routes'][0]['legs'][0]['distance']['value'] > 5000){
                        $('#peringatanKM').show(); 
                        document.getElementById("tombolbeli").disabled = true;
                    }else{
                        $('#peringatanKM').hide();
                    }

                    if(data['routes'][0]['legs'][0]['distance']['value'] > 500){
                        const jarakMaps = data['routes'][0]['legs'][0]['distance']['value']
						var biayaOngkirMaps = jarakMaps - 500
						biayaOngkirMaps *= 5
						biayaOngkirMaps += 2000
                        const ratusan = biayaOngkirMaps.toString().substr(-3)
                        console.log('Jarak ' + data['routes'][0]['legs'][0]['distance']['value'])
                        console.log('Biaya Ongkir ' + biayaOngkirMaps)
                        if(ratusan < 500){
                            const akhir = biayaOngkirMaps - ratusan
                            console.log(akhir)
                            var semiTotal = parseInt(akhir) + parseInt({{ $subtotal }}) + parseInt({{ $jumlah_barang > 5 ? 1000+1000 : 1000}})
                            var totalBayar = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(semiTotal);
                            var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(akhir);

                            ongkir.innerHTML = rupiah
                            ongkirmap.value = akhir
                            total.innerHTML = totalBayar
                            totalHarga.value = semiTotal

                        }else{
                            const akhir = biayaOngkirMaps - ratusan
                            var semiTotal = parseInt(akhir) + parseInt({{ $subtotal }}) + parseInt({{ $jumlah_barang > 5 ? 1000+1000 : 1000}})
                            var totalBayar = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(semiTotal);
                            var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(akhir);

                            ongkir.innerHTML = rupiah
                            ongkirmap.value = akhir

                            total.innerHTML = totalBayar
                            totalHarga.value = semiTotal
                        }
                    }else{
                        var semiTotal = 5000 + parseInt({{ $subtotal }}) + parseInt({{ $jumlah_barang > 5 ? 1000+1000 : 1000}})
                        var totalBayar = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(semiTotal);
                        var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(5000);

                        ongkir.innerHTML = rupiah
                        ongkirmap.value = 5000
                        total.innerHTML = totalBayar
                        totalHarga.value = semiTotal
                    }
                    
                }
            })
            
        }

        // dari modal lanjut beli
        $('#lanjutBeli').click(function(){
            $('#slotForm').html('<button id="tombolbeli" type="submit" class="btn btn-primary w-100 btn-lg">Beli ({{ $jumlah_barang }})</button>');
        });
</script> --}}
    @endif
@endpush
