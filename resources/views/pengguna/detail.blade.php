@extends('layout.pengguna', ['title' => 'Jual '.$produk->nama_produk.' - DKampus'])
@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Touchspin CSS -->
    <link
      href="{{ asset('frontend/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
      rel="stylesheet"
      type="text/css"
    />
    <!-- owl.carousel css -->
    <link
        rel="stylesheet"
        href="{{ asset('frontend/libs/owl.carousel/assets/owl.carousel.min.css') }}"/>
    
    <link
        rel="stylesheet"
        href="{{ asset('frontend/libs/owl.carousel/assets/owl.theme.default.min.css') }}"/>
    <meta property="og:image" itemprop="image" content="{{ asset('foto_produk/'.$produk->gambar_produk) }}">

@endpush

@section('content')
@if(session('danger'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{ session('danger')}}
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif
{{-- Navigation --}}
<div class="mt-3 mb-3 d-flex align-items-center">
  <a href="{{ url('/') }}" class="mx-1">Beranda</a>
  <i class="bx bxs-right-arrow mx-1"></i>
  <a href="{{ url('/') }}" class="mx-1">Delivery</a>
  <i class="bx bxs-right-arrow mx-1"></i>
  <a href="#" class="mx-1 text-dark" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">{{ $produk->nama_produk }}</a>
</div>
{{-- Product Detail  --}}
<section class="row mt-4 bg-white">
    <div class="col-lg-4 mb-4">
        
        <a href="{{ url($produk->url_image) }}" class="progressive replace" style="max-height:250px;object-fit:contain;width:100%" data-lightbox="image">
              <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="image" style="object-fit:contain" />
        </a>
        
        <div class="mt-3 alert alert-info">Klik gambar untuk melihat secara detail</div>
    </div>
    <div class="col-lg-5 mb-5">
        <h3 style="font-weight:500">{{ $produk->nama_produk }}</h3>
        <div class=""></div>
        <div class="mt-3">
            <h4><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h4>
        </div>
        <div class="mt-4">
            <!-- Nav tabs -->
                    <ul
                      class="nav nav-tabs nav-tabs-custom"
                      role="tablist"
                    >
                      <li class="nav-item">
                        <a
                          class="nav-link active"
                          data-bs-toggle="tab"
                          href="#home1"
                          role="tab"
                        >
                          <span class="d-block d-sm-block">Detail</span>
                        </a>
                      </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-3 text-muted">
                      <div class="tab-pane active" id="home1" role="tabpanel">
                        <div class="mt-3">
                            {!! $produk->keterangan !!}
                            <div class="mt-3" style="font-weight:600">Terjual : {{ $produk->terjual }}</div>
                        </div>
                      </div>
                      <div class="tab-pane" id="profile1" role="tabpanel">
                        <div class="mt-3">
                        </div>
                      </div>
                    </div>
                    
                    <div class="mt-3 border-top pt-3 border-2">
                            <div class="d-flex align-items-center">
                                <div class="" style="flex:1">
                                    <div style="font-size:15px" class="text-dark">
                                      <strong>{{ $produk->umkm->nama_toko }}</strong>
                                    </div>
                                    <div class="mt-1 d-lg-block d-none">Bergabung sejak <strong>{{ $produk->umkm->created_at->diffForHumans() }}</strong></div>
                                    <div class="mt-1 d-lg-none d-block">Bergabung sejak <br/> <strong>{{ $produk->umkm->created_at->diffForHumans() }}</strong></div>
                                </div>
                                <div class="">
                                    <a href="{{ url('/toko/'.base64_encode($produk->umkm->id)) }}" class="btn btn-primary">Lihat Toko</a>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
    <div class="col-lg-3 mb-5">
      @if($produk->visibilitas == 'sembunyikan')
        <div class="alert alert-info">Produk ini belum bisa dibeli.</div>
      @else
      <div class="card border">
        <div class="card-body">
          <h5><b>Atur Jumlah</b></h5>
          <form action="{{ url('/cart/add') }}" method="post">
            @csrf
            <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            <input type="hidden" name="harga" value="{{ $produk->harga }}">
            
            <div class="row align-items-center">
              <div class="col-lg-12 col-12">
                  <div class="mt-3 mb-2">
                    <input data-toggle="touchspin" type="text" value="1" id="qty" name="qty" />
                  </div> 
              </div>
              <input type="hidden" name="subtotal" value="{{ $produk->harga }}" id="hideSubtotal">
              <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            </div>
            <div class="mt-3" style="font-size: 15px;">
              <p>Alamat Pengiriman</p>
              <textarea class="form-control" name="alamat" id="alamat" cols="5" rows="5"></textarea>
            </div>
			<div class="mt-3" style="font-size: 15px;">
              {{-- <p>Catatan</p> --}}
              {{-- <input type="text" class="form-control" name="catatan" id="catatan" cols="5" rows="5" id="catatan"> --}}
            </div>
            <div class="mt-3 d-flex justify-content-between align-items-center" style="font-size:15px">
              <div class="">
                Subtotal
              </div>
              <div class="">
                <span id="subtotal">
                  <b>Rp {{ number_format($produk->harga, 0,',','.') }}</b>
                </span>
              </div>
            </div>
            <div class="mt-3">
              <input type="hidden" name="umkm_id" value="{{$produk->umkm_id}}">
              <input type="hidden" name="level" value="{{session('data')->level}}">
              <input type="hidden" name="lat" value="{{$umkm->lat}}">
              <input type="hidden" name="lng" value="{{$umkm->lng}}">

              <button type="submit" class="btn btn-primary w-100 mb-2" name="cart" value="cart"><i class="bx bx-plus me-1"></i> Keranjang</button>
            </div>
          </form>
        </div>
      </div>
      @endif
    </div>
</section>
{{-- Other Page --}}
<h4 class="mt-2"><b>Lainnya dari toko ini</b></h4>
<section class="row mt-3 owl-carousel owl-theme" id="laris">
    @forelse($umkm->produks as $other)
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    <a href="{{ url('/detail/'.$other->id) }}" class="text-dark">
        <div class="card">
            <a href="{{ url($other->url_image) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
              <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="{{ $other->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
            </a>
            <div class="card-body" style="text-transform:capitalize">
                <div class="my-2 d-block d-md-none"><a href="{{ url('/toko/'.base64_encode($other->umkm->id)) }}" style="font-size:10px;font-weight:500">{{ $other->umkm->nama_toko }}</a></div>
                <a href="{{ url('/detail/'.$other->id) }}" class="text-dark">{{ substr($other->nama_produk,0,35) }}</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($other->harga, 0,',','.') }}</strong></h5>
                    <div class="d-block d-md-none" style="font-size:11px;">{{ $other->umkm->kecamatan }}</div>
                    <div class="align-items-center hover-slideup d-none d-md-block"> 
                        <div class="text-dark alamat"><small>{{ $other->umkm->kecamatan }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.base64_encode($other->umkm->id)) }}" class="text-dark" style="font-size:11px">
                            {{ $other->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                    <div class="" style="font-size:11px;">Terjual : {{ $other->terjual }}</div>
                </div>
            </div>
        </div>
        </a>
    </div>
    @empty
    <div class="text-center">Belum ada data</div>
    @endforelse
</section>
{{-- Modal cart --}}
@if (session()->has('failed'))
<div class="modal modal-fade" id="modalCart" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="font-size:12px;">{{session('failed')}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        @foreach($carts as $cart)  
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="" style="font-size:14px">
                            <a href="{{ url('/toko/'.base64_encode($cart->produk->umkm->id)) }}" class="text-dark">  
                                <b>{{ $cart->produk->umkm->nama_toko }}</b>
                            </a>
                        </div>
                        <div class="">
                            <div class="d-flex">
                                <div style="width:80px">
                                    <img src="{{ url($cart->produk->url_image) }}" alt="" class="w-100 rounded">
                                </div>
                                <div class="ms-3" style="flex:1">
                                    <div class="mb-2" style="font-size:14px">
                                        <a href="{{ url('/detail/'.$cart->produk->id) }}" class="text-dark">
                                            {{ $cart->produk->nama_produk }}
                                        </a>
                                    </div>
                                    <div class=""><h5><b>Rp {{ number_format($cart->produk->harga,0,',','.') }}</b></h5>
                                    </div>
                                        <div class="">Jumlah : {{$cart->qty}}<b><div class="col-lg-12 col-2">
                                            <div class="mt-3 mb-2">
                                                <div class="row align-items-center" style="width: 200px;">
                                                <div class="col-lg-12 col-12">
                                                    <div class="mt-3 mb-2">
                                                        <input type="hidden" value="{{$cart->qty}}" id="qty" name="qty" width="500" />
                                                    </div>
                                                </div>
                                                </div>

                                                {{-- Input after changed --}}
                                                <input type="hidden" id="amount_changed" class="amount_changed" value="{{$cart->qty}}">

                                                {{-- id product --}}
                                                <input type="hidden" id="id_product" class="id_product" value="{{$cart->produk_id}}">
                                            </div>
                                        </div></b>
                                    </div>
                                </div>

                                <div class="">
                                   @if ($message = Session::has('failed'))

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" data-cart="{{$cart->id}}" name="checkDelete[]" id="checkDelete">
                                    </div>
                                    @else
                                        <a href="{{ url('cart/delete/'.$cart->id) }}" class="text-secondary text-soft" style="font-size:25px"><i class="bx bx-trash"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
      </div>
      <div class="modal-footer">
        @if (session()->has('failed'))
          <button type="button" class="btn btn-danger" id="hapusPesanan">Hapus</button>
        @else
          <button type="button" class="btn btn-primary" id="keranjang">Keranjang</button>
        @endif
      </div>
    </div>
  </div>
</div>
@endif
@endsection

@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- TouchSpin JS -->
    <script src="{{ asset('frontend/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('frontend/libs/owl.carousel/owl.carousel.min.js')}}"></script>
    {{-- Sweet alert 2 --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
     $('#modalCart').modal('show')
    </script>
    <script>
       // id button delete = hapusPesanan
    $(document).ready(function(){ 
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#hapusPesanan').click(function(){
            let dataId = new Array();
            $('#checkDelete:checked').each(function(){
                dataId.push($(this).attr('data-cart'));
            });
            //url
            let url = '{{url("cart/delete/collection")}}';
            //delete logic
            if(dataId.length > 0){
                Swal.fire({
                    title:"Apakah anda yakin ?",
                    html:'anda akan menghapus <b>('+dataId.length+')</b> Pesanan',
                    showCancelButton:true,
                    showCloseButton:true,
                    confirmButtonText:'Hapus',
                    cancelButtonText:'Batal',
                    allowOutsideClick:false 
                }).then(function(result){
                    if(result.value){
                        $.post(url,{id:dataId},function(data){
                            if(data.code == 1){
                                $(document).ajaxStop(function(){
                                    window.location.reload();
                                    Swal.fire({
                                        icon:'success',
                                        title:data.msg,
                                        showConfirmButton:false,
                                        timer:10000,
                                        allowOutsideClick:false
                                    });
                                });
                            }else{
                                $(document).ajaxStop(function(){
                                    window.location.reload();
                                    Swal.fire({
                                        icon:'error',
                                        title:data.msg,
                                        showConfirmButton:false,
                                        timer:20000,
                                        allowOutsideClick:false
                                    });
                                });
                            }
                        },'json');
                    }
                });
            }
        });
    });
    </script>
    <script>
            
        $("#laris").owlCarousel({
        loop:0,
        margin:16,
        dots:0,
        autoplay:1,
        responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:6,
            nav:false,
        }
        }
        });
        
      $("input[data-toggle='touchspin']").TouchSpin({
          min: 1,
          step: 1,
      });

      $("#qty").change(function(){
        var subtotal = $("#qty").val() * {{ $produk->harga }}
        var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(subtotal);
        $("#subtotal").html(`<b>` + rupiah + `</b>`)
        $("#hideSubtotal").val(subtotal)
      })
    </script>

@endpush