@extends('layout.pengguna', ['title' => 'DKampus - Jual Beli Barang tinggal Delivery aja'])

@push('style')

<!-- owl.carousel css -->
<link
    rel="stylesheet"
    href="{{ asset('frontend/libs/owl.carousel/assets/owl.carousel.min.css') }}"/>

<link
    rel="stylesheet"
    href="{{ asset('frontend/libs/owl.carousel/assets/owl.theme.default.min.css') }}"/>

@endpush

@section('content')

<h4 class="mt-4"><b>{{ request()->segment(2) }}</b></h4>
<section class="row mt-3">
    @forelse($produks as $produk)
    <div class="col-lg-2 col-md-4 col-sm-6 col-6">
    <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">
        <div class="card">
            <a href="{{ url($produk->url_image) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
              <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="{{ $produk->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
            </a>
            <div class="card-body" style="text-transform:capitalize">
                <div class="my-2 d-block d-md-none"><a href="{{ url('/toko/'.base64_encode($produk->umkm->id)) }}" style="font-size:10px;font-weight:500">{{ $produk->umkm->nama_toko }}</a></div>
                <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">{{ substr($produk->nama_produk,0,35) }}</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h5>
                    <div class="d-block d-md-none" style="font-size:11px;">{{ $produk->umkm->kecamatan }}</div>
                    <div class="align-items-center hover-slideup d-none d-md-block"> 
                        <div class="text-dark alamat"><small>{{ $produk->umkm->kecamatan }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.base64_encode($produk->umkm->id)) }}" class="text-dark" style="font-size:11px">
                            {{ $produk->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                    <div class="" style="font-size:11px;">Terjual : {{ $produk->terjual }}</div>
                </div>
            </div>
        </div>
        </a>
    </div>
    @empty
    <div class="text-center">Belum ada data</div>
    @endforelse
</section>
<div class="row mb-5">
    {{ $produks->links() }}
</div>
@endsection

@push('script')

    <!-- owl.carousel js -->
    <script src="{{ asset('frontend/libs/owl.carousel/owl.carousel.min.js')}}"></script>
    
    <script>
    $("#carousel").owlCarousel({
        items:1,
        loop:1,
        margin:16,
        nav:!1,
        dots:0,
        autoplay:1
        });
        
        $("#laris").owlCarousel({
        loop:0,
        margin:16,
        dots:0,
        autoplay:1,
        responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:6,
            nav:false,
        }
        }
        });
        
        $("#promo").owlCarousel({
        loop:0,
        margin:16,
        dots:0,
        autoplayTimeout:3000,
        autoplay:1,
        responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:6,
            nav:false,
        }
        }
        });
    </script>

@endpush