@extends('layout.pengguna', ['title' => $toko->nama_toko ])
@push('style')
    <style>
        .banner{
            background-image: url("/images/banner/{{$toko->banner}}");
            background-size: cover;
        }
    </style>
@endpush

@section('content')

<section class="card shadow">
    <div class="card-body">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
            </div>
        @endif

        @if ($message = Session::get('failed'))
            <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
            </div>
        @endif
        <div class="card text-bg-dark p-1 banner" style="background-color:#FFD59E">
            <div class="d-flex align-items-center mt-4 content">
                <div class="p-4 card">
                    <div class="d-flex align-items-center">
                        <h4 class="me-2 text-dark"><strong>{{ $toko->nama_toko }}</strong></h4>
                    </div>
                    <div class="" style="text-transform:capitalize">
                        <i class="mdi mdi-google-maps" style="font-size:19px"></i> 
                        {{ $toko->alamat }}
                    </div>
                    <div class="mb-4 mt-4">
                        <button type="button" class="btn border mb-2 btn-info mb-lg-0  btn-md px-3" data-bs-toggle="modal"
                        data-bs-target=".bs-example-modal-xl"><b>Info Toko</b></button>
                        @if(session()->has('data') && session()->get("user_id") == $toko->user_id)
                            <button type="button" class="btn border mb-2  mb-lg-0  btn-md px-3 mre-4" data-bs-toggle="modal"
                            data-bs-target=".bannerModal"><b>Ubah Banner</b></button>
                       @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Nav tabs -->
<ul
    class="nav nav-tabs nav-tabs-custom"
    role="tablist"
>
    <li class="nav-item">
    <a
        class="nav-link active"
        data-bs-toggle="tab"
        href="#home1"
        role="tab"
    >
        <span class="d-block d-sm-block">Produk yang dijual</span>
    </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content p-3 text-muted mb-5">
    <div class="tab-pane active" id="home1" role="tabpanel">
    <section class="row">
    <h4 class="mb-4 mt-4"><b>Semua Produk</b></h4>
    @foreach($toko->produks as $produk)
    <div class="col-lg-2 col-md-3 col-sm-4 col-12">
        <div class="card">
            <a href="{{ url($produk->url_image) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
              <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="image" />
            </a>
            <div class="card-body">
                <div class="my-2 d-block d-md-none" style="font-size:10px;font-weight:500">{{ $produk->umkm->nama_toko }}</div>
                <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">{{ substr($produk->nama_produk,0,35) }}...</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h5>
                    <div class="d-block d-md-none" style="font-size:11px;">{{ $produk->umkm->kecamatan }}</div>
                    <div class="align-items-center hover-slideup d-none d-mb-block"> 
                        <div class="text-dark alamat"><small>{{ $produk->umkm->alamat }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.$produk->umkm->id) }}" class="text-dark" style="font-size:11px">
                            {{ $produk->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                    <div class="" style="font-size:11px;">Terjual : {{ $produk->terjual }}</div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</section>
    </div>
</div>


<!--  Extra Large modal example -->
<div
    class="modal fade bs-example-modal-xl"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true"
    >
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        <div class="modal-header px-lg-5">
            <h3
            class="modal-title"
            id="myExtraLargeModalLabel"
            >
            <b>Info Toko</b>
            </h3>
            <button
            type="button"
            class="btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
            ></button>
        </div>
        <div class="modal-body px-lg-5 pb-4">
            <div class="mt-lg-4">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="">
                            <b class="text-secondary text-soft">Deskripsi Toko</b>
                            <p class="mt-1" style="font-size:15px">{{ $toko->keterangan }}</p>
                        </div>
                        <div class="mt-5">
                            <b class="text-secondary text-soft">Alamat Toko</b>
                            <p class="mt-1" style="font-size:15px">{!! $toko->alamat !!}</p>
                        </div>
                        <div class="mt-5">
                            <b class="text-secondary text-soft">Buka Sejak</b>
                            <p class="mt-1" style="font-size:15px">{{ date_format(date_create($toko->created_at), 'F Y') }}</p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6"></div>
                </div>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    {{-- Banner Modal --}}
<div
class="modal fade bannerModal"
tabindex="-1"
role="dialog"
aria-labelledby="bannerModal"
aria-hidden="true"
>
<div class="modal-dialog modal-xl">
    <div class="modal-content">
    <div class="modal-header px-lg-5">
        <h3
        class="modal-title"
        id="bannerModal"
        >
        <b>Ubah Banner</b>
        </h3>
        <button
        type="button"
        class="btn-close"
        data-bs-dismiss="modal"
        aria-label="Close"
        ></button>
    </div>
    <div class="modal-body px-lg-5 pb-4">
        <div class="mt-lg-4">
            <div class="row">
                <div class="col-lg-8">
                    <form action="{{url('toko/banner',$toko->id)}}" enctype="multipart/form-data" method="POST">
                        @csrf   
                        <div class="mb-3">
                            <label for="formFile" class="form-label">Ubah Banner</label>
                            <input class="form-control" type="file" id="banner" name="banner"  accept="image/*" >
                            <span class="badge bg-primary mt-2 p-2">Ukuran Banner : 1000x200</span>
                        </div>
                        <div class="mb-3">
                            <button type="submit" class="btn btn-info">Ubah Banner</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
    {{-- End Banner Modal --}}


@endsection
