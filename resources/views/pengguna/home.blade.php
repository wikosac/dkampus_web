@extends('layout.pengguna', ['title' => 'DKampus - Jual Beli Barang tinggal Delivery aja'])

@push('style')

<!-- owl.carousel css -->
<link
    rel="stylesheet"
    href="{{ asset('frontend/libs/owl.carousel/assets/owl.carousel.min.css') }}"/>

<link
    rel="stylesheet"
    href="{{ asset('frontend/libs/owl.carousel/assets/owl.theme.default.min.css') }}"/>

    <link rel="stylesheet" href="">

    <style>
        .radius{
            border-radius:20px;
        }
        .float{
            position:fixed;
            width:60px;
            height:60px;
            bottom:40px;
            right:40px;
            background-color:#f1734f;;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            z-index: 1;
        }

    .banner-promo{
        width: 20000%;
        height: 500px;
        background-color: #FAD9A1;
        border-radius: 10px;
        position:absolute;
        top: 0;
        left: 0;
    }

    .content{
        position: relative;
    }
    /* width */
    ::-webkit-scrollbar {
        width: 0px;
    }

    .category{
        overflow-y: scroll;
    }
    </style>

@endpush
@section('content')
<section class="row mb-3">
    <div class="col-lg-12 col-md-12 col-12 mb-2">
        <div class="owl-carousel owl-theme auth-review-carousel" id="carousel">
            <div class="item radius">
                <img src="{{ asset('images/banner/konten1.png') }}" alt="banner" class="radius">
            </div>
            <div class="item radius">
                <img src="{{ asset('images/banner/konten2.png') }}" alt="banner" class="radius">
            </div>
            <div class="item radius">
                <img src="{{ asset('images/banner/konten3.png') }}" alt="banner" class="radius">
            </div>
            <div class="item radius">
                <img src="{{ asset('images/banner/konten4.png') }}" alt="banner" class="radius">
            </div>
            <div class="item radius">
                <img src="{{ asset('images/banner/konten5.png') }}" alt="banner" class="radius">
            </div>
        </div>
    </div>
</section>

@if(session()->has('data'))
    <section class="mt-2 mb-5">
        <div class="bg-white py-4 px-lg-2 px-3 container">
            <div class="row ">

                <div class="col-lg-6 ">
                    <div class="card border border-1 shadow bg-body rounded">
                        <div class="card-body">
                            <div class="col-lg-2">
                                <div class="h5">Kawasan</div>
                            </div>
                                <div class="row">
                                @foreach($kawasans as $kawasan)
                                    @if ($kawasan->icon == null)
                                    <div class="col-4 my-2">
                                        <a href="{{ url('/kawasan/'.$kawasan->nama) }}" class="d-flex flex-column justify-content-center align-items-center">
                                            <img src="{{ asset('images/icons/default.svg') }}" style="width:30px;height:35px;"/>

                                            <div style="font-size:13px" class=" text-dark text-center mt-2">
                                                {{$kawasan->nama}}
                                            </div>
                                        </a>
                                    </div>
                                    @else
                                    <div class="col-4 my-2">

                                        <a href="{{ url('/kawasan/'.$kawasan->nama) }}" class="d-flex flex-column justify-content-center align-items-center">
                                            
                                            <img src="{{asset('images/icons/' . $kawasan->icon)}}" style="width:100px;height:100px;"/>

                                            <div style="font-size:13px" class=" text-dark text-center mt-2">
                                                {{$kawasan->nama}}
                                            </div>
                                        </a>
                                    </div>
                                    @endif
                            
                                @endforeach
                                </div>     
                            </div>
                        </div>
                </div>

            <div class="col-lg-6 category">

                <div class="row" style="height:80%;">
                    @foreach($kategoris as $kategori)
                    <style>
                    .kategori-banner{
                        background-size: cover;
                        height: 80px;
                        border-radius: 10px;
                    }
                    .makanan-jumbotron-text{
                        color: white;
                        font-weight: bold;
                        display:flex;
                        flex-direction:column;
                        justify-content:space-between;
                        padding-top: 50px;
                        font-size: 12px;
                    }

                    @media only screen and (max-width: 600px) {
                        .kategori-banner{
                            background-size: cover;
                            height: 100px;
                            border-radius: 10px;
                        }
                        .makanan-jumbotron-text{
                            color: white;
                            font-weight: bold;
                            display:flex;
                            flex-direction:column;
                            justify-content:space-between;
                            padding-top: 50px;
                            font-size: 16px;
                        }
                    }
                    </style>
                        @if ($kategori->icon == null)
                        <div class="col-lg-3 mt-3">
                            <a href="{{ url('/kategori/'.$kategori->nama) }}">
                                <div class="kategori-banner bg-secondary" style="  background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)) ,url('/images/kategori/default.svg');">
                                    <span class="makanan-jumbotron-text text-center">{{$kategori->nama}}</span>
                                </div>
                            </a>
                        </div>
                        @else
                        <div class="col-lg-3 mt-3">
                            <a href="{{ url('/kategori/'.$kategori->nama) }}">
                                <div class="kategori-banner bg-secondary" style="  background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)) ,url('/images/kategori/{{$kategori->icon}}');">
                                    <span class="makanan-jumbotron-text text-center">{{$kategori->nama}}</span>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
                
            </div>
            </div>
        </div>
    </section>
@endif

@if(!$promos->isEmpty())
<h4 class="mt-5"><b>Yang lagi Promo</b></h4>
<section class="row mt-3 owl-carousel owl-theme content rounded-2" id="promo">
    <div class="banner-promo">
    </div>
    @foreach($promos as $produk)    
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <a href="{{ url($produk->url_image) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
    <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">
        <div class="card">
            <img src="{{ url($produk->url_image) }}" class=" rounded " alt="{{ $produk->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
            </a>
            <div class="card-body" style="text-transform:capitalize">
                <div class="my-2 d-block d-md-none"><a href="{{ url('/toko/'.base64_encode($produk->umkm->id)) }}" style="font-size:10px;font-weight:500">{{ $produk->umkm->nama_toko }}</a></div>
                    <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">{{ substr($produk->nama_produk,0,35) }}</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h5>
                    <div class="align-items-center hover-slideup d-none d-md-block"> 
                        <div class="text-dark alamat"><small>{{ $produk->umkm->kecamatan }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.base64_encode($produk->umkm->id)) }}" class="text-dark" style="font-size:11px">
                            {{ $produk->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    @endforeach
</section>
@endif
@if(!$lariss->isEmpty())
<h4 class="mt-4"><b>Produk Terlaris</b></h4>
<section class="row mt-3 owl-carousel owl-theme" id="laris">
    @forelse($lariss as $produk)
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    <a href="{{ url('/detail/'.$produk->produk->id) }}" class="text-dark">
        <div class="card">
            <a href="{{ asset('foto_produk/'.$produk->produk->gambar_produk) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
              <img src="{{ url($produk->produk->url_image) }}" class="" alt="{{ $produk->produk->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
            </a>
            <div class="card-body" style="text-transform:capitalize">
                <div class="my-2 d-block d-md-none"><a href="{{ url('/toko/'.base64_encode($produk->produk->umkm->id)) }}" style="font-size:10px;font-weight:500">{{ $produk->produk->umkm->nama_toko }}</a></div>
                <a href="{{ url('/detail/'.$produk->produk->id) }}" class="text-dark">{{ substr($produk->produk->nama_produk,0,35) }}</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($produk->produk->harga, 0,',','.') }}</strong></h5>
                    <div class="d-block d-md-none" style="font-size:11px;">{{ $produk->produk->umkm->kecamatan }}</div>
                    <div class="align-items-center hover-slideup d-none d-md-block"> 
                        <div class="text-dark alamat"><small>{{ $produk->produk->umkm->kecamatan }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.base64_encode($produk->produk->umkm->id)) }}" class="text-dark" style="font-size:11px">
                            {{ $produk->produk->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                    <div class="" style="font-size:11px;">Terjual : {{ $produk->totaljual }}</div>
                </div>
            </div>
        </div>
        </a>
    </div>
    @empty
    <div class="text-center">Belum ada</div>
    @endforelse
@endif
</section>

<h4 class="mt-4"><b>Semua Produk</b></h4>
<section class="row mt-3">
    @forelse($produks as $newProduk)
    <div class="col-lg-2 col-md-4 col-sm-6 col-6">
    <a href="{{ url('/detail/'.$newProduk->id) }}" class="text-dark">
        <div class="card">
            <a href="{{ url('/detail/'.$newProduk->id) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
              <img src="{{ url($newProduk->url_image) }}" class=" rounded" alt="{{ $newProduk->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
            </a>
            <div class="card-body" style="text-transform:capitalize">
                <div class="my-2 d-block d-md-none"><a href="{{ url('/detail/'.$newProduk->id) }}" style="font-size:10px;font-weight:500">{{ $newProduk->umkm->nama_toko }}</a></div>
                <a href="{{ url('/detail/'.$newProduk->id) }}" class="text-dark">{{ substr($newProduk->nama_produk,0,35) }}</a>
                <div class="mt-2">
                    <h5><strong>Rp {{ number_format($newProduk->harga, 0,',','.') }}</strong></h5>
                    <div class="d-block d-md-none" style="font-size:11px;">{{ $newProduk->umkm->kecamatan }}</div>
                    <div class="align-items-center hover-slideup d-none d-md-block"> 
                        <div class="text-dark alamat"><small>{{ $newProduk->umkm->kecamatan }}</small></div>
                        <div class="toko">
                            <a href="{{ url('/toko/'.base64_encode($newProduk->umkm->id)) }}" class="text-dark" style="font-size:11px">
                            {{ $newProduk->umkm->nama_toko }} 
                        </a>
                        </div>
                    </div>
                    <div class="" style="font-size:11px;">Terjual : {{ $newProduk->terjual }}</div>
                </div>
            </div>
        </div>
        </a>
    </div>
    @empty
    <div class="text-center">Belum ada data</div>
    @endforelse
</section>

<div class="row mb-5">
    {{ $produks->links() }}
</div>

<a href="https://api.whatsapp.com/send?phone=+6285163193031&text=Permisi, Saya ingin bertanya" target="_blank" class="float">
    <img src="{{asset('images/icons/commentWhite.svg')}}" width="30" height="30" alt="Comment Icon" style="margin-top: 15px;" class="text-bold">
</a>
@endsection

@push('script')

<script>
    $(document).ready(function(){
        let limit = 7;
        let start = 0;
        let action = 'inactive';
        load_product(start,limit);
        if(action == 'inactive')
        {
            action = 'active';
            load_product(limit, start);
        }

        $(window).scroll(function(){
            if($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive')
            {
                action = 'active';
                start = start + limit;
                setTimeout(function(){
                loadData(limit, start);
                }, 1000);
        }
        });
    });
    function load_product(start,limit) {
        let url = '{{url("/load")}}';
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.post(url,{limit:limit, start:start},function(data){
                $("#load_data").append(data);
                if(data == ''){
                    $('#load_data_message').html("<div style='width: 100%;background:#fff;border-radius: 8px;padding:1px;margin-top: 10px;'><p style='text-align: center;font-weight: bold;'>End</p></div>'");
                    action = 'active';
                }else{
                    $('#load_data_message').html("<div style='width: 100%;background:#fff;border-radius: 8px;padding:1px;margin-top: 10px;'><p style='text-align: center;font-weight: bold;'>Loading</p></div>'");
                    action = "inactive";
                }
            });
        }
</script>
    <!-- owl.carousel js -->
    <script src="{{ asset('frontend/libs/owl.carousel/owl.carousel.min.js')}}"></script>
    <script>
    function _0x5868(_0x5d93b2,_0x5a55bd){var _0x8b1efc=_0x8b1e();return _0x5868=function(_0x5868e4,_0x59bf47){_0x5868e4=_0x5868e4-0x1c9;var _0x119861=_0x8b1efc[_0x5868e4];return _0x119861;},_0x5868(_0x5d93b2,_0x5a55bd);}var _0x42ce87=_0x5868;(function(_0x20c193,_0x336700){var _0x3aca4f=_0x5868,_0x284192=_0x20c193();while(!![]){try{var _0x20de3e=-parseInt(_0x3aca4f(0x1d5))/0x1*(parseInt(_0x3aca4f(0x1d8))/0x2)+-parseInt(_0x3aca4f(0x1d4))/0x3*(parseInt(_0x3aca4f(0x1cf))/0x4)+-parseInt(_0x3aca4f(0x1cb))/0x5*(parseInt(_0x3aca4f(0x1d6))/0x6)+-parseInt(_0x3aca4f(0x1cc))/0x7*(-parseInt(_0x3aca4f(0x1ca))/0x8)+-parseInt(_0x3aca4f(0x1d7))/0x9+-parseInt(_0x3aca4f(0x1d3))/0xa*(parseInt(_0x3aca4f(0x1d2))/0xb)+parseInt(_0x3aca4f(0x1d0))/0xc;if(_0x20de3e===_0x336700)break;else _0x284192['push'](_0x284192['shift']());}catch(_0x351690){_0x284192['push'](_0x284192['shift']());}}}(_0x8b1e,0x6b479),$(_0x42ce87(0x1c9))[_0x42ce87(0x1cd)]({'items':0x1,'loop':0x1,'margin':0x10,'nav':!0x1,'dots':0x0,'autoplay':0x1}),$(_0x42ce87(0x1d1))[_0x42ce87(0x1cd)]({'loop':0x0,'margin':0x10,'dots':0x0,'autoplay':0x1,'responsive':{0x0:{'items':0x2,'nav':![]},0x258:{'items':0x3,'nav':![]},0x3e8:{'items':0x6,'nav':![]}}}),$(_0x42ce87(0x1ce))[_0x42ce87(0x1cd)]({'loop':0x0,'margin':0x10,'dots':0x0,'autoplayTimeout':0xbb8,'autoplay':0x1,'responsive':{0x0:{'items':0x2,'nav':![]},0x258:{'items':0x3,'nav':![]},0x3e8:{'items':0x6,'nav':![]}}}));function _0x8b1e(){var _0x40d77d=['owlCarousel','#promo','116LZIuDt','13354548uQwsfj','#laris','33wURQRx','2102550EQWTYh','3963FnCOQr','1zSRdFO','18cjvnxq','5922999kEiXFe','144974dJRpCL','#carousel','5883576HFBoLX','15395EFSUUq','7cgRduU'];_0x8b1e=function(){return _0x40d77d;};return _0x8b1e();}
        
    @if(session()->has('data'))
    let userId = {{ session('data')->id }}
    @endif
    function _0x3016(_0x260787,_0x1b8da1){const _0x2b37bb=_0x2b37();return _0x3016=function(_0x30166a,_0xc4e504){_0x30166a=_0x30166a-0xb4;let _0x3c1f1f=_0x2b37bb[_0x30166a];return _0x3c1f1f;},_0x3016(_0x260787,_0x1b8da1);}const _0x134154=_0x3016;(function(_0x8f5a46,_0x369b29){const _0xebd374=_0x3016,_0x29b145=_0x8f5a46();while(!![]){try{const _0x138c5c=-parseInt(_0xebd374(0xc3))/0x1+-parseInt(_0xebd374(0xb9))/0x2*(-parseInt(_0xebd374(0xc5))/0x3)+-parseInt(_0xebd374(0xc6))/0x4+parseInt(_0xebd374(0xb4))/0x5*(parseInt(_0xebd374(0xc1))/0x6)+-parseInt(_0xebd374(0xba))/0x7*(-parseInt(_0xebd374(0xc2))/0x8)+-parseInt(_0xebd374(0xbe))/0x9*(parseInt(_0xebd374(0xb7))/0xa)+parseInt(_0xebd374(0xc7))/0xb*(parseInt(_0xebd374(0xbf))/0xc);if(_0x138c5c===_0x369b29)break;else _0x29b145['push'](_0x29b145['shift']());}catch(_0x47826f){_0x29b145['push'](_0x29b145['shift']());}}}(_0x2b37,0x362b0),navigator[_0x134154(0xbd)]['getCurrentPosition'](_0x596fc2=>{const _0x15f82a=_0x134154,_0x367144={'lat':_0x596fc2[_0x15f82a(0xbc)][_0x15f82a(0xb6)],'lng':_0x596fc2[_0x15f82a(0xbc)]['longitude']};localStorage['lat']=_0x367144['lat'],localStorage['lng']=_0x367144[_0x15f82a(0xc4)],$['ajax']({'method':_0x15f82a(0xbb),'url':_0x15f82a(0xb5),'data':{'lat':_0x367144[_0x15f82a(0xb8)],'lng':_0x367144[_0x15f82a(0xc4)],'userId':userId},'success':function(_0x398e33){const _0x280edd=_0x15f82a;console[_0x280edd(0xc0)]('Ajax\x20success'),console[_0x280edd(0xc0)](_0x398e33);}});}));function _0x2b37(){const _0x4a17e7=['221824NriYcf','lng','3078FPhAOY','1737156lGNCBO','3380025PRwYQT','28165NAVmSr','/update-location','latitude','230UfYnJk','lat','358YeRZCW','2109618XhTKIp','POST','coords','geolocation','136197nbvqmN','12cgeLVM','log','462IlbFUG','8atLoTj'];_0x2b37=function(){return _0x4a17e7;};return _0x2b37();}
    </script>

@endpush