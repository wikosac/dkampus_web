@extends('layout.pengguna', ['title' => 'Syarat & Ketentuan - Dcreativ Indonesia' ])

@push('info')

<div class="d-none d-md-block d-lg-block">
    <div class="container-fluid d-flex justify-content-between py-2 px-4">
        <div class="">
            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>
        </div>
        <div class="">
            @if(session()->has('data') == '')
            <a href="{{ url('/login') }}" class="mx-1">Login</a> 
            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>
            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>
            @endif
        </div>
    </div>
</div>

@endpush

@section('content')

<div class="">
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <ul>
                        <li class="px-4 py-1"><a href="#">Kebijakan Privasi</a></li>
                        <li class="px-4 py-1"><a href="{{ url('/terms') }}">Syarat & Ketentuan</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <h3 class="mb-4">Syarat & Ketentuan</h3>
                    <p>Selamat datang di shop.dcreativ.id.</p>
                    <p>Syarat dan ketentuan yang tertulis dibawah ini yang mengatur segala layanan yang kami berikan kepada anda ketika anda menggunakan situs dan aplikasi kami. Pengguna diharapkan membaca dengan cermat dan seksama.</p>
                    <p>Dengan mendaftar/menggunakan layanan dari situs dan aplikasi kami, maka anda dianggap telah menyetujui semua Syarat dan Ketentuan yang berlaku.</p>
                    <div class="mt-3">
                        <h5>Syarat & Ketentuan menjadi Driver</h5>
                        <ol>
                            <li>Calon Driver wajib memiliki Android smartphone</li>
                            <li>Calon Driver wajib memiliki WhatsApp</li>
                            <li>Memiliki KTP aktif dengan ketentuan:</li>
                            <ul>
                                <li>Minimal berusia 17 tahun dan maksimal berusia 60 tahun pada saat pendaftaran (tidak diperlukan surat keterangan sehat)</li>
                            </ul>
                            <li>Memiliki SIM dengan ketentuan:</li>
                            <ul>
                                <li>SIM dengan status aktif</li>
                                <li>Membawa SIM C</li>
                            </ul>
                            <li>Memiliki STNK aktif</li>
                            <li>Tidak menggunakan narkotika</li>
                            <li>Tidak sedang dalam keadaan mabuk ketika mengantarkan pesanan</li>
                            <li>Berkelakuan sopan</li>
                            <li>Berkomitmen untuk mengantarkan pesanan ketika sudah mengambil pesanan tersebut.</li>

                        </ol>
                    </div>
                    <div class="mt-3">
                        <h5>Syarat & Ketentuan menjadi Mitra UMKM</h5>
                        <ol>
                            <li>Memiliki usaha minimal sudah berjalan selama 3 bulan</li>
                            <li>Memiliki KTP Pemilik usaha</li>
                            <li>Informasi lengkap usaha</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
