@extends('layout.pengguna', ['title' => 'Jual '.$produk->nama.' - Dcreativ Indonesia'])

@push('info')

<div class="d-none d-md-block d-lg-block">
    <div class="container-fluid d-flex justify-content-between pt-1">
        <div class="">
            DeliveryIn aja pesananmu
        </div>
        <div class="">
            @if(session()->has('data') == '')
            <a href="{{ url('/login') }}" class="mx-1">Login</a> 
            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>
            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>
            @endif
        </div>
    </div>
</div>

@endpush

@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Touchspin CSS -->
    <link
      href="{{ asset('frontend/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
      rel="stylesheet"
      type="text/css"
    />
    <!-- owl.carousel css -->
    <link
        rel="stylesheet"
        href="{{ asset('frontend/libs/owl.carousel/assets/owl.carousel.min.css') }}"/>
    
    <link
        rel="stylesheet"
        href="{{ asset('frontend/libs/owl.carousel/assets/owl.theme.default.min.css') }}"/>
    <meta property="og:image" itemprop="image" content="{{ asset('foto_produk/'.$produk->gambar_produk) }}">

@endpush

@section('content')

<div class="mt-3 mb-3 d-flex align-items-center">
  <a href="{{ url('/') }}" class="mx-1">Beranda</a>
  <i class="bx bxs-right-arrow mx-1"></i>
  <a href="{{ url('/') }}" class="mx-1">Produk</a>
  <i class="bx bxs-right-arrow mx-1"></i>
  <a href="#" class="mx-1 text-dark" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">{{ $produk->nama }}</a>
</div>
<section class="row mt-4 bg-white">
    <div class="col-lg-4 mb-4">
        
        <a href="{{ asset('foto_produk/'.$produk->foto) }}" class="progressive replace" style="max-height:250px;object-fit:contain;width:100%" data-lightbox="image">
              <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="image" style="object-fit:contain" />
        </a>
        
        <div class="mt-3 alert alert-info">Klik gambar untuk melihat secara detail</div>
    </div>
    <div class="col-lg-5 mb-5">
        <h3 style="font-weight:500">{{ $produk->nama }}</h3>
        <div class=""></div>
        <div class="mt-3">
            <h4><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h4>
        </div>
        <div class="mt-4">
            <!-- Nav tabs -->
                    <ul
                      class="nav nav-tabs nav-tabs-custom"
                      role="tablist"
                    >
                      <li class="nav-item">
                        <a
                          class="nav-link active"
                          data-bs-toggle="tab"
                          href="#home1"
                          role="tab"
                        >
                          <span class="d-block d-sm-block">Detail</span>
                        </a>
                      </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content p-3 text-muted">
                      <div class="tab-pane active" id="home1" role="tabpanel">
                        <div class="mt-3">
                            {!! $produk->deskripsi !!}
                        </div>
                      </div>
                      <div class="tab-pane" id="profile1" role="tabpanel">
                        <div class="mt-3">
                        </div>
                      </div>
                    </div>
                    
                    <div class="mt-3 border-top pt-3 border-2">
                            <div class="d-flex align-items-center">
                                <div class="" style="flex:1">
                                    <div style="font-size:15px" class="text-dark">
                                      <strong>{{ $produk->mitra->nama }}</strong>
                                    </div>
                                    <div class="mt-1 d-lg-block d-none">Bergabung sejak <strong>{{ $produk->mitra->created_at->diffForHumans() }}</strong></div>
                                    <div class="mt-1 d-lg-none d-block">Bergabung sejak <br/> <strong>{{ $produk->mitra->created_at->diffForHumans() }}</strong></div>
                                </div>
                                <div class="">
                                    <a href="{{ url('/mitra/'.$produk->mitra->slug) }}" class="btn btn-primary">Lihat Toko</a>
                                </div>
                            </div>
                        </div>
        </div>
    </div>
    <div class="col-lg-3 mb-5">
    
      @if($produk->visibilitas == 'sembunyikan')
      
      <div class="alert alert-info">Produk ini belum bisa dibeli.</div>
      
      @else
      <div class="card border">
        <div class="card-body">
          <h5><b>Atur Jumlah</b></h5>
          <form action="#" id="checkout_mitra" method="post">
            <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            <input type="hidden" name="harga" value="{{ $produk->harga }}">
          @csrf
            <div class="row align-items-center">
              <div class="col-lg-12 col-12">
                  <div class="mt-3 mb-2">
                    <input data-toggle="touchspin" type="text" value="1" id="qty" name="qty" />
                  </div>
              </div>
              <input type="hidden" name="subtotal" value="{{ $produk->harga }}" id="hideSubtotal">
              <input type="hidden" name="produk_id" value="{{ $produk->id }}">
            </div>
            <div class="mt-3 d-flex justify-content-between align-items-center" style="font-size:15px">
              <div class="">
                Subtotal
              </div>
              <div class="">
                <span id="subtotal">
                  <b>Rp {{ number_format($produk->harga, 0,',','.') }}</b>
                </span>
              </div>
            </div>
            <div class="mt-3">
              <button type="submit" class="btn btn-primary w-100 mb-2" name="cart" value="cart">Beli Sekarang</button>
            </div>
          </form>
          @if($produk->link_tokped != '' AND $produk->link_bukalapak != '' AND $produk->link_shopee != '')
          @else
          <p class="mt-3"><strong>Atau beli di ecommerce favoritmu</strong></p>
          @endif
          <div class="mt-2">
          <div class="row">
              <div class="col-6">
                  @if($produk->link_tokped)
                  <div class="mb-2">
                    <a href="{{$produk->link_tokped}}" class="" target="_blank">
                      <img src="{{ asset('/images/tokopedia.jpeg') }}" style="width:35px">
                      <span class="ms-1">Tokopedia</span>
                    </a>
                  </div>
                  @endif
                  @if($produk->link_shopee)
                  <div class="mb-2">
                    <a href="{{$produk->link_shopee}}" class="" target="_blank">
                      <img src="{{ asset('/images/shopee.jpeg') }}" style="width:35px">
                      <span class="ms-1">Shopee</span>
                    </a>
                  </div>
                  @endif
                  @if($produk->link_bukalapak)
                  <div class="mb-2">
                    <a href="{{$produk->link_bukalapak}}" class="" target="_blank">
                      <img src="{{ asset('/images/bukalapak.jpeg') }}" style="width:35px">
                      <span class="ms-1">Bukalapak</span>
                    </a>
                  </div>
                  @endif
              </div>
          </div>
        </div>
      </div>
      @endif
    </div>
</section>


@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- TouchSpin JS -->
    <script src="{{ asset('frontend/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('frontend/libs/owl.carousel/owl.carousel.min.js')}}"></script>
    
    <script>
            
        $("#laris").owlCarousel({
        loop:0,
        margin:16,
        dots:0,
        autoplay:1,
        responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:6,
            nav:false,
        }
        }
        });
        
      $("input[data-toggle='touchspin']").TouchSpin({
          min: 1,
          step: 1,
      });

      $("#qty").change(function(){
        var subtotal = $("#qty").val() * {{ $produk->harga }};
        var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(subtotal);
        $("#subtotal").html(`<b>` + rupiah + `</b>`)
        $("#hideSubtotal").val(subtotal)
      });

      $("#checkout_mitra").on('submit', function(e){
        e.preventDefault();
        const qty = $("#qty").val()
        var subtotal = $("#qty").val() * {{ $produk->harga }};
        var rupiah = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(subtotal);
        const hideSubtotal = $("#hideSubtotal").val()
        window.open(`https://api.whatsapp.com/send?phone=085156181331&text=Halo admin saya ingin memesan *'${qty} {{$produk->nama}} - Rp {{ number_format($produk->harga, 0,',','.') }}'* Total Belanja ${rupiah} dari *{{$produk->mitra->nama}}*%0A%0A{{ url('/mitra/'.$produk->mitra->slug) }} `, '_blank')
      })
    </script>

@endpush