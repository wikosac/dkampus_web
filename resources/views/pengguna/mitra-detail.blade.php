@extends('layout.pengguna', ['title' => $mitra->nama.' - DShop' ])



@push('info')



<div class="d-none d-md-block d-lg-block">

    <div class="container-fluid d-flex justify-content-between py-2 px-4">

        <div class="">

            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>

        </div>

        <div class="">

            @if(session()->has('data') == '')

            <a href="{{ url('/login') }}" class="mx-1">Login</a> 

            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>

            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>

            @endif

        </div>

    </div>

</div>



@endpush



@section('content')



<section class="card shadow-none">

    <div class="card-body px-1 px-lg-3">

        <div class="d-flex align-items-center">

            <div class="">

                <img src="{{ asset('/foto_mitra/'.$mitra->foto) }}" style="width:100px">

            </div>

            <div class="ms-3 ms-lg-4">

                <div class="d-flex align-items-center">

                    <h4 class=""> <img src="{{ asset('/images/mitracreativ.jpeg') }}" style="width:14px" class="me-1">  <strong>{{ $mitra->nama }}</strong></h4>

                </div>

                <div class="" style="text-transform:capitalize">

                     <h6><i class="mdi mdi-google-maps" style="font-size:14px"></i> {{ $mitra->alamat }}</h6>

                </div>

                <div class="mt-4" style="text-transform:capitalize">

                     <div class="row">

                         <div class="col-6">

                             @if($mitra->tokopedia)

                             <div class="mb-2">

                                 <a href="{{$mitra->tokopedia}}" class="d-flex" target="_blank">

                                     <img src="{{ asset('/images/tokopedia.jpeg') }}" style="width:15px">

                                     <span class="ms-1">Tokopedia</span>

                                </a>

                            </div>

                             @endif

                             @if($mitra->shopee)

                             <div class="mb-2">

                                 <a href="{{$mitra->shopee}}" class="d-flex" target="_blank">

                                     <img src="{{ asset('/images/shopee.jpeg') }}" style="width:15px">

                                     <span class="ms-1">Shopee</span>

                                </a>

                            </div>

                             @endif

                         </div>

                         <div class="col-6">

                         @if($mitra->bukalapak)

                             <div class="mb-2">

                                 <a href="{{$mitra->bukalapak}}" class="d-flex" target="_blank">

                                     <img src="{{ asset('/images/bukalapak.jpeg') }}" style="width:17px">

                                     <span class="ms-1">Bukalapak</span>

                                </a>

                            </div>

                             @endif

                             @if($mitra->ig)

                             <div class="mb-2">

                                 <a href="{{$mitra->ig}}" class="d-flex" target="_blank">

                                     <img src="{{ asset('/images/ig.jpeg') }}" style="width:15px">

                                     <span class="ms-1">Instagram</span>

                                </a>

                            </div>

                             @endif

                         </div>

                     </div>

                </div>

            </div>

        </div>

        <div class="mt-3">

            <h6><strong>Tentang</strong></h6>

            <div class="mt-1">

                {!! $mitra->keterangan !!}

            </div>

        </div>

    </div>

</section>



<!-- Nav tabs -->

<ul

    class="nav nav-tabs nav-tabs-custom"

    role="tablist"

>

    <li class="nav-item">

    <a

        class="nav-link active"

        data-bs-toggle="tab"

        href="#home1"

        role="tab"

    >

        <span class="d-block d-sm-block">Produk Mitra</span>

    </a>

    </li>

</ul>



<!-- Tab panes -->

<div class="tab-content p-3 text-muted mb-5">

    <div class="tab-pane active" id="home1" role="tabpanel">

    <section class="row">

    @forelse($mitra->produks as $produk)

    <div class="col-lg-2 col-md-3 col-sm-4 col-6">

        <div class="card">

            <a href="{{ url('mitra/produk/'.$produk->slug) }}">

                <img src="{{ asset('foto_produk/'.$produk->foto) }}" alt="" class="card-img-top" style="height:150px;object-fit:cover">

                <div class="card-body">

                    <div class="py-2 text-truncate">{{ $produk->nama }}</div>

                    <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h5>

                </div>

            </a>

        </div>

    </div>

    @empty

    <div class="col-12 text-center">Belum ada produk</div>

    @endforelse

</section>

    </div>

</div>



@endsection

