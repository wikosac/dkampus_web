@extends('layout.pengguna', ['title' => 'Daftar Akun - DKampus']
)

@push('style')
<link rel="stylesheet" href="{{ asset('assets/select2-4.0.6-rc.1/dist/css/select2.min.css')}}">
@endpush

@section('content')

<div class="account-pages pt-sm-5">
      <div class="">
        <div class="row justify-content-center">
          <div class="col-12 col-md-8 col-lg-6 col-xl-5">
            <div class="card overflow-hidden">
              <div class="bg-warning bg-soft">
                <div class="row">
                  <div class="col-7">
                    <div class="text-warning p-4">
                      <h5 class="text-warning">Selamat Datang di D'kampus!</h5>
                      <p>Silahkan Buat Akun anda</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body pt-0">
                <div class="p-2 pt-4">
                <form action="{{ url('register/store') }}" method="post">
            <div class="form_container">
            @csrf
                <div class="private box">
                    <div class="row no-gutters">
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="text" name="nama_depan" class="form-control" value="{{ old('nama_depan') }}" placeholder="Nama Depan">
                                @if($errors->has('nama_depan'))
                                  <div class="danger" style="color: red;">{{ $errors->first('nama_depan') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="text" name="nama_belakang" class="form-control" value="{{ old('nama_belakang') }}" placeholder="Nama Belakang">
                                @if($errors->has('nama_belakang'))
                                  <div class="danger" style="color: red;">{{ $errors->first('nama_belakang') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
                                @if($errors->has('email'))
                                  <div class="danger" style="color: red;">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <input type="text" name="username" value="{{ old('username') }}" class="form-control" placeholder="Username">
                                @if($errors->has('username'))
                                  <div class="danger" style="color: red;">{{ $errors->first('username') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-6 mb-3" >
                            <div class="form-group">
                                <input type="password" name="password1" value="{{ old('password1') }}" class="form-control" placeholder="Password">
                                @if($errors->has('password'))
                                  <div class="danger" style="color: red;">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-6 mb-3">
                            <div class="form-group">
                                <input type="password" name="password2" value="{{ old('password2') }}" class="form-control" placeholder="Konfirmasi Password">
                                @if($errors->has('password'))
                                  <div class="danger" style="color: red;">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="form-group">
                                <input type="text" name="no_telephone" class="form-control" placeholder="Nomor Whatsapp" value="{{ old('no_telephone') }}">
                                @if($errors->has('no_telephone'))
                                  <div class="danger" style="color: red;">{{ $errors->first('no_telephone') }}</div>
                                @endif
                            </div>
                        </div>
                    <div class="col-12 mt-3">
                <label class="form-label">Provinsi:</label>
                <div class="">
                  <select class="form-control" name="provinsi" id="provinsi">
                    <option></option>
                    @foreach($provinces as $provinsi)
                    <option value="{{$provinsi->name}}" data-id="{{$provinsi->id}}">{{$provinsi->name}}</option>
                    @endforeach
                  </select>
                </div>
                @if($errors->has('provinsi'))
                    <div class="danger" style="color: red;">{{ $errors->first('provinsi') }}</div>
                @endif
              </div>
              <div class="col-12 mt-3">
                <label class="form-label" >Kota/Kabupaten:</label>
                <div class="">          
                  <select class="form-control" name="kota" id="kota">
                    <option></option>
                  </select>
                </div>
                @if($errors->has('kota'))
                  <div class="danger" style="color: red;">{{ $errors->first('kota') }}</div>
                @endif
              </div>
              <div class="col-12 mt-3">
                <label class="form-label" >Kecamatan:</label>
                <div class="">          
                  <select class="form-control" name="kecamatan" id="kecamatan">
                    <option></option>
                  </select>
                </div>
                @if($errors->has('kecamatan'))
                  <div class="danger" style="color: red;">{{ $errors->first('kecamatan') }}</div>
                @endif
              </div>
              <div class="col-12 mt-3">
                <label class="form-label" >Kelurahan:</label>
                <div class="">          
                  <select class="form-control" name="kelurahan" id="kelurahan">
                    <option></option>
                  </select>
                </div>
                @if($errors->has('kelurahan'))
                  <div class="danger" style="color: red;">{{ $errors->first('kelurahan') }}</div>
                @endif
              </div>
              <div class="col-12 pl-1 mt-3">
                 <div class="form-group">
                    <textarea name="alamat" class="form-control" placeholder="Alamat">{{ old('alamat') }}</textarea>
                    </div>
                    @if($errors->has('alamat'))
                      <div class="danger" style="color: red;" >{{ $errors->first('alamat') }}</div>
                    @endif
                  </div>
                    </div>
                        <div class="col-12 pl-1 mt-3">
                            <div class="">
                                <input type="checkbox" name="terms" required> Saya menyetujui <a href="{{ url('/terms') }}" target="_blank">Syarat & Ketentuan</a> yang berlaku
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /private -->
                <hr>
                <button type="submit" class="btn btn-primary w-100">Daftar</button>
            </div>
            </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection

@push('script')

<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/select2.min.js') }}"></script>   
<script src="{{ asset('assets/select2-4.0.6-rc.1/dist/js/i18n/id.js') }}"></script> 
<script src="{{ asset('assets/js/register.js') }}"></script>

@endpush
