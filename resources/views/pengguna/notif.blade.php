@extends('layout.pengguna', ['title' => 'Keranjang - Dcreativ Indonesia'])

@push('info')

<div class="d-none d-md-block d-lg-block">
    <div class="container-fluid d-flex justify-content-between py-2 px-4">
        <div class="">
            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>
        </div>
        <div class="">
            @if(session()->has('data') == '')
            <a href="{{ url('/login') }}" class="mx-1">Login</a> 
            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>
            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>
            @endif
        </div>
    </div>
</div>

@endpush

@section('content')

<div class="col-lg-12 text-center align-item-center justify-content-center h-100">
    <div class="mt-5">
        <h3><b>Wah Notifikasi mu Kosong</b></h3>
        <p>Yuk beli produk yang kamu inginkan sekarang</p>
    </div>
</div>

@endsection

@push('script')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1MgLuZuyqR_OGY3ob3M52N46TDBRI_9k&callback=initMap&v=weekly" async></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endpush