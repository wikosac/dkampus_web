@extends('layout.pengguna', ['title' => 'Hasil Pencarian '.Request()->q ])

@push('info')

<div class="d-none d-md-block d-lg-block">
    <div class="container-fluid d-flex justify-content-between py-2 px-4">
        <div class="">
            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>
        </div>
        <div class="">
            @if(session()->has('data') == '')
            <a href="{{ url('/login') }}" class="mx-1">Login</a> 
            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>
            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>
            @endif
        </div>
    </div>
</div>

@endpush

@push('style')

<!-- owl.carousel css -->
<link
    rel="stylesheet"
    href="assets/libs/owl.carousel/assets/owl.carousel.min.css"/>

<link
    rel="stylesheet"
    href="assets/libs/owl.carousel/assets/owl.theme.default.min.css"/>

@endpush

@section('content')
<section class="row mt-2">
    <div class="col-lg-3 mb-3">
        <div class="d-lg-none d-md-none d-block">
        <h5 class="d-flex justify-content-between border rounded py-2 px-3" data-bs-toggle="collapse" data-bs-target="#showFilter">
            <span>Filter</span>
            <i class="bx bx-filter"></i>
        </h5>
        <form action="{{ url('/search') }}" method="get">
        <input type="hidden" name="q" value="{{ Request()->q }}">
        <div class="card collapse" id="showFilter">
            <div class="card-body">
                <strong>Urutkan</strong>
                <table class="table table-borderless mt-2">
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" name="sort" type="radio" value="desc" id="terbaru" {{ Request()->sort == 'desc' ? 'checked' : 'checked' }}>
                              <label class="form-check-label" for="terbaru">
                                Terbaru
                              </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="sort" value="asc" id="terlama"  {{ Request()->sort == 'asc' ? 'checked' : '' }}>
                              <label class="form-check-label" for="terlama">
                                Terlama
                              </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="sort" value="laris" id="laris"  {{ Request()->sort == 'laris' ? 'checked' : '' }}>
                              <label class="form-check-label" for="laris">
                                Terlaris
                              </label>
                            </div>
                        </td>
                    </tr>
                </table>
                <strong>Lokasi</strong>
                <table class="table table-borderless mt-2">
                    @foreach($kawasans as $kawasan)
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" name="lok" type="radio" value="{{ $kawasan->nama }}" id="abungsemuli" {{ Request()->lok == $kawasan->nama ? 'checked' : '' }}>
                              <label class="form-check-label" for="abungsemuli">
                                {{ $kawasan->nama }}
                              </label>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <input type="submit" class="btn btn-primary w-100" value="Terapkan">
            </div>
        </div>
        
        
        </form>
        </div>
        
        <div class="d-lg-block d-md-block d-none">
        <h3 class="d-flex justify-content-between">
            <span>Filter</span>
            <i class="bx bx-filter"></i>
        </h3>
        <form action="{{ url('/search') }}" method="get">
        <input type="hidden" name="q" value="{{ Request()->q }}">
        <div class="card">
            <div class="card-body">
                <strong>Urutkan</strong>
                <table class="table table-borderless mt-2">
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" name="sort" type="radio" value="desc" id="terbaru" {{ Request()->sort == 'desc' ? 'checked' : 'checked' }}>
                              <label class="form-check-label" for="terbaru">
                                Terbaru
                              </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="sort" value="asc" id="terlama"  {{ Request()->sort == 'asc' ? 'checked' : '' }}>
                              <label class="form-check-label" for="terlama">
                                Terlama
                              </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="sort" value="laris" id="laris"  {{ Request()->sort == 'laris' ? 'checked' : '' }}>
                              <label class="form-check-label" for="laris">
                                Terlaris
                              </label>
                            </div>
                        </td>
                    </tr>
                </table>
                <strong>Lokasi</strong>
                <table class="table table-borderless mt-2">
                    @foreach($kawasans as $kawasan)
                    <tr class="py-0">
                        <td class="py-1">
                            <div class="form-check">
                              <input class="form-check-input" name="lok" type="radio" value="{{ $kawasan->nama }}" id="abungsemuli" {{ Request()->lok == $kawasan->nama ? 'checked' : '' }}>
                              <label class="form-check-label" for="abungsemuli">
                                {{ $kawasan->nama }}
                              </label>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <input type="submit" class="btn btn-primary w-100" value="Terapkan">
            </div>
        </div>
        
        
        </form>
        </div>
    </div>
    <div class="col-lg-9">
        <!-- Nav tabs -->
<ul
    class="nav nav-tabs nav-tabs-custom"
    role="tablist"
>
    <li class="nav-item">
    <a
        class="nav-link active"
        data-bs-toggle="tab"
        href="#home1"
        role="tab"
    >
    <span class="d-block d-sm-block" style="font-size:17px;font-weight:600"><i class="bx bx-archive"></i> Produk</span>
    </a>
    </li>
    <li class="nav-item">
    <a
        class="nav-link"
        data-bs-toggle="tab"
        href="#profile1"
        role="tab"
    >
        <span class="d-block d-sm-block" style="font-size:17px;font-weight:600"><i class="bx bx-store"></i> Toko</span>
    </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content p-3 text-muted mb-5">
    <div class="tab-pane active" id="home1" role="tabpanel">
        <p class="mt-2 mb-3">Menampilkan hasil produk untuk <b>" {{ Request()->q }} "</b> di lokasi <b>{{ Request()->lok ? Request()->lok : 'Semua' }}</b></p>
    <div class="row mt-4">
            @forelse($produks as $produk)
            <div class="col-lg-3 col-md-4 col-sm-12 col-12 mb-4">
                <div class="card">
                    <a href="{{ asset('foto_produk/'.$produk->gambar_produk) }}" class="progressive replace" style="max-height:150px;object-fit:cover;width:100%">
                      <img src="https://ikaldki.id/asset/img/default.jpg" class="preview" alt="{{ $produk->nama_produk }}" style="height:150px;object-fit:cover;width:100%" />
                    </a>
                    <div class="card-body" title="{{ $produk->nama_produk }}">
                        <div class="my-2 d-block d-md-none" style="font-size:10px;font-weight:500">{{ $produk->umkm->nama_toko }}</div>
                        <a href="{{ url('/detail/'.$produk->id) }}" class="text-dark">{{ substr($produk->nama_produk,0,35) }}</a>
                        <div class="mt-2">
                            <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }}</strong></h5>
                            <div class="d-block d-md-none" style="font-size:11px;">{{ $produk->umkm->kecamatan }}</div>
                            <div class="align-items-center hover-slideup d-none d-md-block"> 
                                <div class="text-dark alamat"><small>{{ $produk->umkm->kecamatan }}</small></div>
                                <div class="toko">
                                    <a href="{{ url('/toko/'.$produk->umkm->id) }}" class="text-dark" style="font-size:11px">  {{ $produk->umkm->nama_toko }} 
                                </a>
                                </div>
                            </div>
                            <div class="" style="font-size:11px;">Terjual : {{ $produk->terjual }}</div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-lg-12 text-center">
                <h4>Produk yang anda cari tidak ditemukan.</h4>
            </div>
            @endforelse
        </div>
    </div>
    <div class="tab-pane" id="profile1" role="tabpanel">
    <p class="mt-2 mb-3">Menampilkan toko yang menjual produk <b>" {{ Request()->q }} "</b></p>
    <div class="row mt-5">
        @foreach($tokos as $toko)
            <div class="col-lg-4 col-md-4 col-sm-12 col-12 mb-2" title="{{ $toko->nama_toko }}">
                <div class="card border">
                    <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="ms-2" style="flex:1">
                            <div>
                                <a href="{{ url('/toko/'.$toko->id) }}" class="text-dark">
                                    <b>{{ substr($toko->nama_toko, 0, 16) }}..</b>
                                </a>
                            </div>
                            <small>{{ $toko->kecamatan }}</small>
                        </div>
                        <div class="" style="width:40px">
                            <a href="{{ url('/toko/'.base64_encode($toko->id)) }}" class="btn btn-primary btn-sm">Lihat</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </div>
</div>
    </div>
</section>
@endsection

@push('script')

    <!-- owl.carousel js -->
    <script src="assets/libs/owl.carousel/owl.carousel.min.js"></script>
    
    <script>
    $("#carousel").owlCarousel({
        items:1,
        loop:1,
        margin:16,
        nav:!1,
        dots:!0,
        autoplay:1
        });
    </script>

@endpush