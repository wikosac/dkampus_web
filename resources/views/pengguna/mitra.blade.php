@extends('layout.pengguna', ['title' => 'Official Mitra - DShop'])



@push('info')



<div class="d-none d-md-block d-lg-block">

    <div class="container-fluid d-flex justify-content-between py-2 px-4">

        <div class="">

            <a href="https://dcreativ.id" class="text-dark" style="font-size:small">Tentang Dcreativ Indonesia</a>

        </div>

        <div class="">

            @if(session()->has('data') == '')

            <a href="{{ url('/login') }}" class="mx-1">Login</a> 

            <a href="{{ url('/register-v2') }}" class="mx-1">Register</a>

            <a href="{{ url('/umkm/register') }}" class="mx-1">Buka Toko</a>

            @endif

        </div>

    </div>

</div>



@endpush



@push('style')



<!-- owl.carousel css -->

<link

    rel="stylesheet"

    href="{{ asset('frontend/libs/owl.carousel/assets/owl.carousel.min.css') }}"/>



<link

    rel="stylesheet"

    href="{{ asset('frontend/libs/owl.carousel/assets/owl.theme.default.min.css') }}"/>



@endpush



@section('content')

<section class="row mb-3">

    <div class="col-lg-8 col-md-8 col-12 mb-2">

        <div class="owl-carousel owl-theme auth-review-carousel" id="carousel">

            <!--<div class="item rounded">-->

            <!--        <img src="{{ asset('images/bontot.png') }}" alt="" class="rounded">-->

            <!--</div>-->

            <div class="item rounded">

                    <img src="{{ asset('images/acara.jpeg') }}" alt="" class="rounded">

            </div>

            <div class="item rounded">

                    <img src="{{ asset('images/kepo.png') }}" alt="" class="rounded">

            </div>

            <div class="item rounded">

                    <img src="{{ asset('images/semuli.png') }}" alt="" class="rounded">

            </div>

            <div class="item rounded">

                <img src="{{ asset('images/1.png') }}" alt="" class="rounded">

            </div>

            <div class="item rounded">

                <img src="{{ asset('images/2.png') }}" alt="" class="rounded">

            </div>

            <div class="item rounded">

                <img src="{{ asset('frontend/images/iklan.png') }}" alt="" class="">

            </div>



        </div>

    </div>

    <div class="col-lg-4 col-md-4 col-12 mb-2 d-none d-md-block d-lg-block">

        <div class="row mb-2">

            <div class="col-lg-12">

                <img src="{{ asset('frontend/images/iklan.png') }}" alt="" class="w-100">

            </div>

        </div>

        <div class="row mb-1">

            <div class="col-lg-12">

                <img src="{{ asset('frontend/images/iklan1.png') }}" alt="" class="w-100">

            </div>

        </div>

    </div>

</section>



<h5 class="mt-5"><b>Mitra Kami</b></h5>

<section class="row mt-3">

    @foreach($mitras as $mitra)

    <div class="col-lg-2 col-md-3 col-6">

    <a href="{{ url('/mitra/'.$mitra->slug) }}" class="text-dark">

        <div class="card">

            <img src="{{ asset('/foto_mitra/'.$mitra->foto) }}" class="card-img-top p-3 rounded" alt="" style="object-fit:cover;width:100%" />

        </div>

        </a>

    </div>

    @endforeach

</section>



<h5 class="mt-5"><b>Produk Mitra</b></h5>

<section class="row mt-3">

    @forelse($produks as $produk)

    <div class="col-lg-2 col-md-3 col-sm-4 col-6">

        <div class="card">

            <a href="{{ url('mitra/produk/'.$produk->slug) }}">

                <img src="{{ asset('foto_produk/'.$produk->foto) }}" alt="" class="card-img-top" style="height:150px;object-fit:cover">

                <div class="card-body">

                    <div class="my-2 d-block d-md-none" style="font-size:10px;font-weight:500">{{ $produk->mitra->nama }}</div>

                    <div class="py-2 text-dark text-truncate">{{ $produk->nama }}</div>

                    <h5><strong>Rp {{ number_format($produk->harga, 0,',','.') }} </strong></h5>

                <div class="align-items-center hover-slideup d-none d-md-block"> 

                    <div class="text-dark alamat"><small>{{ $produk->mitra->alamat }}</small></div>

                    <div class="toko">

                        <a href="{{ url('/mitra/'.$produk->mitra->slug) }}" class="text-dark" style="font-size:11px">

                        {{ $produk->mitra->nama }} 

                    </a>

                    </div>

                </div>

                </div>

            </a>

        </div>

    </div>

    @empty

    <div class="col-12 text-center">Belum ada produk</div>

    @endforelse

</section>

<div class="row mb-5">

    {{ $produks->links() }}

</div>

@endsection



@push('script')



    <!-- owl.carousel js -->

    <script src="{{ asset('frontend/libs/owl.carousel/owl.carousel.min.js')}}"></script>

    

    <script>

    $("#carousel").owlCarousel({

        items:1,

        loop:1,

        margin:16,

        nav:!1,

        dots:0,

        autoplay:1

        });

        

        $("#laris").owlCarousel({

        loop:0,

        margin:16,

        dots:0,

        autoplay:1,

        responsive:{

        0:{

            items:2,

            nav:false

        },

        600:{

            items:3,

            nav:false

        },

        1000:{

            items:6,

            nav:false,

        }

        }

        });

        

        $("#promo").owlCarousel({

        loop:0,

        margin:16,

        dots:0,

        autoplayTimeout:3000,

        autoplay:1,

        responsive:{

        0:{

            items:2,

            nav:false

        },

        600:{

            items:3,

            nav:false

        },

        1000:{

            items:6,

            nav:false,

        }

        }

        });

    </script>



@endpush