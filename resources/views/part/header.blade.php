<header id="page-topbar" class="shadow-sm" style="z-index:5">
        @stack('info')

    <div class="navbar-header">
        <div class="d-flex align-items-center">

            <div class="navbar-brand-box">
                <a href="{{ url('/') }}" class="logo logo-dark logo-big">
                    <span class="logo-sm">
                        <span class="font-weight-bold" style="font-size:22px;font-weight:500"><img src="{{ asset('asset1/img/Dkampus 1.png') }}" alt="" width="80" height="80"></span>
                    </span>
                    <span class="logo-lg">
                        <span class="font-weight-bold" style="font-size:22px;font-weight:500"><img src="{{ asset('asset1/img/Dkampus 1.png') }}" alt="" width="80" height="80"></span>
                    </span>
                </a>

                <a href="{{ url('/') }}" class="logo logo-light logo-big">
                    <span class="logo-sm">
                        <span class="font-weight-bold" style="font-size:22px;font-weight:500"><img src="{{ asset('asset1/img/Dkampus 1.png') }}" alt="" width="80" height="80"></span>
                    </span>
                    <span class="logo-lg">
                        <span class="font-weight-bold" style="font-size:22px;font-weight:500"><img src="{{ asset('asset1/img/Dkampus 1.png') }}" alt="" width="80" height="80"></span>
                    </span>
                </a>

            </div>

            <!-- App Search-->
            <form class="app-search d-flex justify-content-center" action="{{ url('/search') }}" method="get">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Cari Produk" name="q" autocomplete="off" size="93"/>
                    <span class="bx bx-search"></span>
                </div>
            </form>
            <div class="header-item btn noti-icon waves-effect mx-1 d-flex align-items-center justify-content-center">
                <a href="{{ url('/cart') }}" class="font-size-16"><i class="fas fa-shopping-cart"></i></a>
            </div>

            <div class="header-item btn noti-icon waves-effect mx-1 d-flex align-items-center justify-content-center">
                @if (session()->has('data'))
                    @if (session('data')->level == "user")
                        <a href="{{ url('umkm/register') }}" class="font-size-16"><i class="fas fa-store"></i></a>
                    @else
                        <a href="{{ url('umkm/dashboard') }}" class="font-size-16"><i class="fas fa-store"></i></a>
                    @endif
                @else
                        <a href="{{ url('umkm/register') }}" class="font-size-16"><i class="fas fa-store"></i></a>
                @endif
            </div>
  
            @if(session()->has('data'))
            <div class="dropdown d-inline-block">
                <button
                    type="button"
                    class="btn header-item noti-icon waves-effect d-none d-xl-inline-block"
                    id="page-header-user-dropdown"
                    data-bs-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                    <i class="bx bx-user d-none font-size-16"></i>
                    <span class="d-none d-xl-inline-block ms-1" key="t-henry">{{ session('data')['username'] }}</span >
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a class="dropdown-item text-dark" href="{{ url('/edit-profile') }}">
                        <i
                            class="
                      bx bx-user
                      font-size-16
                      align-middle
                      me-1
                      text-dark
                    "></i>
                        <span key="t-logout">Edit Profile</span>
                    </a >
                    <a class="dropdown-item text-dark" href="{{ url('/pesanan') }}">
                        <i
                            class="
                      bx bx-spreadsheet
                      font-size-16
                      align-middle
                      me-1
                      text-dark
                    "></i>
                        <span key="t-logout">Daftar Pesanan</span>
                    </a >
                    <a class="dropdown-item text-danger" href="{{ url('/keluar') }}">
                        <i
                            class="
                      bx bx-power-off
                      font-size-16
                      align-middle
                      me-1
                      text-danger
                    "></i>
                        <span key="t-logout">Logout</span>
                    </a >
                </div>
            </div>
            @else
            <div class="header-item btn oti-icon mx-1 d-flex align-items-center d-none d-md-none d-sm-none d-lg-block mt-4">
                <div class="d-grid gap-2 d-md-block">
                    <a href="{{url('login')}}" class="btn btn-sm" style="background-color:#F77E21;color:white;">Masuk</a>
                    {{-- <a href="{{url('register-v2')}}" class="btn btn-sm btn-outline-primary">Daftar</a> --}}
                </div>
            </div>
            @endif

          

        </div>
        <div class="d-flex justify-content-lg-center">
            <button
                type="button"
                class="
                btn btn-sm
                font-size-16
                d-lg-none
                header-item
                waves-effect waves-light
              "
              id="menu">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </div>
    </div>
</header>


