<footer class="d-lg-block d-md-block pb-4 text-white bg-dark">
    <div class="container-fluid">
      <div class="row pb-3 border-bottom align-items-center">
          <div class="col-lg-4 mb-3">
              <h6 class="mt-4 text-white"><b>Dcreativ</b></h6>
              <ul style="list-style:none;margin:0;padding:0">
                  <li class="text-secondary"><a href="https://dcreativ.id" class="text-secondary">Tentang Dcreativ</a></li>
              </ul>
              
              <h6 class="mt-4 text-white"><b>Hubungi kami</b></h6>
              <ul style="list-style:none;margin:0;padding:0">
                  <li class="text-secondary"> CS : <a href="https://wa.me/6285156181331" class="text-secondary">+6285156181331</a></li>
              </ul>
          </div>
          <div class="col-lg-4 mb-3">
              <h6 class="mt-4 text-white"><b>Bantuan dan Panduan</b></h6>
              <ul style="list-style:none;margin:0;padding:0">
                  <li class="text-secondary"><a href="{{ url('/terms') }}" class="text-secondary">Syarat dan Ketentuan</a></li>
                  <li class="text-secondary"><a href="#" class="text-secondary">Kebijakan Privasi</a></li>
              </ul>
              
              <h6 class="mt-4 text-white"><b>Ikuti kami</b></h6>
              <div>
                  <a href="https://www.instagram.com/dcreativ_shop"><img src="{{ asset('icons/ic-ig.svg') }}"/></a>
              </div>
          </div>
          <div class="col-lg-4 mt-3">
          </div>
      </div>
      <div class="row pt-3">
        <div class="col-sm-6">
          <script>
            document.write(new Date().getFullYear());
          </script>
          © DKampus Powered by Dcreativ Indonesia
        </div>
        <div class="col-sm-6">
          <div class="text-sm-end d-none d-sm-block">
            
          </div>
        </div>
      </div>
    </div>
  </footer>