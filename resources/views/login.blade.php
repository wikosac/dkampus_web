@extends('layout.loginLayout')
@section('content')
<div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-4">
    <div class="container-fluid">
        <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
            <div class="col mx-auto">
                <div class="card mt-5 mt-lg-0">
                    <div class="card-body">
                        @if(session('pesan'))
                            <div class="alert alert-success">{{ session('pesan') }}</div>
                        @endif
                        <div class="p-4 rounded">
                            <div class="text-center">
                                <h3 class="">
                                    <img src="{{asset('images/Logo/Dkampus_light.svg')}}" alt="logo" width="200" height="200" srcset="">
                                </h3>
                            </div>
                            <div class="form-body">
                                <form action="{{ url('umkm/auth') }}" method="post">    
                                    @csrf
                                    <div class="col-12">
                                        <label for="inputEmailAddress" class="form-label">Username</label>
                                        <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                    </div>
                                    <div class="col-12">
                                        <label for="inputChoosePassword" class="form-label">Enter Password</label>
                                        <div class="input-group" id="show_hide_password">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                            
                                            <a href="javascript:;" class="input-group-text bg-transparent"><i class='bx bx-hide'></i></a>

                                        </div>
                                    </div>
                                    <div class="col-12 mt-4">
                                        <div class="d-grid">
                                            <button type="submit" class="btn btn-primary"><i class="bx bxs-lock-open"></i>Masuk</button>
                                        </div>
                                        <div class="d-grid mt-2">
                                            <a href="{{url('register-v2')}}" class="btn btn-sm btn-outline-primary">Daftar</a>
                                        </div>
                                    </div>
                                    <hr>
                                    <p class="text-muted text-center">atau Masuk dengan</p>
                                    <div class="col-12 mt-4">
                                        <div class="d-grid">
                                            <a href="{{route('auth.google')}}" type="submit" class="btn btn-primary"><i class='bx bxl-google'></i> Google</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
@endsection


  