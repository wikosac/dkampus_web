@extends('layout.pengguna', ['title' => 'Daftar Pesanan - '.session('data')['username'] ])

@section('content')
@push('style')

<link href="{{ asset('asset1/css/cart.css') }}" rel="stylesheet">

@endpush

<div class="mt-3">
<div class="row">
<div class="col-lg-12">
<h4 class="mb-4 mb-4"><b>Daftar Pesanan</b></h4>
@if ($message = session('failed'))
        <div class="alert alert-failed alert-dismissible fade show" role="alert">
        <p>{{$message}}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
@endif
@if ($message = session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{$message}}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
@endif
    <!-- Nav tabs -->
<ul
    class="nav nav-tabs nav-tabs-custom"
    role="tablist"
>
    <li class="nav-item">
    <a
        class="nav-link active"
        data-bs-toggle="tab"
        href="#home1"
        role="tab"
    >
        <span class="d-block d-sm-block">Belum selesai</span>
    </a>
    </li>
    <li class="nav-item">
    <a
        class="nav-link"
        data-bs-toggle="tab"
        href="#profile1"
        role="tab"
    >
        <span class="d-block d-sm-block">Selesai</span>
    </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content mt-2 text-muted mb-5">
    <div class="tab-pane active" id="home1" role="tabpanel">
        <section class="">
            <div id="data"></div>
        </section>
    </div>
    <div class="tab-pane" id="profile1" role="tabpanel">
    
    @forelse($transaksis as $transaksi)
    <div class="card">
        <div class="card-body">
            <div class="d-flex align-items-center mb-3">
                <div class="" style="font-weight:600;font-size:16px">
                    <i class="bx bx-shopping-bag"></i> Belanja
                </div>
                <div class="ms-2">
                    {{ date_format(date_create($transaksi->created_at), 'd M Y') }}
                </div>
                <div class="ms-2">
                    @if($transaksi->status == "selesai")
                    <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-success">{{ $transaksi->status }}</span>
                    @elseif($transaksi->status == "belum")
                    <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-info">{{ $transaksi->status }}</span>
                    @elseif($transaksi->status == "proses")
                    <span style="font-weight:600;text-transform:capitalize" class="badge badge-lg bg-warning">Sedang di{{ $transaksi->status }}</span>
                    @endif
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 mb-3">
                    
            @foreach($transaksi->orderans as $orderan)
                    <div class="d-flex mb-2 align-items-center">
                        <div class="" style="width:80px">
                        <img src="{{ url($orderan->produk->url_image) }}" data-src="{{ url($orderan->produk->url_image) }}" class="lazy w-100" style="height:80px;object-fit:cover" alt="Image">
                        </div>
                        <div class="ms-3" style="flex:1">
                            <div style="font-size:17px;font-weight:600">{{ $orderan->produk->nama_produk }}</div>
                            {{ $orderan->produk->umkm->nama_toko }} ({{ $orderan->produk->umkm->kecamatan }})
                        </div>
                    </div>
            @endforeach
            </div>
            <div class="col-lg-6 mb-3">
                <h6>Total Belanja</h6>
                <h3><strong>Rp.{{ number_format($transaksi->total,0,',','.')}}</strong></h3>
                <button type="button" class="btn btn-sm btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#exampleModal">Saran</button>

                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <form action="{{url('pesanan/saran')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="saran" id="saran" class="form-control" placeholder="Masukkan Kritik / Saran" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                      </div>
                    </div>
                  </div>
                
                
                  @if($transaksi->status != 'selesai' && $transaksi->status != 'proses')
                <div class="mt-3">
                    <p class="text-danger">Pesanan yang dipesan tidak dapat dibatalkan</p>
                </div>
                @endif
            </div>
        </div>
    </div>
        </div>
        @empty
        <div class="text-center mt-5">Belum ada pesanan selesai</div>
    @endforelse
        
    </div>
</div>
</div>
</div>
<!-- /page_header -->

</div>
<!-- /container -->

<!-- /box_cart -->

</main>
<!--/main-->

</div>

<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){

        $.ajax({
            type:'GET',
            url:"{{ url('/datapesanan') }}",
            success:function(data){
                $('#data').html(data);
            }
        })

        setInterval(() => {
            $.ajax({
            type:'GET',
            url:"{{ url('/datapesanan') }}",
            success:function(data){
                $('#data').html(data);
            }
        })
        }, 5000);
    })
</script>
@endsection
