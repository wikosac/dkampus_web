
    <!-- Preloader -->
    <div id="js-preloader" class="js-preloader">
        <div class="content">
          <img src="{{ asset('images/Logo/D.svg') }}" alt="Dkampus Logo">
        </div>
        <div class="preloader-inner">
        </div>
      </div>

      <!-- Mobile Menu -->
      <div class="mobile-nav-wrapper">
        <div class="mobile-menu-inner">
          <ul class="mobile-menu">
            <li><a href="{{ url('/umkm') }}">Beranda</a></li>

            <li class="has-sub"><a href="#">Layanan <i class="sub-icon fa fa-angle-down"></i></a>
              <ul class="sub-menu">
                <li><a href="{{ url('/error') }}">Di Manaje Sosmed</a></li>
                <li><a href="{{ url('/dtoko') }}">DI-Toko</a></li>

              </ul>
            </li>

            <li><a href="contact.html">Blog</a></li>
            <li><a href="contact.html">Hubungi Kami</a></li>
          </ul>
        </div>
      </div>
      <div class="mobile-menu-overlay"></div>

      <!-- Header -->
      <header class="site-header fixed-header">
        <div class="container expanded ">
          <div class="header-wrap">
            <div class="fixed-header-logo">
              <a href="{{ url('/umkm') }}">
              <img src="{{ asset('images/Logo/Dkampus-dark-transparent.svg') }}" alt="Dkampus Header Logo"></a>
            </div>
            <div class="is-fixed-header-logo">
              <a href="{{ url('/umkm') }}"><img src="{{ asset('images/Logo/Dkampus light.svg') }}" alt="Dkampus Header Logo"></a>

            </div>
            <div class="header-nav">
                <ul class="main-menu align-items-end">
                  <li><a href="{{ url('/umkm') }}">Beranda</a></li>

                  <li class="menu-item-has-children"><a href="#">Layanan</a>
                    <ul class="sub-menu">
                      <li><a href="{{ url('/error') }}">Di Manaje Sosmed</a></li>
                      <li><a href="{{ url('/dtoko') }}">DI-Toko</a></li>
                    </ul>
                  </li>
                  <li><a href="contact.html">Blog</a></li>
                  <li><a href="{{ url('/hubungi') }}">Hubungi Kami</a></li>
                </ul>
            </div>

          </div>
        </div>
      </header>
      <!-- Header -->

