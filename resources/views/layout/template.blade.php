<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link href="{{ asset('images/Logo/D.svg') }}" rel="icon">
    <title>Buka Toko - Dkampus</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('assets/styles/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/styles/main.css') }}" />

    @stack('styles')
  </head>

  <body>
    @include('layout.nav')
    @yield('content')
    
    <section class="footer-content">

        <div class="cta-footer">
            <div class="container">
              <div class="row">
                <div class="col-lg-8">
                  <h2>Layanan Media Agency</h2>
                  <h5>Masih bingung? Hubungi kami untuk mendapatkan konsultasi gratis oleh Digital Marketing Specialist dari D’Creativl!</h5>
                </div>
                <div class="col-lg-4">
                  <div class="main-white-button">
                    <a href="#">WhatsApps</a></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <div class="main-footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <div class="footer-heading">
                <h4>D'Creativ Indonesia</h4>
              </div>
              <p class="about-oxana">D’Creativ merupakan layanan media agency yang membantu dalam melakukan Social Media Activation, Social Media Management, Social Media Marketing, Social Media Optimization, hingga berbagai kebutuhan Digital Marketing lainnya.</p>
            </div>
            <div class="col-lg-2">
              <div class="footer-heading">
                <h4>Layanan Kami</h4>
              </div>
              <ul class="useful-links">
                <li><a href="{{ url('/error') }}">Di Manaje Sosmed</a></li>
                <li><a href="{{ url('/dtoko') }}">DI-Toko</a></li>
              </ul>

            </div>
            <div class="col-lg-3">
              <div class="footer-heading">
                <h4>Hubungi!</h4>
              </div>
              <ul class="more-info">
                <li>Email: <a href="#">umkm@dcreativ.id</a></li>
                <li>Phone: <a href="#">+62 851 5618 1331</a></li>
                <li>Address: <a href="#">Telkom University, Jl. Telekomunikasi No. 1, Terusan Buahbatu - Bojongsoang, Sukapura, Kec. Dayeuhkolot, Bandung, Jawa Barat 40257</a></li>
              </ul>
            </div>
            <div class="col-lg-3">
              <div class="footer-heading">
                <h4>Temukan kami</h4>
              </div>
              <p>Shaman synth retro slow-carb. Vape taxidermy twee, put a bird.</p>
              <ul class="social-icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/dcreativ_id/"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>

              </ul>
            </div>
            <div class="col-lg-12">
              <div class="sub-footer">
                <p>Copyright © 2021 <a href="#">D'Creativ</a>. All rights reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Go To Top -->
    <a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>


    <!-- assets/Scripts -->
    <script src="{{ asset('assets/scripts/vendors/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/jquery.hoverIntent.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/wow.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/parallax.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/isotope.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/packery-mode.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/owl-carousel.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/jquery.countTo.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/slide-nav.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/vendors/accordions.js') }}"></script>
    <script src="{{ asset('assets/scripts/main.js') }}"></script>

    @stack('scripts')
  </body>
</html>
