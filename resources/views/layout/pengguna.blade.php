
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta
      content="Selangkah Lebih Mudah Belanja dengan DKampus"
      name="description"
    />
    <meta content="DKampus" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" type="{{ asset('asset1/img/Dkampus 1.png') }}" sizes="20x20" href="{{ asset('asset1/img/Dkampus 1.png') }}">

    <!-- Bootstrap Css -->
    <link
      href="{{ asset('frontend/css/bootstrap.min.css') }}"
      id="bootstrap-style"
      rel="stylesheet"
      type="text/css"
    />
    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Icons Css -->
    <link href="{{ asset('frontend/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link
      href="{{ asset('frontend/css/app.min.css') }}"
      id="app-style"
      rel="stylesheet"
      type="text/css"
    />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/progressive-image.js/dist/progressive-image.css">
    <script src="https://cdn.jsdelivr.net/npm/progressive-image.js/dist/progressive-image.js"></script>
    <style>
        .qr {
            height: 800px;
            margin: 5px auto 5px;
        }
        .slide{
            transform:translateY(100vh);
            transition:all .5s ease-in-out;
            display:none
        }
        .slide.up{
            transform:translateY(0vh);
            display:block
        }
    </style>

    @stack('style')
  </head>

  <body data-topbar="light" data-layout="horizontal">
    <!-- Begin page -->
    <div id="layout-wrapper">

      <!-- ============================================================== -->
      <!-- Start right Content here -->
      <!-- ============================================================== -->
      @include('part.header')
      <div class="main-content">
        <div class="page-content">
          <div class="container-fluid">

            @yield('content')

          </div>
          <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
      </div>

      <!-- end main content-->
    </div>


    <!-- END layout-wrapper -->
    @include('part.footer')


    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>


    <div style="position:fixed;top:0;bottom:0;right:0;left:0;z-index:99" class="bg-white text-dark slide d-lg-none" id="menuKonten">
        <div class="d-flex align-items-center border-bottom pb-2 px-3 pt-3 fixed-top bg-white">
            <div class="" id="closeMenu">
                <i class="bx bx-x" style="font-size:28px;font-weight:500"></i>
            </div>
            <div class="ms-2">
                <span style="font-size:16px;font-weight:500" class="text-dark">Menu Utama</span>
            </div>
        </div>
        <div class="px-4 py-3 mt-6" style="margin-top:4em">
        @if(session()->has('data') != true)
            <div class="row pb-3 border-bottom border-2">
                <div class="">
                    <a href="{{ url('/login') }}" class="btn btn-primary w-100">Masuk</a>
                </div>
                {{-- <div class="col-6">
                    <a href="{{ url('/register-v2') }}" class="btn btn-outline-primary w-100">Daftar</a>
                </div> --}}
            </div>

            <div class="row mt-4">
              <a href="{{url('umkm/register')}}" class="text-dark" style="font-weight:10px; font-size:15px"> <i class="bx bx-store" style="24px"></i> Buka Toko</a>
            </div>
        @else

        <div class="d-flex mt-0 align-items-center">
            <div class="ms-2">
            <span style="font-size:14px;font-weight:600" class="text-dark">{{ session('data')['username'] }}</span>
            </div>
            <div class="ms-auto">
                <a class="btn btn-danger btn-sm" href="{{ url('/keluar') }}"><i class="bx bx-power-off" style="font-size:23px !important"></i></a>
            </div>
        </div>

        @if(session()->get('level') == 'umkm')
            <div class="d-flex mt-3 align-item-center">
                <div class="">
                    <i class="bx bx-store" style="font-size:24px;"></i>
                </div>
                <div class="ms-2">
                <span style="font-size:14px;" class="text-dark"><a href="{{ url('/umkm/dashboard') }}" class="text-dark">Dashboard Toko</a></span>
                </div>
            </div>
            @endif
        @endif



        @if(session('data'))
        <div class="mt-3">
            <span style="font-size:16px;font-weight:500" class="text-dark">Aktivitas saya</span>
        </div>
                <div class="d-flex mt-3 align-item-center">
                    <div class="">
                        <i class="bx bx-user" style="font-size:24px;"></i>
                    </div>
                    <div class="ms-2">
                    <span style="font-size:14px;" class="text-dark"><a href="{{ url('/edit-profile') }}" class="text-dark">Edit Profile</a></span>
                    </div>
                </div>
                <div class="d-flex mt-3 align-item-center">
                    <div class="">
                      <i class="bx bx-store" style="font-size:24px;"></i>
                    </div>
                    <div class="ms-2">
                    <span style="font-size:14px;" class="text-dark"><a href="{{ url('/umkm/register') }}" class="text-dark">Buka Toko</a></span>
                    </div>
                </div>
                <div class="d-flex mt-3 align-item-center">
                    <div class="">
                        <i class="bx bx-spreadsheet" style="font-size:24px;"></i>
                    </div>
                    <div class="ms-2">
                    <span style="font-size:14px;" class="text-dark"><a href="{{ url('/pesanan') }}" class="text-dark">Daftar Pesanan</a></span>
                </div>

                @endif
        </div>
    </div>

    <!-- JAVASCRIPT -->
    @stack('script')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    
    <script src="{{ asset('frontend/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('frontend/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('frontend/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('frontend/libs/node-waves/waves.min.js') }}"></script>


    <!-- <script src="frontend/js/pages/dashboard.init.js"></script> -->

    <script src="{{ asset('frontend/js/app.js') }}"></script>

    <script>

        $(document).ready(function(){

          $('#menu').click(function(){
              $('#menuKonten').toggleClass('up')
          })
          $('#closeMenu').click(function(){
              $('#menuKonten').toggleClass('up')
          })
        })

        $(window).on('load', function() {

            $('#js-preloader').addClass('loaded');

        });


    </script>
  </body>
</html>
