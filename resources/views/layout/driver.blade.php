<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#E96E00">
    <link rel="icon" type="{{ asset('images/Logo/D.svg') }}" sizes="16x16">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    @stack('style')
</head>
<body>

    <div class="d-lg-block d-md-block d-sm-block d-none text-center">
        <div style="margin:50vh 0">
            <h3><strong>404 Not Found</strong></h3>
            <p>Ups!! Tampilan dekstop belum tersedia saat ini.</p>
        </div>
    </div>

    <div class="d-lg-none d-md-none d-sm-none d-block">
@yield('content')
    @if(Request::segment(2) == '' || Request::segment(2) == 'create_account' || Request::segment(3) == 'success' || Request::segment(2) == '404')
    @else
    <div style="position:fixed;bottom:0;left:0;right:0;background:white;box-shadow:-2px -2px 8px rgba(0,0,0,.1)" class="border-top d-flex justify-content-around align-items-center pb-2 pt-2">
        <div class="{{ Request::segment(2) == 'dashboard' ? 'nav-bottom-item-active' : 'nav-bottom-item' }}">
            <a href="{{ url('/driver/dashboard') }}" class="">
                <div class="d-flex align-items-center flex-column ">
                    <div class=""><i class="fas fa-clipboard-list" style="font-size:23px"></i></div>
                    <div class="" style="font-size:small">Pesanan</div>
                </div>
            </a>
        </div>
        <div class="{{ Request::segment(2) == 'orderan' ? 'nav-bottom-item-active' : 'nav-bottom-item' }}">
            <a href="{{ url('/driver/orderan') }}" class="">
                <div class="d-flex align-items-center flex-column ">
                    <div class=""><i class="fas fa-history"></i></div>
                    <div class="" style="font-size:small">Riwayat Pesanan</div>
                </div>
            </a>
        </div>
        <div class="{{ Request::segment(2) == 'profile' ? 'nav-bottom-item-active' : 'nav-bottom-item' }}">
            <a href="{{ url('/driver/profile') }}" class="">
                <div class="d-flex align-items-center flex-column ">
                    <div class=""><i class="fas fa-user" style="font-size:23px"></i></div>
                    <div class="" style="font-size:small">Profile</div>
                </div>
            </a>
        </div>
    </div>
    @endif

    </div>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

@stack('script')

</body>
</html>