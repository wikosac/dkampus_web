<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	 {{-- Icon Tab Bar --}}
     <link rel="icon" href="{{asset('images/Logo/Dkampus Light.svg')}}">
	<!--plugins-->
	<link href="{{asset('signUp_asset/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
	<link href="{{asset('signUp_asset/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
	<link href="{{asset('signUp_asset/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
    
	<!-- loader-->
	<link href="{{asset('signUp_asset/css/pace.min.css')}}" rel="stylesheet" />
	<script src="{{asset('signUp_asset/js/pace.min.js')}}"></script>

	<!-- Bootstrap CSS -->
	<link href="{{asset('signUp_asset/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('signUp_asset/css/bootstrap-extended.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
	<link href="{{asset('signUp_asset/css/app.css')}}" rel="stylesheet">
	<link href="{{asset('signUp_asset/css/icons.css')}}" rel="stylesheet">
	<title>Dkampus - Masuk</title>
</head>

<body>
	<!--wrapper-->
	<div class="wrapper">
		<div class="authentication-header"></div>
		{{-- <header class="login-header shadow">
			<nav class="navbar navbar-expand-lg navbar-light bg-white rounded fixed-top rounded-0 shadow-sm">
				<div class="container-fluid">
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
					</button>
				</div>
			</nav>
		</header> --}}
		@yield('content')
		<footer class="bg-white shadow-sm border-top p-2 text-center fixed-bottom">
			<p class="mb-0">Copyright © {{Carbon\Carbon::now()->year}}. All right reserved.</p>
		</footer>
	</div>
	<!--end wrapper-->
	<!-- Bootstrap JS -->
	<script src="{{asset('signUp_asset/js/bootstrap.bundle.min.js')}}"></script>
	<!--plugins-->
	<script src="{{asset('signUp_asset/js/jquery.min.js')}}"></script>
	<script src="{{asset('signUp_asset/plugins/simplebar/js/simplebar.min.js')}}"></script>
	<script src="{{asset('signUp_asset/plugins/metismenu/js/metisMenu.min.js')}}"></script>
	<script src="{{asset('signUp_asset/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
	<!--Password show & hide js -->
	<script>
		$(document).ready(function () {
			$("#show_hide_password a").on('click', function (event) {
				event.preventDefault();
				if ($('#show_hide_password input').attr("type") == "text") {
					$('#show_hide_password input').attr('type', 'password');
					$('#show_hide_password i').addClass("bx-hide");
					$('#show_hide_password i').removeClass("bx-show");
				} else if ($('#show_hide_password input').attr("type") == "password") {
					$('#show_hide_password input').attr('type', 'text');
					$('#show_hide_password i').removeClass("bx-hide");
					$('#show_hide_password i').addClass("bx-show");
				}
			});
		});
	</script>
	<!--app JS-->
	<script src="{{asset('assets/js/app.js')}}"></script>
</body>

</html>