<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama_depan')->nullable();
            $table->string('nama_belakang')->nullable();
            $table->string('fullname')->nullable();
            $table->string('username');
            $table->string('email');
            $table->string('password1');

            $table->string('provinsi')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_telephone')->nullable();

            $table->string('ktp')->nullable();
            $table->string('nik')->nullable();
            $table->string('sim')->nullable();

            $table->integer('total_order')->nullable();
            $table->integer('fee_ojek')->nullable();

            $table->enum('status', ['belum', 'diterima']);
            $table->enum('level', ['umkm', 'admin', 'admin_super', 'user', 'driver']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
