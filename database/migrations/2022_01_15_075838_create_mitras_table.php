<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMitrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitras', function (Blueprint $table) {
            $table->id();
            $table->string("nama");
            $table->string("alamat", 555);
            $table->string("foto", 555)->nullable();
            $table->string("shopee", 755)->nullable();
            $table->string("ig", 755)->nullable();
            $table->string("facebook", 755)->nullable();
            $table->string("tokopedia", 755)->nullable();
            $table->string("bukalapak", 755)->nullable();
            $table->text("keterangan");
            $table->string("no_wa", 20);
            $table->string("slug", 555);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitras');
    }
}
