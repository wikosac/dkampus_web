<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenggunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggunas', function (Blueprint $table) {
            $table->id();
            $table->string('nama_depan');
            $table->string('nama_belakang');
            $table->string('username');
            $table->string('email');
            $table->string('password1');

            $table->string('provinsi')->nullable();
            $table->string('kabupaten')->nullable();;
            $table->string('kecamatan')->nullable();;
            $table->string('alamat')->nullable();;

            $table->enum('level', ['umkm', 'admin', 'admin_super', 'user', 'driver']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunas');
    }
}
