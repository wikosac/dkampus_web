<?php

use App\Http\Controllers\{UmkmController, PenggunaController, CartController, MailController, DriverController, AdminController,GoogleController, NotifController};
use Illuminate\Support\Facades\Route;

// Driver Dashboard
Route::middleware(['cekDriver'])->group(function(){
    // Driver
    Route::view('/driver/dashboard', 'driver.dashboard');
    Route::get('/driver/logout', function(){
        session()->forget('data');
        session()->forget('user_id');
        session()->forget('level');
        return redirect('driver');
    });
    Route::get('/driver/dataorder', [DriverController::class, 'dataorder']);

    Route::get('/driver/order/detail/{id}', [DriverController::class, 'detail']);
    Route::get('/driver/order/ambil/{id}', [DriverController::class, 'ambil']);
    Route::get('/driver/order/selesai/{id}', [DriverController::class, 'selesai']);
    Route::get('/driver/order/delete/{id}', [DriverController::class, 'delete']);
    
    Route::get('/driver/orderan', [DriverController::class, 'orderan']);
    Route::get('/driver/profile', [DriverController::class, 'profile']);
});

Route::middleware(['cekUmkm'])->group(function(){
    Route::get('/umkm/dashboard', [UmkmController::class, 'index']);
    Route::get('/umkm/transaksi', [UmkmController::class, 'transaksi']);
    Route::get('/umkm/manage_produk', [UmkmController::class, 'manage_produk']);
    Route::get('/umkm/add_produk', [UmkmController::class, 'add_produk']);
    Route::get('/umkm/add', [UmkmController::class, 'add']);
    Route::get('/umkm/add_produk', [UmkmController::class, 'add_produk']);
    Route::post('/umkm/insert_produk', [UmkmController::class, 'insert_produk']);
    Route::get('/umkm/edit_produk/{id}', [UmkmController::class, 'edit_produk']);
    Route::post('/umkm/update_produk', [UmkmController::class, 'update_produk']);
    Route::get('/umkm/delete/{id}', [UmkmController::class, 'delete_produk']);
    Route::get('/umkm/keluar', [UmkmController::class, 'keluar']);
    Route::get('/umkm/setting', [UmkmController::class, 'setting'])->name('umkm.setting');
    Route::post('/umkm/setting', [UmkmController::class, 'updateSetting'])->name('umkm.update-setting');
    Route::post('/umkm/update-location', [UmkmController::class, 'updateLocation'])->name('umkm.update-location');
});

Route::middleware(['CekSession'])->group(function(){

    // Cart
    Route::get('/cart', [CartController::class, 'index'])->middleware('CekSession');
    Route::post('/cart/add', [CartController::class, 'store']);
    Route::get('/cart/delete/{id}', [CartController::class, 'delete']);
    Route::post('/cart/berubah/{id}',[CartController::class,'changedOrder']);
    Route::post('/cart/delete/collection', [CartController::class, 'deleteCollection']);

    Route::post('/order', [CartController::class, 'order']);

    //mail
    Route::get('/mail',[MailController::class,'index'])->name('user.mail');
    
    //notification
    Route::get('/notification',[NotifController::class,'index'])->name('user.notification');


    // Pesanan
    Route::get('/pesanan', [PenggunaController::class, 'pesanan']);
    Route::get('/datapesanan', [PenggunaController::class, 'datapesanan']);
    Route::get('/pesanan/delete/{id}', [PenggunaController::class, 'delete_pesanan']);
    Route::post('pesanan/saran',[PenggunaController::class,'saranPesanan']);
    
    // Profile
    Route::get('edit-profile', [PenggunaController::class, 'edit_profile']);
    Route::post('edit-profile', [PenggunaController::class, 'edit_profile_proses'])->name('edit-profile-proses');

    // Ajax Update Lokasi
    Route::post('update-location', [PenggunaController::class, 'update_location'])->name('update-location');
    
    // Ajax Alamat
    Route::post('getKota', [PenggunaController::class, 'getKota'])->name('getKota');
    Route::post('getKecamatan', [PenggunaController::class, 'getKecamatan'])->name('getKecamatan');
    Route::post('getKelurahan', [PenggunaController::class, 'getKelurahan'])->name('getKelurahan');

    // Hitung Jarak
    Route::get('/hitung-jarak', [PenggunaController::class, 'hitungJarak']);

});

Route::middleware(['cekAdmin'])->group(function(){

    Route::get('/admin/dashboard', AdminController::class.'@index');
    Route::get('/admin/keluar', [AdminController::class,'logout']);

    // ADMIN Bagian Mitra
    Route::get('/admin/mitra', AdminController::class.'@mitra');
    Route::get('/admin/mitra/edit/{id}', AdminController::class.'@mitra_edit');
    Route::post('/admin/mitra/edit', AdminController::class.'@mitra_update');
    Route::get('/admin/mitra/produk/add', AdminController::class.'@mitra_produk_add');
    Route::post('/admin/mitra/produk/store', AdminController::class.'@mitra_produk_store');
    Route::get('/admin/mitra/{id}/produks', AdminController::class.'@produk_mitra');
    Route::get('/admin/mitra/produk/edit/{id}', AdminController::class.'@edit_produk_mitra');
    Route::post('/admin/mitra/produk/update', AdminController::class.'@update_produk_mitra');
    Route::get('/admin/mitra/produk/delete/{id}', AdminController::class.'@delete_produk_mitra');

    // ADMIN Bagian UMKM
    Route::get('/admin/umkm', AdminController::class.'@umkm');
    Route::get('/admin/umkm/{id}/produk', AdminController::class.'@produk_umkm');
    Route::get('/admin/umkm/produk/delete/{id}', AdminController::class.'@delete_produk');
    Route::get('/admin/umkm/pending', AdminController::class.'@pending_umkm');
    Route::get('/admin/umkm/detail/{id}', AdminController::class.'@detail_umkm');
    Route::get('/admin/umkm/approve/{id}', AdminController::class.'@approve_umkm');
    Route::get('/admin/umkm/delete/{id}', AdminController::class.'@delete_umkm');

    // ADMIN Bagian Driver
    Route::get('/admin/driver', AdminController::class.'@driver');
    Route::get('/admin/driver/detail/{id}', AdminController::class.'@detail_driver');
    Route::get('/admin/driver/approve/{id}', AdminController::class.'@approve_driver');
    Route::get('/admin/driver/pending', AdminController::class.'@pending_driver');
    Route::get('/admin/driver/delete/{id}', AdminController::class.'@delete_driver');

    // ADMIN Bagian Ongkir
    Route::get('/admin/ongkir', AdminController::class.'@ongkir');
    Route::get('/admin/ongkir/input', AdminController::class.'@input_ongkir');
    Route::post('/admin/ongkir/input/store', AdminController::class.'@store_ongkir');
    Route::get('/admin/ongkir/delete/{id}', AdminController::class.'@delete_ongkir');

    // ADMIN Bagian Kategori
    Route::get('/admin/kategori', AdminController::class.'@kategori');
    Route::get('/admin/kategori/add', AdminController::class.'@add_kategori');
    Route::post('/admin/kategori/add', AdminController::class.'@store_kategori');
    Route::get('/admin/kategori/edit/{id}', AdminController::class.'@edit_kategori');
    Route::post('/admin/kategori/edit', AdminController::class.'@update_kategori');
    Route::get('/admin/kategori/delete/{id}', AdminController::class.'@delete_kategori');

    // ADMIN Bagian Kawasan
    Route::get('/admin/kawasan', AdminController::class.'@kawasan');
    Route::get('/admin/kawasan/add', AdminController::class.'@add_kawasan');
    Route::post('/admin/kawasan/add', AdminController::class.'@store_kawasan');
    Route::get('/admin/kawasan/edit/{id}', AdminController::class.'@edit_kawasan');
    Route::post('/admin/kawasan/edit', AdminController::class.'@update_kawasan');
    Route::get('/admin/kawasan/delete/{id}', AdminController::class.'@delete_kawasan');

    // ADMIN Bagian Transaksi
    Route::get('/admin/transaksi', AdminController::class.'@transaksi');
    Route::get('/admin/transaksi/approve/{id}', AdminController::class.'@approve_transaksi');   

    Route::get('admin/toko/transaksi',[AdminController::class,'tokoTransaksi']);

});


// Route UMKM
Route::get('/umkm', function () {
    return view('umkm.home');
});
Route::post('umkm/insertuser', [UmkmController::class, 'store']);
Route::post('/umkm/insert', [UmkmController::class, 'insert']);
Route::get('/hubungi', function () {
    return view('umkm.hubungi');
});

Route::get('/error', function () {
    return view('umkm.error');
});


Route::get('/dtoko', function () {
    return view('umkm.dtoko');
});

Route::get('/umkm/register', function () {
    return view('umkm.register');
});

Route::get('/umkm/visibilitas/off/{id}',[UmkmController::class,'visibilitas_off']);

Route::get('/umkm/visibilitas/on/{id}',[UmkmController::class,'visibilitas_on']);

Route::get('/registerumkm', [UmkmController::class, 'add']);


Route::get('/', [PenggunaController::class, 'index']);
Route::post('/load', [PenggunaController::class, 'load']);
Route::post('/data-wilayah/{jenis}', [PenggunaController::class, 'data_wilayah']);
Route::post('toko/banner/{id}',[PenggunaController::class,'bannerStore']);
Route::get('/toko/{id}', [PenggunaController::class, 'toko']);
Route::get('/search', [PenggunaController::class, 'search']);
Route::get('/terms', function(){
    return view('pengguna.terms');
});
Route::get('/register-v2', [PenggunaController::class, 'addRegister']);
Route::post('/login/check', [PenggunaController::class, 'login_check']);
Route::get('/register', function () {
    return view('pengguna.registerpengguna');
});
Route::post('/register/store', [PenggunaController::class, 'register']);
Route::get('/keluar', [PenggunaController::class, 'keluar']);


Route::get('/kawasan/{daerah}', [PenggunaController::class, 'kawasan']);
Route::get('/kategori/{kategori}', [PenggunaController::class, 'kategori']);


// login
Route::get('login', function () {
    return view('login');
});

Route::post('umkm/auth', [UmkmController::class, 'auth']);
Route::get('/detail/{id}', [PenggunaController::class, 'detail']);

// Google Login

Route::get('auth/google',[GoogleController::class,'redirectToGoogle'])->name('auth.google');

Route::get('auth/google/callback',[GoogleController::class,'handleGoogleCallback'])->name('auth.google.callback');



// Driver Routing
Route::get('/driver', [DriverController::class, 'index']);
Route::get('/driver/404', function(){
    return view('driver.404');
});
Route::post('/driver/login/auth', [DriverController::class, 'auth']);
Route::get('/driver/create_account', [DriverController::class, 'create_account']);
Route::post('/driver/create/store', [DriverController::class, 'store_account']);
Route::get('/driver/create/success', [DriverController::class, 'success_create']);

// Mitra Routing
Route::get('/mitra', [PenggunaController::class, 'mitra']);
Route::get('/mitra/{mitra:slug}', [PenggunaController::class, 'mitra_detail']);
Route::get('/mitra/produk/{slug}', [PenggunaController::class, 'mitra_produk_detail']);


